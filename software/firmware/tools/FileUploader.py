import requests
import sys
import argparse
import os
from os.path import relpath
from os import listdir
from os.path import isfile, join
import threading
import time
import gzip
from pathlib import Path

FILE_UPLOAD_URI         = "/api/file_upload"
DIRECTORY_EMPTY_URI     = "/api/dir_empty"
DIRECTORY_CREATE_URI    = "/api/dir_create"
FILESYSTEM_PREFIX       = "/spifat/"

class FileUploader:
    def __init__(self, ip, _dir, gzip):
        self.ip = ip
        self.dir = _dir
        self.gzip = gzip

    def empty_dir(self):
        response = requests.post("http://" + self.ip + DIRECTORY_EMPTY_URI, data={ 'dir' : FILESYSTEM_PREFIX })
        if response.status_code != 200:
            print(f"Failed to empty {FILESYSTEM_PREFIX}, code : {response.status_code}, reason : {response.content}")
            return False
        else:
            print(f"Deleted {FILESYSTEM_PREFIX}")
            return True


    def create_dir(self, _dir):
        if _dir == ".": return True
        response = requests.post("http://" + self.ip + DIRECTORY_CREATE_URI, data={ 'dir' : FILESYSTEM_PREFIX + _dir })
        if response.status_code != 200:
            print(f"Failed to create {FILESYSTEM_PREFIX + _dir}, code : {response.status_code}, reason : {response.content}")
            return False
        else:
            print(f"Created {_dir}")
            return True


    def upload_file(self, file):
        if(Path(file).suffix == ".map"):
            return
        f = open(file, "br")
        data = gzip.compress(f.read())
        response = requests.post("http://" + self.ip + FILE_UPLOAD_URI, files={ FILESYSTEM_PREFIX + relpath(file, self.dir) : data })
        if response.status_code != 200:
            print(f"Failed to upload {FILESYSTEM_PREFIX + relpath(file, self.dir)}, code : {response.status_code}, reason : {response.content}")
            return False
        else:
            print(f"Uploaded {FILESYSTEM_PREFIX + relpath(file, self.dir)}, gzip'd size: {len(data)}")
            return True
        f.close()
        

    def upload_dir(self):
        self.upload_files(self.dir)

    def upload_files(self, _dir):
        self.create_dir(relpath(_dir, self.dir))
        threads = []
        for f in listdir(_dir):
            if isfile(join(_dir, f)):
                self.upload_file(join(_dir, f))
            else:
                self.upload_files(join(_dir, f))
        
def __main__():
    parser = argparse.ArgumentParser(description='Upload files to ESP filesystem.')
    parser.add_argument('--ip', type=str, required = True)
    parser.add_argument('--dir', type=str, required = True)
    parser.add_argument('--file', type=str)
    parser.add_argument('--gz', type=str, default = True)

    args = parser.parse_args()
    uploader = FileUploader(args.ip, args.dir, args.gz)
    if args.file:
        if not uploader.upload_file(args.file): sys.exit(-1)
    else:
        if not uploader.empty_dir(): sys.exit(-1)
        if not uploader.upload_dir(): sys.exit(-1)


if __name__ == "__main__":
    __main__()