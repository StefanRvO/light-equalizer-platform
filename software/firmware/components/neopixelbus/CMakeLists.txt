file(GLOB_RECURSE source_files "./NeoPixelBus/src/internal/*.cpp" "./NeoPixelBus/src/internal/*.c")

idf_component_register(SRCS ${source_files}
                        INCLUDE_DIRS "./NeoPixelBus/src"
                        REQUIRES "arduino")
target_compile_options(${COMPONENT_LIB} PRIVATE "-Wno-type-limits")