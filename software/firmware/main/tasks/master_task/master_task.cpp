#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "esp_mesh.h"
#include "esp_log.h"

#include "modules/protocol_module/message_handler/message_handler_module.h"
#include "modules/light_management_module/light_management_module.h"
#include "modules/light_management_module/frequency_management_module/frequency_management_module.h"
#include "modules/light_control_module/light_control_module.hpp"
#include "modules/mesh_module/node_info_module/node_info_module.h"
#include "modules/alive_led_module/alive_led_module.hpp"
#include "modules/runtime_stats/runtime_stats.h"
#include "modules/fft_module/fft_module.h"
#include "modules/mesh_module/mesh_module.h"
#include "modules/filesystem_sync_module/filesystem_sync_module.h"
#include "modules/firmware_sync_module/firmware_sync_module.h"
#include "modules/mesh_module/root/mesh_root_module.h"
#include "modules/mesh_module/mesh_module.h"
#include "modules/web_server_module/web_server_module.h"
#include "modules/file_system_module/file_system_module.h"
#include "modules/onboard_led_strength_module/onboard_led_strength_module.h"
#include "modules/debug_led_module/debug_led_module.hpp"
#include "modules/led_strip_module/led_strip_module.hpp"

#define TASK_INTERVAL 2

__attribute__((unused)) static const char *TAG = "MASTER TASK";

static bool is_behind = false;

static void pending_status(void);
static void update_is_behind(TickType_t last_wake_time);

esp_err_t master_task_init(void)
{
    esp_err_t ret = ESP_OK;
    #if defined(CONFIG_IDF_TARGET_ESP32) || defined(CONFIG_IDF_TARGET_ESP32S3)

      if(config_reader_get_mesh_type() == NODE_TYPE_ROOT)
      {
        ret |= fft_module_init();
      }
    #endif
    ret |= light_management_module_init();
    ret |= web_server_module_init();
    led_strip_module_init();
    debug_led_module_init();
    alive_led_module_init();
    node_info_module_init();
    file_system_module_init();
    filesystem_sync_module_init();
    ESP_LOGI(TAG,"MASTER INIT");
    return ret;
}

void master_task(void *taskdata)
{
  ESP_LOGI(TAG,"MASTER LOOP STARTED");
  ESP_ERROR_CHECK(master_task_init());
  TickType_t last_wake_time = xTaskGetTickCount();

  while(true)
  {
    vTaskDelayUntil(&last_wake_time, TASK_INTERVAL);
    alive_led_module_step();
    root_receive_task();
    light_management_module_step();
    onboard_led_strength_module_step();
    node_info_module_send_info();
    node_cleanup_step();
    mesh_module_handle_disconnected_timeout();


    if(firmware_sync_module_get_receive_offset() == 0)
    {
      firmware_sync_module_send_data_step();
    }
    if(!firmware_sync_module_is_sending() && firmware_sync_module_get_receive_offset() == 0)
    {
      filesystem_sync_module_send_data_step();
      led_strip_module_step();
      #if defined(CONFIG_IDF_TARGET_ESP32) || defined(CONFIG_IDF_TARGET_ESP32S3)

      if(config_reader_get_mesh_type() == NODE_TYPE_ROOT)
      {
        fft_module_step();
        frequency_management_module_step();
      }
      #endif
    }
    //pending_status();
    update_is_behind(last_wake_time);
    // print_runtime_stats();
    if(!is_behind)
    {
      web_server_task(NULL);
    }
    // print_wifi_mode();
  }
}

void update_is_behind(TickType_t last_wake_time)
{
    TickType_t cur_tick = xTaskGetTickCount();
    if(cur_tick - last_wake_time < 5)
    {
      is_behind = false;
    }
    else if(cur_tick - last_wake_time > 50)
    {
      is_behind = true;
    }
}

void pending_status(void)
{
  static uint32_t cnt = 0;
  if(cnt++ % 500 == 0)
  {
    mesh_tx_pending_t pending;
    esp_mesh_get_tx_pending(&pending);
    ESP_LOGI(TAG, "Messages in send queue: %d %d %d %d %d %d",  pending.to_parent,
                                                                pending.to_parent_p2p,
                                                                pending.to_child,
                                                                pending.to_child_p2p,
                                                                pending.mgmt,
                                                                pending.broadcast);
  }
}

bool master_task_is_behind(void)
{
  return is_behind;
}