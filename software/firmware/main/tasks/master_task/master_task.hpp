#pragma once

#include "esp_err.h"
#include "esp_log.h"

esp_err_t master_task_init(void);

void master_task(void *taskdata);
bool master_task_is_behind(void);