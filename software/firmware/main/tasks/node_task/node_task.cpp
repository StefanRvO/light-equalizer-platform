#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "esp_mesh.h"
#include "esp_log.h"

#include "modules/protocol_module/message_handler/message_handler_module.h"
#include "modules/light_control_module/light_control_module.hpp"
#include "modules/mesh_module/node_info_module/node_info_module.h"
#include "modules/alive_led_module/alive_led_module.hpp"
#include "modules/runtime_stats/runtime_stats.h"
#include "modules/filesystem_sync_module/filesystem_sync_module.h"
#include "modules/firmware_sync_module/firmware_sync_module.h"
#include "modules/mesh_module/mesh_module.h"
#include "modules/debug_led_module/debug_led_module.hpp"
#include "modules/led_strip_module/led_strip_module.hpp"
#include "modules/mesh_module/node/mesh_node_module.h"

#define delay_ms(ms) vTaskDelay((ms) / portTICK_RATE_MS)

#define TASK_INTERVAL 2

__attribute__((unused)) static const char *TAG = "NODE TASK";


esp_err_t node_task_init(void)
{
    esp_err_t ret = ESP_OK;
    ESP_LOGI(TAG,"NODE INIT");
    led_strip_module_init();
    alive_led_module_init();
    filesystem_sync_module_init();
    debug_led_module_init();

    return ret;
}

extern "C" void node_task(void *taskdata)
{
  TickType_t last_wake_time = xTaskGetTickCount();
  ESP_ERROR_CHECK(node_task_init());
  ESP_LOGI(TAG,"NODE LOOP STARTED");
  
  while(true)
  {
    vTaskDelayUntil(&last_wake_time, TASK_INTERVAL);
    alive_led_module_step();
    node_read_task();
    firmware_sync_module_send_progress_step();
    node_info_module_send_info();
    if(!firmware_sync_module_get_receive_offset())
    {
      filesystem_sync_module_send_progress_step();
      led_strip_module_step();
    }
    print_wifi_mode();
  }
}