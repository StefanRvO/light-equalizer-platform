#pragma once
#include "esp_err.h"
#include "esp_log.h"

#include "modules/c_link.h"

C_LINK void node_task(void *taskdata);
C_LINK esp_err_t node_task_init(void);