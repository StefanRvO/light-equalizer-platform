#include "esp_err.h"
#include "esp_log.h"
#include "esp_mesh.h"
#include "esp_spi_flash.h"
#include "nvs_flash.h"
#include "driver/uart.h"
#include "modules/mesh_module/mesh_module.h"
#include "modules/config_reader_module/config_reader_module.h"
#include "modules/file_system_module/file_system_module.h"
#include "tasks/master_task/master_task.hpp"
#include "tasks/node_task/node_task.hpp"
#include "libs/string_format_lib/string_format_lib.h"
#include "modules/firmware_sync_module/firmware_sync_module.h"
#include "modules/board_revision_module/board_revision_module.h"
#include "esp_ota_ops.h"
#include "Arduino.h"

__attribute__((unused)) static const char *TAG = "MAIN";

extern "C" void app_main()
{
    initArduino();
    board_revision_module_init();
    nvs_flash_init();
    config_reader_module_init();
    mesh_module_init();
    file_system_update_flash_sha256();
    firmware_sync_module_init();
    esp_chip_info_t chip_info;
    /**
     * @brief Print system information.
     */
    esp_chip_info(&chip_info);  
 
    char *fs_sha256 = new char[65];
    char *fw_sha256 = new char[65]; 
    string_format_bin_to_hex(fs_sha256, file_system_get_sha256(), 32);
    string_format_bin_to_hex(fw_sha256, firmware_sync_module_get_sha256(), 32);

    ESP_LOGI(TAG,"******************* SYSTEM INFO *******************");
    ESP_LOGI(TAG,"idf version      : %s", esp_get_idf_version()); 
    ESP_LOGI(TAG,"compile time     : %s %s", __DATE__, __TIME__); 
    ESP_LOGI(TAG,"free heap        : %d Byte", esp_get_free_heap_size());
    ESP_LOGI(TAG,"Boot Partition   : %s", esp_ota_get_boot_partition()->label);
    ESP_LOGI(TAG,"CPU cores        : %d", chip_info.cores);
    ESP_LOGI(TAG,"function         : WiFi%s%s",
             (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
             (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");
    ESP_LOGI(TAG,"silicon revision : %d", chip_info.revision);
    ESP_LOGI(TAG,"flash            : %d MB %s", spi_flash_get_chip_size() / (1024 * 1024),
             (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");
    ESP_LOGI(TAG,"Board TYPE: %u",  config_reader_get_board_type());
    ESP_LOGI(TAG,"Board Revision %u", board_revision_module_get_revision());
    ESP_LOGI(TAG,"Device Name: %s", config_reader_get_name());
    ESP_LOGI(TAG,"NODE TYPE: %u",   config_reader_get_mesh_type());
    ESP_LOGI(TAG,"Firmware SHA256: %s", fw_sha256);
    ESP_LOGI(TAG,"FATFS    SHA256: %s", fs_sha256);
    ESP_LOGI(TAG,"***************************************************");

    delete[] fs_sha256;
    delete[] fw_sha256;

    #if defined(CONFIG_IDF_TARGET_ESP32) || defined(CONFIG_IDF_TARGET_ESP32S3)
    #define CORE_ID 1
    #else
    #define CORE_ID 0
    #endif

    #if defined(CONFIG_IDF_TARGET_ESP32) || defined(CONFIG_IDF_TARGET_ESP32S3)
    if(config_reader_is_root())
    {
        xTaskCreatePinnedToCore(master_task, "MASTER_TASK", 11000, NULL, 10, NULL, CORE_ID);
    }
    else
    #endif
    {
        xTaskCreatePinnedToCore(node_task, "SLAVE_TASK", 5000, NULL, 10, NULL, CORE_ID);
    }
}
