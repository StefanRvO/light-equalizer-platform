#pragma once

#include <stdint.h>
#include "esp_err.h"
#include "esp_log.h"
#include "driver/spi_common.h"

#define mcp3002_CHANNEL_CNT 2

typedef struct {
    float *sample_buf[mcp3002_CHANNEL_CNT];
    uint32_t buf_size;
    uint32_t sample_cnt[mcp3002_CHANNEL_CNT];
} mcp3002_samples_t;

esp_err_t mcp3002_init(spi_host_device_t spi_host, int mosi_pin, int miso_pin, int sclk_pin, int cs_pin);
esp_err_t mcp3002_sample_step(mcp3002_samples_t *samples);