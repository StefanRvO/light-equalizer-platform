#include "wm8782.h"
#include "modules/gpio_utilities/gpio_utilities.h"

void wm8782_init(const wm8782_pin_config_t* const config)
{
    gpio_utilities_set_pin_floating(config->format_select_pin); //Set I2S mode
    gpio_utilities_set_pin_high(config->fast_sample_enable_pin); //Fast sample = slow
    gpio_utilities_set_pin_floating(config->word_length_select_pin); //24 bit words
}