#pragma once

typedef struct
{
    int fast_sample_enable_pin;
    int format_select_pin;
    int word_length_select_pin;
} wm8782_pin_config_t;



void wm8782_init(const wm8782_pin_config_t* const pin_config);