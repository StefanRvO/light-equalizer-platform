#include "ak5720.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/i2s.h"
#include "modules/gpio_utilities/gpio_utilities.h"
#include "esp_err.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "driver/ledc.h"
#include <math.h>

#if defined(CONFIG_IDF_TARGET_ESP32) || defined(CONFIG_IDF_TARGET_ESP32S3)


//Custom pins
#define GAIN_SELECT_PIN   16
#define FILTER_SELECT_PIN 21
#define POWER_OFF_PIN     19

__attribute__((unused)) static const char *TAG = "AK5720";

static void power_off(void);
static void power_on(void);
static void set_gain_0db(void);
static void set_gain_15db(void);
static void set_filter_sharp(void);
void set_filter_delay(void);

static ak5720_pin_config_t pin_config;

void ak5720_init(const ak5720_pin_config_t* const config)
{
    pin_config = *config;
    gpio_utilities_setup(pin_config.power_off_pin, GPIO_MODE_OUTPUT, 1);
    gpio_utilities_setup(pin_config.filter_select_pin, GPIO_MODE_OUTPUT, 0);
    gpio_utilities_setup(pin_config.gain_select_pin, GPIO_MODE_OUTPUT, 0);
    set_filter_sharp();
    set_gain_0db();
    power_off();
    power_on();
}

void power_off(void)
{
    gpio_set_level(pin_config.power_off_pin, 0);
}

void power_on(void)
{
    gpio_set_level(pin_config.power_off_pin, 1);
}

void set_gain_0db(void)
{
    gpio_set_level(pin_config.gain_select_pin, 0);
}

void set_gain_15db(void)
{
    gpio_set_level(pin_config.gain_select_pin, 1);
}

void set_filter_sharp(void)
{
    gpio_set_level(pin_config.filter_select_pin, 0);
}

void set_filter_delay(void)
{
    gpio_set_level(pin_config.filter_select_pin, 1);
}
#endif