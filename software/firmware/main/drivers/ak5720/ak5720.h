#pragma once
#include "modules/sound_sampler_module/sound_sampler_module.h"

typedef struct
{
    int power_off_pin;
    int filter_select_pin;
    int gain_select_pin;
} ak5720_pin_config_t;

void ak5720_init(const ak5720_pin_config_t* const config);
