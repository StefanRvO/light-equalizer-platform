#include "esp_err.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "alive_led_driver.h"
#include "driver/ledc.h"

static const gpio_num_t LED_PIN = 2;

__attribute__((unused)) static const char *TAG = "ALIVE_LED_DRIVER";


void alive_led_driver_init(void)
{
    ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_8_BIT, // resolution of PWM duty
        .freq_hz = 100000,                   // frequency of PWM signal
        .speed_mode = LEDC_LOW_SPEED_MODE,  // timer mode
        .timer_num = LEDC_TIMER_0,           // timer index
        .clk_cfg = LEDC_AUTO_CLK,            // Auto select the source clock
    };
    ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer));

    ledc_channel_config_t ledc_channel = {
            .channel    = LEDC_CHANNEL_0,
            .duty       = 0,
            .gpio_num   = LED_PIN,
            .speed_mode = LEDC_LOW_SPEED_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_TIMER_0
    };
    ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel));
}

void alive_led_set(float strength)
{
    uint16_t duty = ((1 << LEDC_TIMER_8_BIT) - 1) * (1 - strength);
    ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0, duty);
    ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0);
}