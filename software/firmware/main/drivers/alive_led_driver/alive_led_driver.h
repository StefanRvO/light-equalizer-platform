#pragma once
#include <stdbool.h>
#include "modules/c_link.h"
C_LINK void alive_led_driver_init(void);
C_LINK void alive_led_set(float strength);
