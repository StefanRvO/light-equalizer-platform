#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

COMPONENT_ADD_INCLUDEDIRS := "." 

COMPONENT_SRCDIRS := $(COMPONENT_SRCDIRS) $(shell cd "$(COMPONENT_PATH)" && find . -type d)

$(info $$COMPONENT_SRCDIRS is [${COMPONENT_SRCDIRS}])