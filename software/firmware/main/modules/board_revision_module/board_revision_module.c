#include "board_revision_module.h"
#include "driver/gpio.h"
#include "modules/gpio_utilities/gpio_utilities.h"
#include "esp_log.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

static const gpio_num_t revision_pins[] = {23, 12, 13};
static board_revision_t board_revision;

__attribute__((unused)) static const char *TAG = "BOARD_REVISION";

typedef enum
{
    PIN_LOW,
    PIN_HIGH,
    PIN_FLOATING,
} pin_config_t;


static pin_config_t get_pin_config(gpio_num_t gpio);




void board_revision_module_init(void)
{
    board_revision = 0;
    for(uint8_t i = 0; i < sizeof(revision_pins) / sizeof(revision_pins[0]); i++)
    {
        pin_config_t pin_config = get_pin_config(revision_pins[i]);
        ESP_LOGI(TAG, "PIN %u, %u", revision_pins[i], pin_config);
        board_revision += pin_config  << (2 * i);;
        gpio_set_pull_mode(revision_pins[i], GPIO_FLOATING);
    }

    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);

    board_revision += chip_info.revision * 10000;
    ESP_LOGI(TAG, "BOARD REVISION: %u", board_revision);
    switch(board_revision)
    {
        case BOARD_REVISION_V2_I2S:
        case BOARD_REVISION_V1_I2S:
        case BOARD_REVISION_V3_I2S:
        case BOARD_REVISION_V1_SLAVE:
            break;
        default:
                ESP_LOGE(TAG, "UNKNOWN_BOARD_REVISION %u", board_revision);
                while(true);
            break;
    }
}

board_revision_t board_revision_module_get_revision(void)
{
    return board_revision;
}

pin_config_t get_pin_config(gpio_num_t gpio)
{
    gpio_utilities_setup_with_pull(gpio, GPIO_MODE_INPUT, 0, GPIO_PULLUP_ONLY);
    vTaskDelay(5 / portTICK_RATE_MS);
    if(!gpio_get_level(gpio))
    {
        return PIN_LOW;
    }
    gpio_set_pull_mode(gpio, GPIO_PULLDOWN_ONLY);
    vTaskDelay(5 / portTICK_RATE_MS);
    if(gpio_get_level(gpio))
    {
        return PIN_HIGH;
    }
    return PIN_FLOATING;   
}