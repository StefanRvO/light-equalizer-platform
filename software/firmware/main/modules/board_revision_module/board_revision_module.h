#pragma once

#include "modules/c_link.h"

typedef enum
{
    BOARD_REVISION_V1_I2S = 10042,
    BOARD_REVISION_V2_I2S = 10010,
    BOARD_REVISION_V3_I2S = 30010,
    BOARD_REVISION_V1_SLAVE = 0,
} board_revision_t;

C_LINK void board_revision_module_init(void);
C_LINK board_revision_t board_revision_module_get_revision(void);