#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "esp_err.h"
#include "esp_log.h"

#include "modules/web_server_module/websocket_handler/websocket_handler.h"
#include "modules/mesh_module/root/mesh_root_module.h"

#define PUBLISH_INTERVAL 1111

static float strength = 1.;
static TickType_t next_publish = 0;
__attribute__((unused)) static const char *TAG = "ONBOARD_LED_STRENGTH";

void onboard_led_strength_module_step(void)
{
    TickType_t current_tick = xTaskGetTickCount();
    if(next_publish < current_tick)
    {
        next_publish = current_tick + PUBLISH_INTERVAL;
        websocket_handler_publish_onboard_led_strength();
        
        send_to_all(ONBOARD_LED_STRENGTH_MESSAGE, &strength, sizeof(strength));
    }
}

void onboard_led_strength_module_set(float _strength)
{
    strength = _strength;
}

float onboard_led_strenght_module_get(void)
{
    return strength;
}

void onboard_led_strength_handle_message(light_mesh_message_t* msg)
{
    strength = *(float *)msg->payload;
}