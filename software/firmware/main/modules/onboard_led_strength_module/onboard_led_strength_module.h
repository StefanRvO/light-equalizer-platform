#include "modules/protocol_module/protocol_module.h"
#include "modules/c_link.h"

C_LINK void onboard_led_strength_module_step(void);
C_LINK void onboard_led_strength_module_set(float strength);
C_LINK float onboard_led_strenght_module_get(void);
C_LINK void onboard_led_strength_handle_message(light_mesh_message_t* msg);