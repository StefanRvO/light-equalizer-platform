#include "esp_err.h"
#include "esp_log.h"

#include "driver/gpio.h"

#include "power_supply_module.h"

static gpio_num_t pins[] = {14, 25};
void power_supply_module_init(void)
{
    #if defined(CONFIG_IDF_TARGET_ESP32) || defined(CONFIG_IDF_TARGET_ESP32S3)

    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = 0;
    for(uint8_t i = 0; i < sizeof(pins) / sizeof(*pins); i++)
    {
        io_conf.pin_bit_mask += 1 << pins[i];
    }
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    gpio_config(&io_conf);
    power_supply_module_set(false, POWER_SUPPLY_LEFT);
    power_supply_module_set(false, POWER_SUPPLY_RIGHT);
    #endif

}

void power_supply_module_set(bool state, power_supply_type_t supply)
{
    #if defined(CONFIG_IDF_TARGET_ESP32) || defined(CONFIG_IDF_TARGET_ESP32S3)


    if(supply < POWER_SUPPLY_CNT)
    {
        gpio_set_level(pins[supply], state);
    }

    #endif
}