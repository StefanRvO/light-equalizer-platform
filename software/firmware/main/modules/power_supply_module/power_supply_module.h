#pragma once

#include "modules/c_link.h"

typedef enum
{
    POWER_SUPPLY_LEFT,
    POWER_SUPPLY_RIGHT,
    POWER_SUPPLY_CNT
} power_supply_type_t;

C_LINK void power_supply_module_init(void);

C_LINK void power_supply_module_set(bool state, power_supply_type_t supply);