#include "sound_sampler_module.h"

#include "drivers/ak5720/ak5720.h"
#include "drivers/wm8782/wm8782.h"
#include "modules/gpio_utilities/gpio_utilities.h"
#include "modules/board_revision_module/board_revision_module.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/i2s.h"
#include "esp_err.h"
#include "esp_log.h"

#define I2S_NUM 0
#define READ_CHUNK_SIZE 128

#if defined(CONFIG_IDF_TARGET_ESP32) || defined(CONFIG_IDF_TARGET_ESP32S3)


__attribute__((unused)) static const char *TAG = "SOUND_SAMPLER_MODULE";


static const i2s_pin_config_t v1_v2_config =
{
    .bck_io_num   = 18,
    .ws_io_num    = 5,
    .data_out_num = -1,
    .data_in_num  = 17,
};

static const i2s_pin_config_t v3_config =
{
    .bck_io_num   = 17,
    .ws_io_num    = 16,
    .data_out_num = -1,
    .data_in_num  = 4,
};


static const i2s_config_t i2s_v1_config = {
     .mode = I2S_MODE_SLAVE | I2S_MODE_RX,
     .sample_rate = SAMPLE_RATE / 2,
     .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT,
     .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
     .communication_format = I2S_COMM_FORMAT_STAND_I2S,
     .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1, // default interrupt priority
     .dma_buf_count = 8,
     .dma_buf_len = READ_CHUNK_SIZE,
     .use_apll = true,
};

static const i2s_config_t i2s_v2_v3_config = {
     .mode = I2S_MODE_MASTER | I2S_MODE_RX,
     .sample_rate = SAMPLE_RATE,
     .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT,
     .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
     .communication_format = I2S_COMM_FORMAT_STAND_I2S,
     .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1, // default interrupt priority
     .dma_buf_count = 8,
     .dma_buf_len = READ_CHUNK_SIZE,
     .use_apll = true,
};


static const ak5720_pin_config_t ak5720_v1_pin_config = {
    .power_off_pin = 19,
    .filter_select_pin = 21,
    .gain_select_pin = 16,
};

static const wm8782_pin_config_t wm8782_v2_pin_config = {
    .fast_sample_enable_pin = 21,
    .format_select_pin = 19,
    .word_length_select_pin = 16,
};

static const wm8782_pin_config_t wm8782_v3_pin_config = {
    .fast_sample_enable_pin = 18,
    .format_select_pin = 19,
    .word_length_select_pin = 5,
};

static i2s_pin_config_t const* get_pin_config(void);
static void init_chip(void);
static void start_mclk(void);
static void set_i2s_config(void);

i2s_pin_config_t const* get_pin_config(void)
{
    switch(board_revision_module_get_revision())
    {
        case BOARD_REVISION_V1_I2S:
        case BOARD_REVISION_V2_I2S:
            return &v1_v2_config;
        case BOARD_REVISION_V3_I2S:
            return &v3_config;
        default:
            break;
    }
    return NULL;
}

void init_chip(void)
{
    switch(board_revision_module_get_revision())
    {
        case BOARD_REVISION_V1_I2S:
            start_mclk();
            ak5720_init(&ak5720_v1_pin_config);
            break;
        case BOARD_REVISION_V2_I2S:
            wm8782_init(&wm8782_v2_pin_config);
            start_mclk();
            break;
        case BOARD_REVISION_V3_I2S:
            wm8782_init(&wm8782_v3_pin_config);
            start_mclk();
            break;
        case BOARD_REVISION_V1_SLAVE:
            break;
    }
}

void set_i2s_config(void)
{
    switch(board_revision_module_get_revision())
    {
        case BOARD_REVISION_V1_I2S:
            ESP_ERROR_CHECK(i2s_driver_install(I2S_NUM, &i2s_v1_config, 0, NULL));
            break;
        case BOARD_REVISION_V2_I2S:
        case BOARD_REVISION_V3_I2S:
            ESP_ERROR_CHECK(i2s_driver_install(I2S_NUM, &i2s_v2_v3_config, 0, NULL));
            break;
        case BOARD_REVISION_V1_SLAVE:
            break;
    }

}

void sound_sampler_module_init(void)
{
    gpio_utilities_set_pin_low(0);
    vTaskDelay(5 / portTICK_RATE_MS);
    set_i2s_config();
    ESP_ERROR_CHECK(i2s_set_pin(I2S_NUM, get_pin_config()));
    init_chip();
}

void start_mclk(void)
{
    PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0_CLK_OUT1);
    #if I2S_NUM == 1
    REG_WRITE(PIN_CTRL, 0xF);
    #elif I2S_NUM == 0
    REG_WRITE(PIN_CTRL, 0x0);
    #endif
}

void sound_sampler_module_flush(void)
{
    int32_t buf[READ_CHUNK_SIZE];
    size_t read = sizeof(buf);
    while(read == sizeof(buf))
    {
        i2s_read(I2S_NUM, buf, sizeof(buf), &read, 0);
    }
}

void sound_sampler_module_read(sound_samples_t *samples)
{
    int32_t buf[READ_CHUNK_SIZE];
    size_t read = sizeof(buf);
    while(read == sizeof(buf))
    {
        read = 0;
        uint32_t free_space = (samples->buf_size - samples->channel_data[0].sample_cnt) * sizeof(int32_t);
        free_space += (samples->buf_size - samples->channel_data[1].sample_cnt) * sizeof(int32_t);
        if(free_space > sizeof(buf))
        {
            free_space = sizeof(buf);
        }
        i2s_read(I2S_NUM, buf, free_space, &read, 0);
        for(uint32_t i = 0; i < read / 4; i++)
        {
            channel_samples_t* channel_data = &samples->channel_data[samples->nxt_channel];
            uint32_t *sample_cnt = &channel_data->sample_cnt;

            float sample = (buf[i] >> 8) / (float)(1U << 23);

            channel_data->sample_buf[*sample_cnt] = sample;

            (*sample_cnt)++;
            samples->nxt_channel++;
            samples->nxt_channel %= 2;
        }
    }
}

#endif