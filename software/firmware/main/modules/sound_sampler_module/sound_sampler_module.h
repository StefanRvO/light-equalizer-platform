#pragma once

#include <stdint.h>


#define SOUND_CHANNEL_CNT 2
#define SOUND_BUFFER_SIZE 2048

#define SAMPLE_RATE 44100
// #define SAMPLE_RATE 5000

typedef float sound_buffer_datatype_t;

typedef struct
{
    sound_buffer_datatype_t sample_buf[SOUND_BUFFER_SIZE];
    uint32_t sample_cnt;
} channel_samples_t;

typedef struct {
    uint32_t buf_size;
    channel_samples_t channel_data[SOUND_CHANNEL_CNT];
    uint32_t nxt_channel; 
} sound_samples_t;

void sound_sampler_module_init(void);
void sound_sampler_module_flush(void);
void sound_sampler_module_read(sound_samples_t *samples);