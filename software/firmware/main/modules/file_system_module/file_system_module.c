#include "file_system_module.h"
#include "esp_err.h"
#include "esp_log.h"

#include "esp_vfs.h"
#include "esp_vfs_fat.h"
#include "vfs_fat_internal.h"
#include "diskio.h"

#include "diskio_rawflash.h"


#include <dirent.h>
#define MAX_FILES 10
#define FATFS_PARTITION_LABEL "storage"

__attribute__((unused)) static const char *TAG = "FILESYSTEM_MODULE";
const char *base_path = "/spifat";
static void list_all_files(char * path);

static wl_handle_t s_wl_handle = WL_INVALID_HANDLE;

static uint8_t fat_sha256[32];

int file_system_module_init(void)
{
    esp_vfs_fat_mount_config_t mount_config = {
                .max_files = 10,
                .format_if_mount_failed = true,
                .allocation_unit_size = CONFIG_WL_SECTOR_SIZE
        };
    esp_err_t err = esp_vfs_fat_spiflash_mount(base_path, FATFS_PARTITION_LABEL, &mount_config, &s_wl_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Failed to mount FATFS (%s)", esp_err_to_name(err));
        return err;
    }
    ESP_LOGI(TAG,"MOUNTED /spifat/");
    list_all_files("/spifat");
    return err;
}

void list_all_files(char* path)
{
    DIR* dir;
    struct dirent *ent;
    if((dir=opendir(path)) != NULL){
        while (( ent = readdir(dir)) != NULL){
            if(ent->d_type == DT_DIR && strcmp(ent->d_name, ".") != 0  && strcmp(ent->d_name, "..") != 0){
                ESP_LOGI(TAG,"%s\n", ent->d_name);
                list_all_files(ent->d_name);
            }
        }
        closedir(dir);
    }
}

void file_system_update_flash_sha256(void)
{
    const esp_partition_t * part = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, 
                                                            ESP_PARTITION_SUBTYPE_DATA_FAT, 
                                                            FATFS_PARTITION_LABEL);

    ESP_ERROR_CHECK(esp_partition_get_sha256(part, fat_sha256));
}

uint32_t file_system_read_fat(uint8_t* buf, size_t offset, size_t size)
{
    if(size == 0)
    {
        return 0;
    }
    const esp_partition_t * part = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, 
                                                            ESP_PARTITION_SUBTYPE_DATA_FAT, 
                                                            FATFS_PARTITION_LABEL);
    uint32_t bytes_remaining = part->size - offset;
    ESP_ERROR_CHECK(esp_partition_read(part, offset, buf, bytes_remaining > size ? size : bytes_remaining));
    return bytes_remaining > size ? size : bytes_remaining;
}


const uint8_t *file_system_get_sha256(void)
{
    return fat_sha256;
}

const esp_partition_t * file_system_get_target_partition(void)
{
    return esp_partition_find_first(ESP_PARTITION_TYPE_DATA, 
                                    ESP_PARTITION_SUBTYPE_DATA_FAT, 
                                    FATFS_PARTITION_LABEL);
}