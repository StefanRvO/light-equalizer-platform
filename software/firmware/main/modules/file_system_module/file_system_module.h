#pragma once
#include <stdint.h>
#include <stddef.h>
#include "esp_vfs_fat.h"
#include "modules/c_link.h"

C_LINK int file_system_module_init(void);
C_LINK const uint8_t *file_system_get_sha256(void);
C_LINK void file_system_update_flash_sha256(void);
C_LINK uint32_t file_system_read_fat(uint8_t* buf, size_t offset, size_t size);
C_LINK const esp_partition_t * file_system_get_target_partition(void);