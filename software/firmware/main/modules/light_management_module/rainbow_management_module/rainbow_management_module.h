#pragma once

#include "modules/protocol_module/protocol_module.h"

void rainbow_management_module_step(void);

void rainbow_management_module_set_update_rate(uint32_t _update_rate_ms);

uint32_t rainbow_management_module_get_update_rate(void);

void rainbow_management_module_handle_message(light_mesh_message_t* msg);