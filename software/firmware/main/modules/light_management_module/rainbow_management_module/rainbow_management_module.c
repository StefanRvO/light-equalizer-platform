#include "esp_err.h"
#include "esp_log.h"

#include "rainbow_management_module.h"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"
#include "modules/light_control_module/rainbow_control_module/rainbow_control_module.h"
#include "modules/mesh_module/root/mesh_root_module.h"

__attribute__((unused)) static const char *TAG = "RAINBOW_LIGHT_MANAGEMENT";


static uint32_t update_rate_ms = 100;

static TickType_t next_publish = 0;
#define PUBLISH_INTERVAL 1075

void rainbow_management_module_step(void)
{
    TickType_t current_tick = xTaskGetTickCount();
    if(next_publish < current_tick)
    {
        next_publish = current_tick + PUBLISH_INTERVAL;
        websocket_handler_publish_rainbow_settings();
        send_to_all(RAINBOW_MESSAGE, &update_rate_ms, sizeof(update_rate_ms));
        rainbow_light_control_module_set(update_rate_ms);
    }
}

void rainbow_management_module_set_update_rate(uint32_t _update_rate_ms)
{
    update_rate_ms = _update_rate_ms;
    next_publish = 0;
}

uint32_t rainbow_management_module_get_update_rate(void)
{
    return update_rate_ms;
}

void rainbow_management_module_handle_message(light_mesh_message_t* msg)
{
    rainbow_light_control_module_set(*(uint32_t*)msg->payload);
}