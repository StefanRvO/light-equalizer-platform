#include "esp_err.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "nvs.h"
#include <math.h>

#include "light_management_module.h"

#include "modules/protocol_module/protocol_module.h"
#include "modules/mesh_module/root/mesh_root_module.h"
#include "modules/light_control_module/light_control_module.hpp"
#include "modules/config_reader_module/config_reader_module.h"
#
#include "strobelight_management_module/strobelight_management_module.h"
#include "glitter_light_management_module/glitter_light_management_module.h"
#include "frequency_management_module/frequency_management_module.h"
#include "pulse_light_management_module/pulse_light_management_module.h"
#include "gol_management_module/gol_management_module.h"
#include "running_sections_management_module/running_sections_management_module.h"
#include "rainbow_management_module/rainbow_management_module.h"
#include "frequency_settings_management_module/frequency_settings_management_module.h"
#include "shooting_management_module/shooting_management_module.hpp"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"

static light_mode_type_t global_mode = LIGHT_MODE_OFF;
static TickType_t next_update = 0;
static float strength = 0.2F;
static pixelColor_t color = {.raw32 = 0x00000000};
__attribute__((unused)) static const char *TAG = "LIGHT_MANAGEMENT";

#define MODE_PUBLISH_INTERVAL 1000

static void publish_light_mode(void);
static void light_mode_step(void);
static void trigger_publish(void);

int light_management_module_init(void)
{
    int ret = 0;
    ret |= strobe_light_management_module_init();
    ret |= glitter_light_management_module_init();
           frequency_management_module_init();
    next_update = xTaskGetTickCount();

    global_mode = config_reader_get_uint32("global_mode", LIGHT_MODE_OFF);
    color.raw32 = config_reader_get_uint32("color", 0xFFFFFFFF);

    return 0;
}

void light_management_module_step(void)
{
    if(next_update < xTaskGetTickCount())
    {
        publish_light_mode();
        next_update = xTaskGetTickCount() + MODE_PUBLISH_INTERVAL;
    }
    light_mode_step();
}

void light_mode_step(void)
{
    strobe_light_management_module_step();
    glitter_light_management_module_step();
    pulse_light_management_module_step();
    gol_management_module_step();
    running_sections_management_module_step();
    rainbow_management_module_step();
    frequency_settings_management_step();
    shooting_management_module_step();
}

void light_management_set_global_mode(light_mode_type_t _mode)
{
    if(global_mode == _mode)
    {
        return;
    }
    global_mode  = _mode;
    config_reader_set_uint32("global_mode", global_mode);
}

void light_management_set_strength(float _strength)
{
    if(strength > 1 || strength < 0 )
    {
        return;
    }
    if(strength != 0 && !isnormal(strength))
    {
        return;
    }
    strength = _strength;
    trigger_publish();
}

void light_management_set_color(pixelColor_t _color)
{
    if(_color.raw32 == color.raw32)
    {
        return;
    }
    color = _color;
    config_reader_set_uint32("color", color.raw32);
    trigger_publish();
}

light_mode_type_t light_management_module_get_global_mode(void)
{
    return global_mode;
}

void light_management_module_get_light_mode(light_mode_type_t *_global_mode, float *_strength, pixelColor_t *_color)
{
    *_global_mode    = global_mode;
    *_strength       = strength;
    *_color          = color;
}

void trigger_publish(void)
{
    next_update = 0;
}

void publish_light_mode(void)
{
    light_mode_message_t light_mode_message = {.global_mode    = global_mode, 
                                               .strength = strength,
                                               .color = color};

    send_to_all(LIGHT_MODE_MESSAGE, &light_mode_message, sizeof(light_mode_message));
    websocket_handler_publish_light_mode();
    light_control_module_set_global_light_mode(global_mode, strength, color);
}