#pragma once
#include <sys/time.h>
#include "libs/strobe_lib/strobe_lib.h"


int strobe_light_management_module_init(void);

void strobe_light_management_module_step(void);

void strobe_light_management_module_set_specs(strobe_light_timing_spec_t *specs);