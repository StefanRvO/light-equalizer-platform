#include "esp_err.h"
#include "esp_log.h"

#include "strobelight_management_module.h"

#include "modules/protocol_module/protocol_module.h"
#include "modules/mesh_module/root/mesh_root_module.h"
#include "modules/light_control_module/strobe_light_control_module/strobe_light_control_module.h"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"

#define DEFAULT_STROBE_LIGHT_ON_US 50000
#define DEFAULT_STROBE_LIGHT_OFF_US 200000
#define US_PER_S 1000000
#define PUBLISH_INTERVAL 1010

static strobe_light_timing_spec_t strobe_light_timing_spec;
static TickType_t                 next_update = 0;

static void publish_specs(void);
static void trigger_publish(void);

int strobe_light_management_module_init(void)
{
    strobe_light_timing_spec.on_time.tv_sec   = 0;
    strobe_light_timing_spec.on_time.tv_usec  = DEFAULT_STROBE_LIGHT_ON_US;
    strobe_light_timing_spec.off_time.tv_sec  = 0;
    strobe_light_timing_spec.off_time.tv_usec = DEFAULT_STROBE_LIGHT_OFF_US;
    next_update = xTaskGetTickCount();
    return 0;
}

void strobe_light_management_module_step(void)
{
    if(next_update < xTaskGetTickCount())
    {
        publish_specs();
        next_update = xTaskGetTickCount() + 1000;
    }
}

void strobe_light_management_module_set_specs(strobe_light_timing_spec_t *specs)
{
    strobe_light_timing_spec = *specs;
    trigger_publish();
}

void trigger_publish(void)
{
    next_update = 0;
}

void publish_specs(void)
{
    send_to_all(STROBE_LIGHT_TIMING_SPEC_MESSAGE, &strobe_light_timing_spec, sizeof(strobe_light_timing_spec));
    websocket_handler_publish_strobe();
    strobe_light_spec_set(strobe_light_timing_spec);
}