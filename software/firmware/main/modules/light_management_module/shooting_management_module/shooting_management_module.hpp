#pragma once

#include "modules/protocol_module/protocol_module.h"
#include "modules/light_control_module/shooting_control_module/shooting_control_module.hpp"
#include "modules/c_link.h"

C_LINK void shooting_management_module_step(void);

C_LINK void shooting_management_module_set_settings(shooting_settings_t* _settings);

C_LINK shooting_settings_t shooting_management_module_get_settings(void);

C_LINK void shooting_management_module_handle_message(light_mesh_message_t* msg);