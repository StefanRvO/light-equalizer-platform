#include "esp_err.h"
#include "esp_log.h"

#include "shooting_management_module.hpp"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"
#include "modules/light_control_module/shooting_control_module/shooting_control_module.hpp"
#include "modules/mesh_module/root/mesh_root_module.h"

__attribute__((unused)) static const char *TAG = "RAINBOW_LIGHT_MANAGEMENT";


static shooting_settings_t settings = {
    .spawn_rate = 0.75,
    .fade_rate_min = 0.05,
    .fade_rate_max = 0.3,
    .speed_min = 10,
    .speed_max = 150,
};

static TickType_t next_publish = 0;
#define PUBLISH_INTERVAL 1001

void shooting_management_module_step(void)
{
    TickType_t current_tick = xTaskGetTickCount();
    if(next_publish < current_tick)
    {
        next_publish = current_tick + PUBLISH_INTERVAL;
        websocket_handler_publish_shooting_settings();
        send_to_all(SHOOTING_MESSAGE, &settings, sizeof(settings));
        shooting_control_module_set_settings(&settings);
    }
}

void shooting_management_module_set_settings(shooting_settings_t* _settings)
{
    settings = *_settings;
    next_publish = 0;
}

shooting_settings_t shooting_management_module_get_settings(void)
{
    return settings;
}

void shooting_management_module_handle_message(light_mesh_message_t* msg)
{
    shooting_control_module_set_settings((shooting_settings_t*)msg->payload);
}