#pragma once
#include <stdint.h>
#include <stdbool.h>
#include "drivers/ak5720/ak5720.h"
#include "kiss_fftr.h"
#include "modules/c_link.h"

#define BINS_TO_SEND 128

typedef struct {
    kiss_fft_scalar fft[2][BINS_TO_SEND];
    uint8_t seq_count;
} frequency_data_t;

typedef struct {
    uint16_t fft[2][BINS_TO_SEND];
    uint8_t seq_count;
} frequency_data_message_t;

C_LINK void frequency_management_module_init(void);

C_LINK void frequency_management_module_set_frequencies(kiss_fft_scalar* data, uint32_t channel, bool publish);

C_LINK void frequency_management_module_step(void);