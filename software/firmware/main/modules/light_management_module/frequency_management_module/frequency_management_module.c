#include "frequency_management_module.h"
#include "modules/mesh_module/root/mesh_root_module.h"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"
#include "modules/light_control_module/frequency_control_module/frequency_control_module.h"
#include "modules/light_management_module/light_management_module.h"
#include "libs/half_float/half_float.h"
#include "libs/mel_scale_lib/mel_scale_lib.h"
#include "esp_err.h"
#include "esp_log.h"
#include <math.h>

#define UPDATES_PER_PUBLISH 1

static frequency_data_t frequency_data = {0};
static uint32_t update_cnt = 0;

static void publish(void);

__attribute__((unused)) static const char *TAG = "FREQ_MANAGEMENT_MODULE";


void frequency_management_module_init(void)
{
    memset(&frequency_data, 0x00, sizeof(frequency_data));
}

void frequency_management_module_set_frequencies(kiss_fft_scalar* data, uint32_t channel, bool publish)
{
    uint32_t fft_bins = SOUND_BUFFER_SIZE/2;
    fft_to_mel_bins(data, frequency_data.fft[channel], fft_bins, BINS_TO_SEND, 16000);
    update_cnt++;
}

void frequency_management_module_step(void)
{
    if(update_cnt)
    {
        publish();
        update_cnt = 0;
    }
}

void publish(void)
{
    // ws_frequency_info_message_set_data(&frequency_data);    
    // websocket_handler_publish_frequency_info();

    frequency_data.seq_count++;
    frequency_data_message_t msg;
    float_to_half_float_arr(msg.fft[0], frequency_data.fft[0], BINS_TO_SEND);
    float_to_half_float_arr(msg.fft[1], frequency_data.fft[1], BINS_TO_SEND);

    msg.seq_count = frequency_data.seq_count;

    send_to_all(FREQUENCY_INFO_MESSAGE, &msg, sizeof(msg));
    frequency_control_module_set_frequencies(&frequency_data, frequency_data.seq_count);
    memset(&frequency_data.fft, 0x00, sizeof(frequency_data.fft));
}