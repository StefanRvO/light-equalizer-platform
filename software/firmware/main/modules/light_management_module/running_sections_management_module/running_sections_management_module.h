#pragma once
#include "modules/protocol_module/protocol_module.h"


void running_sections_management_module_step(void);

void running_sections_management_module_set(uint32_t _update_rate_ms, 
                                            uint32_t _section_count, 
                                            uint32_t _section_size);

void running_sections_management_module_get(uint32_t* _update_rate_ms, 
                                            uint32_t* _section_count, 
                                            uint32_t* _section_size);

void running_sections_management_module_handle_message(light_mesh_message_t* msg);