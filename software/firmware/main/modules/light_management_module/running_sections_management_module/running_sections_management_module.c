#include "esp_err.h"
#include "esp_log.h"

#include "running_sections_management_module.h"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"
#include "modules/light_control_module/running_sections_light_control_module/running_sections_light_control_module.h"
#include "modules/mesh_module/root/mesh_root_module.h"

__attribute__((unused)) static const char *TAG = "RUNNING_SECTIONS_LIGHT_MANAGEMENT";


static uint32_t update_rate_ms = 100;
static uint32_t section_count = 3;
static uint32_t section_size  = 3;

static TickType_t next_publish = 0;
#define PUBLISH_INTERVAL 975

void running_sections_management_module_step(void)
{
    TickType_t current_tick = xTaskGetTickCount();
    if(next_publish < current_tick)
    {
        next_publish = current_tick + PUBLISH_INTERVAL;
        websocket_handler_publish_running_sections_settings();
        running_sections_message_t msg;
        msg.update_rate_ms = update_rate_ms;
        msg.section_count  = section_count;
        msg.section_size   = section_size;
        send_to_all(RUNNING_SECTIONS_MESSAGE, &msg, sizeof(msg));
        running_sections_light_control_module_set(update_rate_ms, section_size, section_count);
    }
}

void running_sections_management_module_set(uint32_t _update_rate_ms, 
                                            uint32_t _section_count, 
                                            uint32_t _section_size)
{
    update_rate_ms = _update_rate_ms;
    section_count  = _section_count;
    section_size   = _section_size;
    next_publish = 0;
}

void running_sections_management_module_get(uint32_t* _update_rate_ms, 
                                            uint32_t* _section_count, 
                                            uint32_t* _section_size)
{
    *_update_rate_ms = update_rate_ms;
    *_section_count  = section_count;
    *_section_size   = section_size;
}

void running_sections_management_module_handle_message(light_mesh_message_t* msg)
{
    running_sections_message_t* _msg = (running_sections_message_t*)msg->payload;
    running_sections_light_control_module_set(_msg->update_rate_ms, _msg->section_size, _msg->section_count);
}