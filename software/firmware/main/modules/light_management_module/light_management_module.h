#pragma once
#include "esp32_digital_led_lib.h"
#include <stdbool.h>
#include "modules/c_link.h"

typedef enum
{
    LIGHT_MODE_OFF,
    LIGHT_MODE_ON,
    LIGHT_MODE_STROBE,
    LIGHT_MODE_GLITTER,
    LIGHT_MODE_FREQUENCY_PANEL,
    LIGHT_MODE_TETRIS,
    LIGHT_MODE_GAME_OF_LIFE,
    LIGHT_MODE_RUNNING_RING,
    LIGHT_MODE_PULSE,
    LIGHT_MODE_RUNNING_SECTIONS,
    LIGHT_MODE_RAINBOW,
    LIGHT_MODE_FREQUENCY_STRING,
    LIGHT_MODE_FREQUENCY_RING,
    LIGHT_MODE_FREQUENCY_SINGLE_COLOR,
    LIGHT_MODE_FREQUENCY_SINGLE_COLOR_BEAT,
    LIGHT_MODE_AUDIO_PULSE, //fade the color on and off. Change the fade speed depending on volume change. Make it possible to activate color change
    LIGHT_MODE_AUDIO_GLITTER, //Control glitter speed and on percent depening on volume or frequencies.
    LIGHT_MODE_MATRIX, //Make a matrix like look for panels with falling blocks.
    LIGHT_MODE_FREQUENCY_PANEL_FALLING, //Falling blocks with new blocks created depending on frequency power
    LIGHT_MODE_FREQUENCY_PANEL_BOUNCE, //Create a block for each frequency group and make it bounce. Bounce speed depends on power level of bin
    LIGHT_MODE_FREQUENCY_GLITTER_COLOR, //Create random glitter points. Number of points per color depends on frequency power
    LIGHT_MODE_FIRE, //Randomly create points of fire. Make the fire fade away from ignition point.
    LIGHT_MODE_FIRE_AUDIO, //Randomly create points of fire. Make the fire fade away from ignition point. Ignition points are spawned by frequency power.
    LIGHT_MODE_ENERGY, //Expand from center (off strip, panel or ring). Added colors depends on power and frequency.
    LIGHT_MODE_SHOOTING, //Shoot points along the led strip, with fading trails.
    LIGHT_MODE_MANDELBROT,
    LIGHT_MODE_CNT,
} light_mode_type_t;

C_LINK int light_management_module_init(void);
C_LINK void light_management_module_step(void);

C_LINK void light_management_set_local_mode(light_mode_type_t _mode);
C_LINK void light_management_set_global_mode(light_mode_type_t _mode);
C_LINK void light_management_set_is_global_mode(bool is_global);
C_LINK void light_management_set_strength(float _strength);
C_LINK void light_management_set_color(pixelColor_t _color);

C_LINK void light_management_module_get_light_mode(light_mode_type_t *_global_mode, float *_strength, pixelColor_t *_color);

C_LINK light_mode_type_t light_management_module_get_global_mode(void);