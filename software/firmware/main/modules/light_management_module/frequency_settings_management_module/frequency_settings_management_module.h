#pragma once

#include "modules/protocol_module/protocol_module.h"
#include "libs/frequency_control_lib/frequency_control_lib.h"
#include <stdbool.h>

void frequency_settings_management_step(void);

void frequency_settings_management_set_power_func(freq_power_func_t* func);

float frequency_settings_management_get_exponent(void);

float frequency_settings_management_get_power(void);

float frequency_settings_management_get_y_cross(void);

void frequency_settings_management_set_scaling(freq_scaling_t *_scaling);


void frequency_settings_management_set_decay_rate(float rate);
void frequency_settings_management_set_show_peaks(bool peaks);
void frequency_settings_management_set_show_current(bool current);

float frequency_settings_management_get_decay_rate(void);
bool frequency_settings_management_get_show_peaks(void);
bool frequency_settings_management_get_show_current(void);

float frequency_settings_management_get_log_base(void);

float frequency_settings_management_get_multiplier(void);

void frequency_settings_management_handle_message(light_mesh_message_t* msg);