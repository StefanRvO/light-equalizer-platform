#include "frequency_settings_management_module.h"

#include "modules/web_server_module/websocket_handler/websocket_handler.h"
#include "modules/light_control_module/frequency_control_module/frequency_control_module.h"
#include "modules/mesh_module/root/mesh_root_module.h"
#include "libs/frequency_control_lib/frequency_control_lib.h"
__attribute__((unused)) static const char *TAG = "FREQ_SETTINGS_MGMT";

#define PUBLISH_INTERVAL 1081

static TickType_t next_publish = 0;


static freq_power_func_t power_func = {.power = 16000, .exponent = 3, .y_cross = 0};
static freq_scaling_t scaling = {.log_base = 1.2, .multiplier = 1./28.};
static float peak_decay_rate  = 0.1;
static bool show_peaks   = true;
static bool show_current = true;

void frequency_settings_management_step(void)
{
    TickType_t current_tick = xTaskGetTickCount();
    if(next_publish < current_tick)
    {
        next_publish = current_tick + PUBLISH_INTERVAL;
        websocket_handler_publish_frequency_settings();
        frequency_settings_message_t msg;
        msg.exponent = power_func.exponent;
        msg.power    = power_func.power;
        msg.y_cross  = power_func.y_cross;

        msg.scaling_log_base = scaling.log_base;
        msg.scaling_divider  = 1 / scaling.multiplier;

        msg.peak_decay_rate = peak_decay_rate;
        msg.show_current = show_current;
        msg.show_peaks = show_peaks;

        send_to_all(FREQUENCY_SETTINGS_MESSAGE, &msg, sizeof(msg));
        frequency_control_module_set_power_func(&power_func);
        frequency_control_module_set_scaling(&scaling);
        frequency_control_module_set_decay_rate(peak_decay_rate);
        frequency_control_module_set_show_current(show_current);
        frequency_control_module_set_show_peaks(show_peaks);
    }

}

void frequency_settings_management_set_power_func(freq_power_func_t *func)
{
    power_func = *func;
    next_publish = 0;
}

void frequency_settings_management_set_scaling(freq_scaling_t *_scaling)
{
    scaling = *_scaling;
    next_publish = 0;
}

void frequency_settings_management_set_decay_rate(float rate)
{
    peak_decay_rate = rate;
}

void frequency_settings_management_set_show_peaks(bool peaks)
{
    show_peaks = peaks;
}

void frequency_settings_management_set_show_current(bool current)
{
    show_current = current;
}

float frequency_settings_management_get_decay_rate(void)
{
    return peak_decay_rate;
}

bool frequency_settings_management_get_show_peaks(void)
{
    return show_peaks;
}

bool frequency_settings_management_get_show_current(void)
{
    return show_current;
}

float frequency_settings_management_get_log_base(void)
{
    return scaling.log_base;
}

float frequency_settings_management_get_multiplier(void)
{
    return scaling.multiplier;
}

float frequency_settings_management_get_exponent(void)
{
    return power_func.exponent;
}

float frequency_settings_management_get_power(void)
{
    return power_func.power;
}

float frequency_settings_management_get_y_cross(void)
{
    return power_func.y_cross;
}


void frequency_settings_management_handle_message(light_mesh_message_t* msg)
{
    frequency_settings_message_t* freq_msg = (frequency_settings_message_t*)msg->payload;
    freq_power_func_t f;
    freq_scaling_t    s;
    f.power    = freq_msg->power;
    f.exponent = freq_msg->exponent;
    f.y_cross  = freq_msg->y_cross;
    s.log_base = freq_msg->scaling_log_base;
    s.multiplier = 1 / freq_msg->scaling_divider;
    frequency_control_module_set_power_func(&f);
    frequency_control_module_set_scaling(&s);
    frequency_control_module_set_decay_rate(freq_msg->peak_decay_rate);
    frequency_control_module_set_show_current(freq_msg->show_current);
    frequency_control_module_set_show_peaks(freq_msg->show_peaks);

}