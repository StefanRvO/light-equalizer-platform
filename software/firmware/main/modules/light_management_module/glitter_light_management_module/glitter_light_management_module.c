#include "esp_err.h"
#include "esp_log.h"

#include "glitter_light_management_module.h"

#include "modules/protocol_module/protocol_module.h"
#include "modules/mesh_module/root/mesh_root_module.h"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"

#define US_PER_S 1000000
#define PUBLISH_INTERVAL 1020

static glitter_light_data_t data;
static TickType_t           next_update = 0;

static void publish_specs(void);
static void trigger_publish(void);

int glitter_light_management_module_init(void)
{
    data.percent_on          = 50;
    data.update_rate.tv_sec  = 0;
    data.update_rate.tv_usec = 50000;
    data.random_color        = false;
    next_update              = xTaskGetTickCount();
    return 0;
}

void glitter_light_management_module_step(void)
{
    if(next_update < xTaskGetTickCount())
    {
        publish_specs();
        next_update = xTaskGetTickCount() + PUBLISH_INTERVAL;
    }
}

void glitter_light_management_module_set_data(glitter_light_data_t *_data)
{
    data = *_data;
    trigger_publish();
}

void glitter_light_management_module_get_data(glitter_light_data_t *_data)
{
    *_data = data;
}

void trigger_publish(void)
{
    next_update = 0;
}

void publish_specs(void)
{
    send_to_all(GLITTER_LIGHT_DATA_MESSAGE, &data, sizeof(data));
    glitter_light_control_module_set_data(&data);
    websocket_handler_publish_glitter();
}