#pragma once
#include "modules/light_control_module/glitter_light_control_module/glitter_light_control_module.h"

int glitter_light_management_module_init(void);

void glitter_light_management_module_step(void);

void glitter_light_management_module_set_data(glitter_light_data_t *data);

void glitter_light_management_module_get_data(glitter_light_data_t *_data);