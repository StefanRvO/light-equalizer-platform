#pragma once
#include "modules/light_control_module/pulse_light_control_module/pulse_light_control_module.h"
#include "modules/protocol_module/protocol_module.h"

void pulse_light_management_module_step(void);

void pulse_light_management_module_set_data(pulse_light_data_t *data);

void pulse_light_management_module_get_data(pulse_light_data_t *_data);

void pulse_light_management_module_handle_message(light_mesh_message_t* msg);
