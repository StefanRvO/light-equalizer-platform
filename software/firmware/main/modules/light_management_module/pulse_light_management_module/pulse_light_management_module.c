#include "esp_err.h"
#include "esp_log.h"

#include "pulse_light_management_module.h"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"
#include "modules/mesh_module/root/mesh_root_module.h"

__attribute__((unused)) static const char *TAG = "PULSE_LIGHT_MANAGEMENT";


static pulse_light_data_t pulse_data = {.pulse_time_ms = 1000, 
                                        .direction = 0,
                                        .single_shot = false
                                       };

static TickType_t next_publish = 0;
#define PUBLISH_INTERVAL 980

void pulse_light_management_module_step(void)
{
    TickType_t current_tick = xTaskGetTickCount();
    if(next_publish < current_tick)
    {
        next_publish = current_tick + PUBLISH_INTERVAL;
        websocket_handler_publish_pulse_settings();
        pulse_light_message_t msg;
        msg.direction     = pulse_data.direction;
        msg.pulse_time_ms = pulse_data.pulse_time_ms;
        msg.single_shot   = pulse_data.single_shot;
        send_to_all(PULSE_SETTINGS_MESSAGE, &msg, sizeof(msg));
        pulse_light_control_module_set(&pulse_data);

    }
}

void pulse_light_management_module_set_data(pulse_light_data_t *data)
{
    pulse_data = *data;
    next_publish = 0;

}

void pulse_light_management_module_get_data(pulse_light_data_t *data)
{
    *data = pulse_data;
}

void pulse_light_management_module_handle_message(light_mesh_message_t* msg)
{
    pulse_light_message_t* pulse_msg = (pulse_light_message_t*)msg->payload;
    pulse_data.pulse_time_ms = pulse_msg->pulse_time_ms;
    pulse_data.direction     = pulse_msg->direction;
    pulse_data.single_shot   = pulse_msg->single_shot;
    pulse_light_control_module_set(&pulse_data);
}