#include "esp_err.h"
#include "esp_log.h"

#include "gol_management_module.h"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"
#include "modules/mesh_module/root/mesh_root_module.h"

__attribute__((unused)) static const char *TAG = "GOL_LIGHT_MANAGEMENT";


static uint32_t update_rate = 80;

static TickType_t next_publish = 0;
#define PUBLISH_INTERVAL 1180

void gol_management_module_step(void)
{
    TickType_t current_tick = xTaskGetTickCount();
    if(next_publish < current_tick)
    {
        next_publish = current_tick + PUBLISH_INTERVAL;
        websocket_handler_publish_gol_settings();
        send_to_all(GAME_OF_LIFE_SETTINGS_MESSAGE, &update_rate, sizeof(update_rate));
        gol_light_control_module_set_update_rate(update_rate);
    }
}

void gol_management_module_set_update_rate(uint32_t update_rate_ms)
{
    update_rate = update_rate_ms;
    next_publish = 0;
}

uint32_t gol_management_module_get_update_rate(void)
{
    return update_rate;
}

void gol_management_module_handle_message(light_mesh_message_t* msg)
{
    update_rate = *(uint32_t*)msg->payload;
    gol_light_control_module_set_update_rate(update_rate);
}