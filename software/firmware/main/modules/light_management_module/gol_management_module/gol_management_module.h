#pragma once
#include "modules/light_control_module/gol_control_module/gol_control_module.h"
#include "modules/protocol_module/protocol_module.h"

void gol_management_module_step(void);

void gol_management_module_set_update_rate(uint32_t update_rate_ms);

uint32_t gol_management_module_get_update_rate(void);

void gol_management_module_handle_message(light_mesh_message_t* msg);
