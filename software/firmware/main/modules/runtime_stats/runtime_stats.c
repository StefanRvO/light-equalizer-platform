/**
 * @brief Timed printing system information
 */
#include "esp_err.h"
#include "esp_log.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>


static TickType_t next_print_time = 0;

void print_runtime_stats()
{
    TickType_t current_time = xTaskGetTickCount();
    if(current_time > next_print_time)
    {
        size_t heap = xPortGetFreeHeapSize();
        ESP_LOGI("HEAP", "free: %d", heap);
        uint32_t task_cnt = uxTaskGetNumberOfTasks();
        TaskStatus_t* tasks = malloc(sizeof(TaskStatus_t) * task_cnt);
        ESP_LOGI("TASKS", "%d %d %p", task_cnt, sizeof(TaskStatus_t) * task_cnt, tasks);
        uint32_t ulTotalRunTime;
        task_cnt = uxTaskGetSystemState( tasks,
                                         task_cnt,
                                         &ulTotalRunTime );
        for(uint8_t i = 0; i < task_cnt; i++)
        {
            ESP_LOGI("STATS", "%s: %d %d", tasks[i].pcTaskName,
                                           tasks[i].ulRunTimeCounter,
                                           tasks[i].usStackHighWaterMark);
        }
        next_print_time = current_time + 5000;
        free(tasks);
    }
}