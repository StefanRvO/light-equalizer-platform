#include "web_server_file_upload_api.h"
#include "esp_err.h"
#include "esp_log.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

__attribute__((unused)) static const char *TAG = "WEB_SERVER_UPLOAD";

static void handle_file_upload(struct mg_connection* nc, int ev, void *ev_data);
struct mg_str upload_fname(struct mg_connection* nc, struct mg_str fname);
static void handle_dir_create(struct mg_connection* nc, int ev, void *ev_data);
static int createdir(char *path);
static int remove_directory(const char *path, int depth);
static void handle_remove_dir(struct mg_connection* nc, int ev, void *ev_data);

int web_server_file_upload_api_init(struct mg_connection *nc)
{
    mg_register_http_endpoint(nc, "/api/file_upload", handle_file_upload);
    mg_register_http_endpoint(nc, "/api/dir_create", handle_dir_create);
    mg_register_http_endpoint(nc, "/api/dir_empty", handle_remove_dir);
    return 0;
}

static void handle_file_upload(struct mg_connection* nc, int ev, void *ev_data) 
{
    switch(ev)
    {
        case MG_EV_HTTP_MULTIPART_REQUEST:
        case MG_EV_HTTP_PART_BEGIN:
        case MG_EV_HTTP_PART_DATA:
        case MG_EV_HTTP_PART_END:
        case MG_EV_HTTP_MULTIPART_REQUEST_END:
            mg_file_upload_handler(nc, ev, ev_data, upload_fname);
            break;
    }
}

struct mg_str upload_fname(struct mg_connection* nc, struct mg_str fname)
{
    return fname;
}

void handle_dir_create(struct mg_connection* nc, int ev, void *ev_data) 
{
    char dirname[200];
    struct http_message *hm = (struct http_message *) ev_data;
    switch(ev)
    {
        case MG_EV_HTTP_REQUEST:
            if(strncmp(hm->method.p, "POST", hm->method.len) == 0)
            {
                if(mg_get_http_var(&hm->body, "dir", dirname, sizeof(dirname)) < 0)
                {
                    mg_http_send_error(nc, 400, "Directory name not in post data");
                    return;
                }
                if(createdir(dirname) != 0)
                {
                    mg_http_send_error(nc, 400, "Could not create directory");
                    return;
                }
                mg_http_send_error(nc, 200, "OK");
            }     
    }
}

void handle_remove_dir(struct mg_connection* nc, int ev, void *ev_data) 
{
    char dirname[200];
    struct http_message *hm = (struct http_message *) ev_data;
    switch(ev)
    {
        case MG_EV_HTTP_REQUEST:
            if(strncmp(hm->method.p, "POST", hm->method.len) == 0)
            {
                if(mg_get_http_var(&hm->body, "dir", dirname, sizeof(dirname)) < 0)
                {
                    mg_http_send_error(nc, 400, "Directory name not in post data");
                    return;
                }
                if(remove_directory(dirname, 0) != 0)
                {
                    mg_http_send_error(nc, 400, "Could not remove directory");
                    return;
                }
                mg_http_send_error(nc, 200, "OK");
            }     
    }
}

int createdir(char *path)
{
    ESP_LOGI(TAG,"Creating directory %s", path);
    struct stat st = {0};

    if (stat(path, &st) == -1) {
        return mkdir(path, 0777);
    }
    return 0;
}

int remove_directory(const char *path, int depth)
{
   DIR *d = opendir(path);
   size_t path_len = strlen(path);
   int r = 0;

   if (d)
   {
      struct dirent *p;

      r = 0;

      while (!r && (p=readdir(d)))
      {
          int r2 = -1;
          char *buf;
          size_t len;

          /* Skip the names "." and ".." as we don't want to recurse on them. */
          if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, ".."))
          {
             continue;
          }

          len = path_len + strlen(p->d_name) + 2; 
          buf = malloc(len);

          if (buf)
          {
             struct stat statbuf;

             snprintf(buf, len, "%s/%s", path, p->d_name);

             if (!stat(buf, &statbuf))
             {
                if (S_ISDIR(statbuf.st_mode))
                {
                   r2 = remove_directory(buf, depth + 1);
                }
                else
                {
                   r2 = unlink(buf);
                }
             }

             free(buf);
          }

          r = r2;
      }

      closedir(d);
   }

   if (!r && depth != 0)
   {
      r = rmdir(path);
   }

   return r;
}