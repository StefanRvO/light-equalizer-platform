#pragma once
#include "mongoose.h"

void web_server_static_module_init(void);
void web_server_static_module_serve(struct mg_connection *nc, struct http_message *hm);