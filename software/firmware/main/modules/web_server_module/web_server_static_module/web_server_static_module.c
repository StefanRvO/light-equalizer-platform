#include "web_server_static_module.h"
#include "esp_err.h"
#include "esp_log.h"


__attribute__((unused)) static const char *TAG = "WEB_SERVER_STATIC";

static const struct mg_serve_http_opts http_opts = {.document_root = "/spifat/",
                                                    .extra_headers = "Content-Encoding: gzip"};

void web_server_static_module_init(void)
{
}

void web_server_static_module_serve(struct mg_connection *nc, struct http_message *hm)
{
    char addr[32];
    mg_sock_addr_to_str(&nc->sa, addr, sizeof(addr),
                        MG_SOCK_STRINGIFY_IP | MG_SOCK_STRINGIFY_PORT);
    ESP_LOGI(TAG,"HTTP request from %s: %.*s %.*s\n", addr, (int) hm->method.len,
            hm->method.p, (int) hm->uri.len, hm->uri.p);
    if(false && hm->uri.len > 1)
    {
        //Rewrite to .gz -- Gzip compression
        struct mg_str modified_uri;
        char *uri = malloc(hm->uri.len + 3);
        modified_uri.p = uri;
        modified_uri.len = hm->uri.len + 3;
        strncpy(uri, hm->uri.p, hm->uri.len);
        uri[hm->uri.len]     = '.';
        uri[hm->uri.len + 1] = 'g';
        uri[hm->uri.len + 2] = 'z';
        hm->uri = modified_uri;
        ESP_LOGI(TAG,"HTTP request from %s: %.*s %.*s\n", addr, (int) hm->method.len,
                hm->method.p, (int) hm->uri.len, hm->uri.p);
        mg_serve_http(nc, hm, http_opts);
        free(uri);
    }
    else
    {
        mg_serve_http(nc, hm, http_opts);
    }
    nc->flags |= MG_F_SEND_AND_CLOSE;
}