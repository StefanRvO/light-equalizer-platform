#include "web_server_module.h"
#include "esp_err.h"
#include "esp_log.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>


#include "mongoose.h"
#include "web_server_static_module/web_server_static_module.h"
#include "web_server_file_upload_api/web_server_file_upload_api.h"

#include "websocket_handler/websocket_handler.h"

#define WEBSERVER_PORT "80"

__attribute__((unused)) static const char *TAG = "WEB_SERVER";

static struct mg_mgr mgr;
static struct mg_connection *nc;

static void ev_handler(struct mg_connection *nc, int ev, void *p);

int web_server_module_init(void)
{
    int ret = 0;
    mg_mgr_init(&mgr, NULL);
    nc = mg_bind(&mgr, WEBSERVER_PORT, ev_handler);
    if (nc == NULL) {
        printf("Error setting up listener!");
        return 1;
    }
    web_server_static_module_init();
    web_server_file_upload_api_init(nc);
    websocket_handler_init();
    ESP_LOGI(TAG,"Starting web-server on port %s", WEBSERVER_PORT);
    mg_set_protocol_http_websocket(nc);
    return ret;

}

void web_server_task(void *parameters)
{
  mg_mgr_poll(&mgr, 0);
  websocket_handler_step(nc);
}


void ev_handler(struct mg_connection *nc, int ev, void *p) {
  switch (ev) {
    case MG_EV_ACCEPT: {
      char addr[32];

      mg_sock_addr_to_str(&nc->sa, addr, sizeof(addr),
                          MG_SOCK_STRINGIFY_IP | MG_SOCK_STRINGIFY_PORT);
      ESP_LOGI(TAG,"Connection %p from %s", nc, addr);
      break;
    }
    case MG_EV_HTTP_REQUEST: {
      struct http_message *hm = (struct http_message *) p;
      web_server_static_module_serve(nc, hm);
      ESP_LOGD(TAG, "HTTP_REQUEST");
      break;
    }
    case MG_EV_WEBSOCKET_HANDSHAKE_DONE: {
      ESP_LOGD(TAG, "MG_EV_WEBSOCKET_HANDSHAKE_DONE");
      break;
    }
    case MG_EV_WEBSOCKET_FRAME: {
      ESP_LOGD(TAG, "MG_EV_WEBSOCKET_FRAME");
      struct websocket_message *wm = (struct websocket_message *) p;
      websocket_handler_handle_msg(nc, wm);
      break;
    }
    case MG_EV_CLOSE: {
      char addr[32];
      mg_sock_addr_to_str(&nc->sa, addr, sizeof(addr),
                          MG_SOCK_STRINGIFY_IP | MG_SOCK_STRINGIFY_PORT);
      ESP_LOGI(TAG,"Connection %p from %s closed", nc, addr);
      break;
    }
  }
}