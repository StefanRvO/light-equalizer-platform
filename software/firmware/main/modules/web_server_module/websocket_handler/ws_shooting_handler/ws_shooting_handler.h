#pragma once
#include <stdint.h>
#include <stddef.h>

size_t ws_shooting_handler_get_binary(uint8_t *buf, size_t buf_size);

void ws_shooting_handler_handle(uint8_t *buf, size_t buf_size);