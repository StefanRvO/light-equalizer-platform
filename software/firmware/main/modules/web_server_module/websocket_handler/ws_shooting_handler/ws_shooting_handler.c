#include "ws_shooting_handler.h"
#include "modules/light_management_module/shooting_management_module/shooting_management_module.hpp"
#include "esp_err.h"
#include "esp_log.h"

__attribute__((unused)) static const char *TAG = "WS_RAINBOW";


size_t ws_shooting_handler_get_binary(uint8_t *buf, size_t buf_size)
{
    *(shooting_settings_t *)buf = shooting_management_module_get_settings();
    return sizeof(shooting_settings_t);
}

void ws_shooting_handler_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= sizeof(shooting_settings_t))
    {
        shooting_management_module_set_settings((shooting_settings_t*)buf);
    }
}