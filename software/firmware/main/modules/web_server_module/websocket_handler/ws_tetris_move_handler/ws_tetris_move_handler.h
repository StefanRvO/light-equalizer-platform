#pragma once
#include <stdint.h>
#include "mongoose.h"

void ws_tetris_move_message_handler_handle(uint8_t *buf, size_t buf_size);
