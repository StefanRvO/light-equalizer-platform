#include "esp_err.h"
#include "esp_log.h"
#include "ws_tetris_move_handler.h"
#include "modules/light_control_module/tetris_control_module/tetris_control_module.h"


void ws_tetris_move_message_handler_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= 1)
    {
        tetris_control_module_set_move(buf[0]);
    }
}