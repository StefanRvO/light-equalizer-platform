#include "esp_err.h"
#include "esp_log.h"
#include "ws_pulse_light_handler.h"
#include "modules/light_management_module/pulse_light_management_module/pulse_light_management_module.h"

__attribute__((unused)) static const char *TAG = "WS_PULSE_LIGHT_HANDLER";


typedef struct __attribute__((packed))
{
    uint32_t pulse_time_ms;
    int8_t direction;
    uint8_t single_shot;
} ws_pulse_message_t;


size_t ws_pulse_light_handler_get_binary(uint8_t *buf, size_t buf_size)
{
    if(buf_size < sizeof(ws_pulse_message_t))
    {
        return 0;
    }
    ws_pulse_message_t* msg = (ws_pulse_message_t*) buf;
    pulse_light_data_t data;
    pulse_light_management_module_get_data(&data);
    msg->pulse_time_ms = data.pulse_time_ms;
    msg->direction     = data.direction;
    msg->single_shot   = data.single_shot;
    return sizeof(ws_pulse_message_t);
}

void ws_pulse_light_handler_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= sizeof(ws_pulse_message_t))
    {
        ws_pulse_message_t* msg = (ws_pulse_message_t *)buf;
        pulse_light_data_t data;
        data.pulse_time_ms = msg->pulse_time_ms;
        data.direction     = msg->direction;
        data.single_shot   = msg->single_shot;
        pulse_light_management_module_set_data(&data);
    }
}