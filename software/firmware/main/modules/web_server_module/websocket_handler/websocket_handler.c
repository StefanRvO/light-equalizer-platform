#include "websocket_handler.h"
#include "math.h"
#include "ws_light_mode_message_handler/ws_light_mode_message_handler.h"
#include "ws_strobe_message_handler/ws_strobe_message_handler.h"
#include "ws_glitter_message_handler/ws_glitter_message_handler.h"
#include "ws_node_info_message_handler/ws_node_info_message_handler.h"
#include "ws_tetris_move_handler/ws_tetris_move_handler.h"
#include "ws_wifi_settings_handler/ws_wifi_settings_handler.h"
#include "ws_ota_upgrade_handler/ws_ota_upgrade_handler.h"
#include "ws_pulse_light_handler/ws_pulse_light_handler.h"
#include "ws_onboard_led_strength_handler/ws_onboard_led_strength_handler.h"
#include "ws_gol_handler/ws_gol_handler.h"
#include "ws_running_sections/ws_running_sections.h"
#include "ws_rainbow_handler/ws_rainbow_handler.h"
#include "ws_frequency_settings_handler/ws_frequency_settings_handler.h"
#include "ws_shooting_handler/ws_shooting_handler.h"

#include "esp_err.h"
#include "esp_log.h"

__attribute__((unused)) static const char *TAG = "WS_HANDLER";

typedef struct
{
    bool new_light_mode;
    bool new_strobe;
    bool new_glitter;
    bool new_node_info;
    bool new_wifi_settings;
    bool new_pulse_settings;
    bool reset_ota;
    bool onboard_led_strength;
    bool ping;
    bool gol_settings;
    bool running_sections;
    bool rainbow;
    bool frequency_settings;
    bool shooting_settings;
} socket_handler_state_t;


static TickType_t next_update;
static socket_handler_state_t socket_handler_state = {0};


static void send_light_mode(struct mg_connection *c);
static void send_glitter(struct mg_connection *nc);
static void send_strobe(struct mg_connection *nc);
static void send_node_info(struct mg_connection *nc);
static void send_wifi_settings(struct mg_connection *nc);
static void send_reset_ota(struct mg_connection *nc);
static void send_pulse_settings(struct mg_connection *nc);
static void send_onboard_led_strength(struct mg_connection *nc);
static void send_ping(struct mg_connection *nc);
static void send_gol_settings(struct mg_connection *nc);
static void send_running_sections(struct mg_connection *nc);
static void send_rainbow(struct mg_connection * nc);
static void send_frequency_settings(struct mg_connection* nc);
static void send_shooting_settings(struct mg_connection* nc);

static void send_data(struct mg_connection *nc, uint8_t* buf, size_t len);
static void ping_handler(uint8_t* buf, size_t buf_size);

typedef void(*message_handler_t)(uint8_t* buf, size_t buf_size);

static const message_handler_t message_handlers[WEBSOCKET_PACKAGE_TYPE_CNT] = {
    [LIGHT_MODE]            = ws_light_mode_message_handler_handle,
    [STROBE]                = ws_strobe_message_handler_handle,
    [GLITTER]               = ws_glitter_message_handler_handle,
    [NODE_INFO]             = ws_node_info_message_handler_handle,
    [FREQUENCY_INFO]        = NULL,
    [SAMPLES]               = NULL,
    [PING]                  = ping_handler,
    [TETRIS_MOVE]           = ws_tetris_move_message_handler_handle,
    [WIFI_SETTINGS]         = ws_wifi_settings_handler_handle,
    [SCAN_WIFI]             = ws_wifi_settings_handler_handle_scan,
    [OTA_DATA]              = ws_ota_upgrade_handler_handle,
    [PULSE_SETTINGS]        = ws_pulse_light_handler_handle,
    [ONBOARD_LED_STRENGTH]  = ws_onboard_led_strength_handler_handle,
    [GAME_OF_LIFE_SETTINGS] = ws_gol_handler_handle,
    [RUNNING_SECTIONS]      = ws_running_sections_handle,
    [RAINBOW]               = ws_rainbow_handler_handle,
    [FREQUENCY_SETTINGS]    = ws_frequency_settings_handler_handle,
    [SHOOTING_SETTINGS]     = ws_shooting_handler_handle,
};


static int is_websocket(const struct mg_connection *nc) {
  return nc->flags & MG_F_IS_WEBSOCKET;
}

void websocket_handler_init(void)
{
    next_update = xTaskGetTickCount();
}

void websocket_handler_handle_msg(struct mg_connection *nc, struct websocket_message *wm)
{
    if(wm->size)
    {
        ws_package_type_t type = wm->data[0];
        if(type < WEBSOCKET_PACKAGE_TYPE_CNT)
        {
            message_handlers[type](wm->data + 1, wm->size - 1);
        }
    }
}

void ping_handler(uint8_t* buf, size_t buf_size)
{
    socket_handler_state.ping = true;
}

void websocket_handler_step(struct mg_connection *nc)
{
    if(socket_handler_state.new_light_mode)
    {
        send_light_mode(nc);
    }
    if(socket_handler_state.new_strobe)
    {
        send_strobe(nc);
    }
    if(socket_handler_state.new_glitter)
    {
        send_glitter(nc);
    }
    if(socket_handler_state.new_node_info)
    {
        send_node_info(nc);
    }
    if(socket_handler_state.new_wifi_settings)
    {
        send_wifi_settings(nc);
    }
    if(socket_handler_state.reset_ota)
    {
        send_reset_ota(nc);
    }
    if(socket_handler_state.new_pulse_settings)
    {
        send_pulse_settings(nc);
    }
    if(socket_handler_state.onboard_led_strength)
    {
        send_onboard_led_strength(nc);
    }
    if(socket_handler_state.ping)
    {
        send_ping(nc);
    }
    if(socket_handler_state.gol_settings)
    {
        send_gol_settings(nc);
    }
    if(socket_handler_state.running_sections)
    {
        send_running_sections(nc);
    }
    if(socket_handler_state.rainbow)
    {
        send_rainbow(nc);
    }
    if(socket_handler_state.frequency_settings)
    {
        send_frequency_settings(nc);
    }
    if(socket_handler_state.shooting_settings)
    {
        send_shooting_settings(nc);
    }
}

void websocket_handler_publish_light_mode(void)
{
    socket_handler_state.new_light_mode = true;
}

void websocket_handler_publish_audio_samples(void)
{
    //socket_handler_state.new_samples = true;
}

void websocket_handler_publish_strobe(void)
{
    socket_handler_state.new_strobe = true;
}

void websocket_handler_publish_glitter(void)
{
    socket_handler_state.new_glitter = true;
}

void websocket_handler_publish_node_info(void)
{
    socket_handler_state.new_node_info = true;
}

void websocket_handler_publish_frequency_info(void)
{
    //socket_handler_state.new_frequency_info = true;
}

void websocket_handler_publish_wifi_settings(void)
{
    socket_handler_state.new_wifi_settings = true;
}

void websocket_handler_reset_ota(void)
{
    socket_handler_state.reset_ota = true;
}

void websocket_handler_publish_pulse_settings(void)
{
    socket_handler_state.new_pulse_settings = true;
}

void websocket_handler_publish_onboard_led_strength(void)
{
    socket_handler_state.onboard_led_strength = true;
}

void websocket_handler_publish_gol_settings(void)
{
    socket_handler_state.gol_settings = true;
}

void websocket_handler_publish_running_sections_settings(void)
{
    socket_handler_state.running_sections = true;
}

void websocket_handler_publish_rainbow_settings(void)
{
    socket_handler_state.rainbow = true;
}

void websocket_handler_publish_frequency_settings(void)
{
    socket_handler_state.frequency_settings = true;
}

void websocket_handler_publish_shooting_settings(void)
{
    socket_handler_state.shooting_settings = true;
}

void send_light_mode(struct mg_connection *nc)
{
    socket_handler_state.new_light_mode = false;
    uint8_t buf[100];
    buf[0] = LIGHT_MODE;
    size_t len = ws_light_mode_message_handler_get_binary(buf + 1, sizeof(buf) - 1);
    send_data(nc, buf, len + 1);
}

void send_strobe(struct mg_connection *nc)
{
    socket_handler_state.new_strobe = false;
    uint8_t buf[100];
    buf[0] = STROBE;
    size_t len = ws_strobe_message_handler_get_binary(buf + 1, sizeof(buf) - 1);
    send_data(nc, buf, len + 1);
}

void send_glitter(struct mg_connection *nc)
{
    socket_handler_state.new_glitter = false;
    uint8_t buf[100];
    buf[0] = GLITTER;
    size_t len = ws_glitter_message_handler_get_binary(buf + 1, sizeof(buf) - 1);
    send_data(nc, buf, len + 1);
}

void send_node_info(struct mg_connection *nc)
{
    socket_handler_state.new_node_info = false;
    uint8_t buf[2500];
    buf[0] = NODE_INFO;
    size_t len = ws_node_info_message_handler_get_binary(buf + 1, sizeof(buf) - 1);
    send_data(nc, buf, len + 1);
}

void send_wifi_settings(struct mg_connection *nc)
{
    socket_handler_state.new_wifi_settings = false;
    uint8_t buf[1001];
    buf[0] = WIFI_SETTINGS;
    size_t len = ws_wifi_settings_handler_get_binary(buf + 1, sizeof(buf) - 1);
    send_data(nc, buf, len + 1);
}

void send_reset_ota(struct mg_connection *nc)
{
    socket_handler_state.reset_ota = false;
    uint8_t buf[1001];
    buf[0] = OTA_DATA;
    send_data(nc, buf, 1);
}

void send_pulse_settings(struct mg_connection *nc)
{
    socket_handler_state.new_pulse_settings = false;
    uint8_t buf[1001];
    buf[0] = PULSE_SETTINGS;
    size_t len = ws_pulse_light_handler_get_binary(buf + 1, sizeof(buf) - 1);
    send_data(nc, buf, len + 1);
}

void send_onboard_led_strength(struct mg_connection *nc)
{
    socket_handler_state.onboard_led_strength = false;
    uint8_t buf[1001];
    buf[0] = ONBOARD_LED_STRENGTH;
    size_t len = ws_onboard_led_strength_handler_get_binary(buf + 1, sizeof(buf) - 1);
    send_data(nc, buf, len + 1);
}

void send_ping(struct mg_connection *nc)
{
    socket_handler_state.ping = false;
    uint8_t buf[1001];
    buf[0] = PING;
    send_data(nc, buf, 1);
}

void send_gol_settings(struct mg_connection *nc)
{
    socket_handler_state.gol_settings = false;
    uint8_t buf[1001];
    buf[0] = GAME_OF_LIFE_SETTINGS;
    size_t len = ws_gol_handler_get_binary(buf + 1, sizeof(buf) - 1);
    send_data(nc, buf, len + 1);
}

void send_running_sections(struct mg_connection *nc)
{
    socket_handler_state.running_sections = false;
    uint8_t buf[1001];
    buf[0] = RUNNING_SECTIONS;
    size_t len = ws_running_sections_get_binary(buf + 1, sizeof(buf) - 1);
    send_data(nc, buf, len + 1);
}

void send_data(struct mg_connection *nc, uint8_t* buf, size_t len)
{
    struct mg_connection *c;
    for (c = mg_next(nc->mgr, NULL); c != NULL; c = mg_next(nc->mgr, c)) {
        if(is_websocket(c))
        {
            mg_send_websocket_frame(c, WEBSOCKET_OP_BINARY, buf, len);
        }
    }
}

void send_rainbow(struct mg_connection *nc)
{
    socket_handler_state.rainbow = false;
    uint8_t buf[1001];
    buf[0] = RAINBOW;
    size_t len = ws_rainbow_handler_get_binary(buf + 1, sizeof(buf) - 1);
    send_data(nc, buf, len + 1);
}

void send_frequency_settings(struct mg_connection* nc)
{
    socket_handler_state.frequency_settings = false;
    uint8_t buf[1001];
    buf[0] = FREQUENCY_SETTINGS;
    size_t len = ws_frequency_settings_handler_get_binary(buf + 1, sizeof(buf) - 1);
    send_data(nc, buf, len + 1);
}

void send_shooting_settings(struct mg_connection* nc)
{
    socket_handler_state.shooting_settings = false;
    uint8_t buf[1001];
    buf[0] = SHOOTING_SETTINGS;
    size_t len = ws_shooting_handler_get_binary(buf + 1, sizeof(buf) - 1);
    send_data(nc, buf, len + 1);
}