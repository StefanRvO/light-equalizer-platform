#pragma once

#include <stdint.h>

size_t ws_light_mode_message_handler_get_binary(uint8_t *buf, size_t buf_size);

void ws_light_mode_message_handler_handle(uint8_t *buf, size_t buf_size);