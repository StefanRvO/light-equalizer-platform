#include "esp_err.h"
#include "esp_log.h"
#include "ws_light_mode_message_handler.h"
#include "modules/light_management_module/light_management_module.h"

__attribute__((unused)) static const char *TAG = "WS_LIGHT_MODE";


typedef struct __attribute__((packed))
{
    uint8_t mode;
    float strength;
    uint8_t r;
    uint8_t g;
    uint8_t b;
} ws_light_mode_message_t;


size_t ws_light_mode_message_handler_get_binary(uint8_t *buf, size_t buf_size)
{
    if(buf_size < sizeof(ws_light_mode_message_t))
    {
        return 0;
    }
    ws_light_mode_message_t* msg = (ws_light_mode_message_t*) buf;
    buf = (uint8_t*)msg;

    pixelColor_t color;
    light_mode_type_t mode;
    float strength;
    light_management_module_get_light_mode(&mode, &strength, &color);
    msg->r = color.r;
    msg->g = color.g;
    msg->b = color.b;
    msg->mode = mode;
    msg->strength = strength;
    return sizeof(ws_light_mode_message_t);
}

void ws_light_mode_message_handler_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= sizeof(ws_light_mode_message_t))
    {
        ws_light_mode_message_t* msg = (ws_light_mode_message_t *)buf;
        pixelColor_t color = {.r = msg->r, .g = msg->g, .b = msg->b};
        float strength = msg->strength;
        light_mode_type_t mode = msg->mode;
        light_management_set_global_mode(mode);
        light_management_set_strength(strength);
        light_management_set_color(color);
    }
}