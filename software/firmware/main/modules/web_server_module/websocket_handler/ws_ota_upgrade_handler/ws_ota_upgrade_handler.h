#pragma once

#include <stdint.h>
#include <stddef.h>

void ws_ota_upgrade_handler_handle(uint8_t *buf, size_t buf_size);