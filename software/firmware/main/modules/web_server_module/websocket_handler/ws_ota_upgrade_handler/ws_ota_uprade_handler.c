#include "ws_ota_upgrade_handler.h"
#include "modules/firmware_sync_module/firmware_sync_module.h"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"

#include "libs/string_format_lib/string_format_lib.h"

#include "esp_err.h"
#include "esp_log.h"
#include "esp_ota_ops.h"

__attribute__((unused)) static const char *TAG = "WS_OTA_UPGRADE";

typedef struct __attribute__((packed))
{
    uint32_t total_size;
    uint32_t offset;
    uint32_t size;
    uint8_t data[1500];

} ws_ota_upgrade_data_t;


void ws_ota_upgrade_handler_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= sizeof(ws_ota_upgrade_data_t))
    {
        ws_ota_upgrade_data_t *msg = (ws_ota_upgrade_data_t*)buf;
        if(msg->size > sizeof(msg->data))
        {
            ESP_LOGI(TAG, "Size was larger than allowed: %d", msg->size);
            return;
        }

        if(msg->offset == 0)
        {
            ESP_LOGI(TAG, "OTA Upgrade started");
            firmware_sync_module_ota_erase();
        }

        if(msg->offset != firmware_sync_module_get_receive_offset())
        {
            websocket_handler_reset_ota();
        }
        else
        {
            firmware_sync_module_write_firmware(msg->data, msg->offset, msg->size);
            if(msg->offset + msg->size >= msg->total_size)
            {
                const esp_partition_t * part = esp_ota_get_next_update_partition(NULL);
                ESP_ERROR_CHECK(esp_ota_set_boot_partition(part));
                esp_restart();
            }
        }  
    }
}