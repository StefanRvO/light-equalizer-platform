#pragma once

#include <stdint.h>
#include <stddef.h>

size_t ws_running_sections_get_binary(uint8_t *buf, size_t buf_size);
void ws_running_sections_handle(uint8_t *buf, size_t buf_size);