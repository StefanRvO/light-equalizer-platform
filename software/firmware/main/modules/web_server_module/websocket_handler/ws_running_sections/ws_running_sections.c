#include "esp_err.h"
#include "esp_log.h"
#include "ws_running_sections.h"
#include "modules/light_management_module/running_sections_management_module/running_sections_management_module.h"

__attribute__((unused)) static const char *TAG = "WS_RUNNING_SECTIONS";


typedef struct __attribute__((packed))
{
    uint32_t period_time_ms;
    uint32_t section_count;
    uint32_t section_size;
} ws_running_sections_message_t;


size_t ws_running_sections_get_binary(uint8_t *buf, size_t buf_size)
{
    if(buf_size < sizeof(ws_running_sections_message_t))
    {
        return 0;
    }
    ws_running_sections_message_t* msg = (ws_running_sections_message_t*) buf;
    running_sections_management_module_get(&msg->period_time_ms, &msg->section_count, &msg->section_size);
    return sizeof(ws_running_sections_message_t);
}

void ws_running_sections_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= sizeof(ws_running_sections_message_t))
    {
        ws_running_sections_message_t* msg = (ws_running_sections_message_t *)buf;
        running_sections_management_module_set(msg->period_time_ms, msg->section_count, msg->section_size);
    }
}