#include "ws_onboard_led_strength_handler.h"
#include "modules/onboard_led_strength_module/onboard_led_strength_module.h"
#include "esp_err.h"
#include "esp_log.h"

__attribute__((unused)) static const char *TAG = "WS_ONBOARD_LED_STRENGTH";


size_t ws_onboard_led_strength_handler_get_binary(uint8_t *buf, size_t buf_size)
{
    *(float *)buf = onboard_led_strenght_module_get();
    return sizeof(float);
}

void ws_onboard_led_strength_handler_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= sizeof(float))
    {
        onboard_led_strength_module_set(*(float*)buf);
    }
}