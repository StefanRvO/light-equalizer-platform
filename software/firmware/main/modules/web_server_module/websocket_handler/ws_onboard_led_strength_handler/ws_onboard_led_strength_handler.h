#pragma once
#include <stdint.h>
#include <stddef.h>

size_t ws_onboard_led_strength_handler_get_binary(uint8_t *buf, size_t buf_size);
void ws_onboard_led_strength_handler_handle(uint8_t *buf, size_t buf_size);