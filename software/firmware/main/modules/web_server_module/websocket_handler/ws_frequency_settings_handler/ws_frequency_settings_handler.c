#include "ws_frequency_settings_handler.h"
#include "modules/light_management_module/frequency_settings_management_module/frequency_settings_management_module.h"
#include "modules/protocol_module/protocol_module.h"
#include "esp_err.h"
#include "esp_log.h"

__attribute__((unused)) static const char *TAG = "WS_FREQ_SETTINGS";


size_t ws_frequency_settings_handler_get_binary(uint8_t *buf, size_t buf_size)
{
    frequency_settings_message_t* msg = (frequency_settings_message_t *)buf;
    msg->power = frequency_settings_management_get_power();
    msg->exponent = frequency_settings_management_get_exponent();
    msg->y_cross  = frequency_settings_management_get_y_cross();
    msg->scaling_divider = 1 / frequency_settings_management_get_multiplier();
    msg->scaling_log_base = frequency_settings_management_get_log_base();
    msg->peak_decay_rate = frequency_settings_management_get_decay_rate();
    msg->show_current = frequency_settings_management_get_show_current();
    msg->show_peaks   = frequency_settings_management_get_show_peaks();
    return sizeof(*msg);
}

void ws_frequency_settings_handler_handle(uint8_t *buf, size_t buf_size)
{
    frequency_settings_message_t* msg = (frequency_settings_message_t *)buf;
    freq_power_func_t f;
    freq_scaling_t scaling;
    if(buf_size >= sizeof(*msg))
    {
        f.exponent = msg->exponent;
        f.power    = msg->power;
        f.y_cross  = msg->y_cross;
        scaling.log_base = msg->scaling_log_base;
        scaling.multiplier = 1 / msg->scaling_divider;
        frequency_settings_management_set_power_func(&f);
        frequency_settings_management_set_scaling(&scaling);
        frequency_settings_management_set_decay_rate(msg->peak_decay_rate);
        frequency_settings_management_set_show_current(msg->show_current);
        frequency_settings_management_set_show_peaks(msg->show_peaks);
    }
}