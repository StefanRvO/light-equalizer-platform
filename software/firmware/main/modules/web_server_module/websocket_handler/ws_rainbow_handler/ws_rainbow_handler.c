#include "ws_rainbow_handler.h"
#include "modules/light_management_module/rainbow_management_module/rainbow_management_module.h"
#include "esp_err.h"
#include "esp_log.h"

__attribute__((unused)) static const char *TAG = "WS_RAINBOW";


size_t ws_rainbow_handler_get_binary(uint8_t *buf, size_t buf_size)
{
    *(uint32_t *)buf = rainbow_management_module_get_update_rate();
    return sizeof(uint32_t);
}

void ws_rainbow_handler_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= sizeof(uint32_t))
    {
        rainbow_management_module_set_update_rate(*(uint32_t*)buf);
    }
}