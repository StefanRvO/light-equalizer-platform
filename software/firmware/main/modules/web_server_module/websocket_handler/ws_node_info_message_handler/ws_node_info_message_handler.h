#pragma once
#include <stdint.h>

size_t ws_node_info_message_handler_get_binary(uint8_t *buf, size_t buf_size);
void ws_node_info_message_handler_handle(uint8_t *buf, size_t buf_size);