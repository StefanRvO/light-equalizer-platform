#include "esp_err.h"
#include "esp_log.h"
#include "ws_node_info_message_handler.h"
#include "modules/mesh_module/node_info_module/node_info_module.h"

typedef struct __attribute__((packed))
{
    uint32_t node_cnt;
} ws_node_info_message_header_t;

__attribute__((unused)) static const char *TAG = "WS_HANDLER_NODE_INFO";

#define MAX_NODE_INFO_CNT 20

size_t ws_node_info_message_handler_get_binary(uint8_t *buf, size_t buf_size)
{
    size_t max_size = sizeof(ws_node_info_message_header_t) + sizeof(ws_node_info_message_piece_t) * MAX_NODE_INFO_CNT;
    if(buf_size < max_size)
    {
        ESP_LOGW(TAG,"%d, %d", buf_size, max_size);
        return 0;
    }
    size_t info_cnt;
    ws_node_info_message_piece_t* node_info_pieces = (ws_node_info_message_piece_t*)(buf + sizeof(ws_node_info_message_header_t));
    node_info_module_get_info_pieces(node_info_pieces, MAX_NODE_INFO_CNT, &info_cnt);
    ((ws_node_info_message_header_t*)buf)->node_cnt = info_cnt;

    return sizeof(ws_node_info_message_header_t) + sizeof(ws_node_info_message_piece_t) *info_cnt;
}

void ws_node_info_message_handler_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= sizeof(ws_node_info_message_piece_t))
    {
        ws_node_info_message_piece_t* msg = (ws_node_info_message_piece_t *)buf;
        node_info_hash_entry_t info;
        memcpy(info.mac_address, msg->mac_addr, sizeof(info.mac_address));
        msg->name_len = msg->name_len > sizeof(info.node_info.node_name) -1 ? sizeof(info.node_info.node_name) -1: msg->name_len;
        strncpy(info.node_info.node_name, msg->node_name, msg->name_len);
        info.node_info.node_name[msg->name_len] = '\0';
        info.node_info.strobe_offset.tv_sec  = msg->strobe_offset / 1000000;
        info.node_info.strobe_offset.tv_usec = msg->strobe_offset % 1000000;
        info.node_info.led_length          = msg->led_length;
        info.node_info.local_mode            = msg->local_mode;
        info.node_info.is_mode_global        = msg->is_mode_global;
        info.node_info.panel_height     = msg->panel_height;
        info.node_info.panel_width      = msg->panel_width;
        info.node_info.light_type            = msg->light_type;
        info.node_info.color_order      = msg->color_order;
        info.node_info.channel_order         = msg->channel_order;

        memcpy(&info.node_info.ring,  &msg->ring,  sizeof(msg->ring));
        node_info_module_set_info(&info);
    }
}