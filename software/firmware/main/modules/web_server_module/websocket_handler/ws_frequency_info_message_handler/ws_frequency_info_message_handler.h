#pragma once

#include <stdint.h>
#include "mongoose.h"
#include "modules/light_management_module/frequency_management_module/frequency_management_module.h"

size_t ws_frequency_info_message_handler_get_binary(uint8_t *buf, size_t buf_size, uint32_t channel);
void ws_frequency_info_message_handler_handle(uint8_t *buf, size_t buf_size);
void ws_frequency_info_message_set_data(frequency_data_t *data);