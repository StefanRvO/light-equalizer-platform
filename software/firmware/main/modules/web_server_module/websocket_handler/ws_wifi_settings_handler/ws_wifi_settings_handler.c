#include "ws_wifi_settings_handler.h"
#include "modules/mesh_module/mesh_module.h"
#include "modules/config_reader_module/config_reader_module.h"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"

#include "esp_err.h"
#include "esp_log.h"

#include <string.h>

__attribute__((unused)) static const char *TAG = "WS_WIFI_SETTINGS";


typedef struct __attribute__((packed))
{
    uint16_t scan_cnt;
    uint8_t  is_connected;
    uint8_t  ssid_len;
    uint8_t  pass_len;
    char sta_ssid[32];
    char sta_pass[64];
} ws_wifi_settings_header_t;

typedef struct __attribute__((packed))
{
    uint8_t bssid[6];
    uint8_t ssid_len;
    char ssid[33];
    uint8_t channel;
    uint8_t second_chan;
    int8_t rssi;
    uint8_t authmode;
} ws_wifi_settings_piece_t;


typedef struct __attribute__((packed))
{
    uint8_t ssid_len;
    uint8_t pass_len;
    char sta_ssid[32];
    char sta_pass[64];
} ws_wifi_settings_command_t;


size_t ws_wifi_settings_handler_get_binary(uint8_t *buf, size_t buf_size)
{
    ws_wifi_settings_header_t *header = (ws_wifi_settings_header_t*)buf;
    const wifi_ap_record_t* scan_info = mesh_module_get_scan_info(&header->scan_cnt);
    size_t size = sizeof(ws_wifi_settings_header_t) + sizeof(ws_wifi_settings_piece_t) * header->scan_cnt;
    if(buf_size < size)
    {
        ESP_LOGI(TAG,"%d, %d", buf_size, size);
        return 0;
    }
    char *sta_ssid;
    char *sta_pass;
    config_reader_get_sta(&sta_ssid, &sta_pass);
    
    memcpy((uint8_t *) header->sta_ssid, sta_ssid, sizeof(header->sta_ssid));
    memcpy((uint8_t *) header->sta_pass, sta_pass, sizeof(header->sta_pass));

    header->is_connected = mesh_is_started();
    header->ssid_len = strlen(sta_ssid);
    header->pass_len = strlen(sta_pass);

    buf += sizeof(ws_wifi_settings_header_t);
    for(uint16_t i = 0; i < header->scan_cnt; i++)
    {
        ws_wifi_settings_piece_t *piece = (ws_wifi_settings_piece_t*)buf;
        memcpy(piece->bssid, scan_info[i].bssid, sizeof(piece->bssid));
        memcpy(piece->ssid,  scan_info[i].ssid,  sizeof(piece->ssid));
        piece->channel     = scan_info[i].primary;
        piece->second_chan = scan_info[i].second;
        piece->rssi        = scan_info[i].rssi;
        piece->authmode    = scan_info[i].authmode;
        piece->ssid_len    = strlen((char*)scan_info[i].ssid);

        buf += sizeof(ws_wifi_settings_piece_t);
    }
    return size;
}

void ws_wifi_settings_handler_handle_scan(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= 1 && buf[0])
    {
        mesh_module_wifi_scan_step();
    }
    websocket_handler_publish_wifi_settings();
}
void ws_wifi_settings_handler_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= sizeof(ws_wifi_settings_command_t))
    {
        ws_wifi_settings_command_t *cmd = (ws_wifi_settings_command_t*)buf;
        if(cmd->pass_len > sizeof(cmd->sta_pass) -1 || cmd->ssid_len > sizeof(cmd->sta_ssid))
        {
            return;
        }
        cmd->sta_pass[cmd->pass_len] = '\0';
        cmd->sta_ssid[cmd->ssid_len] = '\0';
        config_reader_set_sta(cmd->sta_ssid, cmd->sta_pass);

        //Restart for now after setting new station.
        //We could potentially re-configure on the fly,
        //But the effect will likely not be much different
        //As we will have the restart wifi.
        esp_restart();
    }
}