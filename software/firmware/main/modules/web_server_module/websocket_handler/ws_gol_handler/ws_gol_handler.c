#include "esp_err.h"
#include "esp_log.h"
#include "ws_gol_handler.h"
#include "modules/light_management_module/gol_management_module/gol_management_module.h"

__attribute__((unused)) static const char *TAG = "WS_GOL_HANDLER";


size_t ws_gol_handler_get_binary(uint8_t *buf, size_t buf_size)
{
    if(buf_size < sizeof(float))
    {
        return 0;
    }
    uint32_t* msg = (uint32_t*) buf;
    *msg = gol_management_module_get_update_rate();
    return sizeof(*msg);
}

void ws_gol_handler_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= sizeof(uint32_t))
    {
        uint32_t* msg = (uint32_t *)buf;
        gol_management_module_set_update_rate(*msg);
    }
}