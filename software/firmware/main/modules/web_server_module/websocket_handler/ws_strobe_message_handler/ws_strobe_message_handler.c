#include "esp_err.h"
#include "esp_log.h"
#include "ws_strobe_message_handler.h"
#include "modules/light_control_module/strobe_light_control_module/strobe_light_control_module.h"


typedef struct __attribute__((packed))
{
    uint32_t on_time;
    uint32_t off_time;
} ws_strobe_message_t;


size_t ws_strobe_message_handler_get_binary(uint8_t *buf, size_t buf_size)
{
    if(buf_size < sizeof(ws_strobe_message_t))
    {
        return 0;
    }
    ws_strobe_message_t* msg = (ws_strobe_message_t*) buf;
    buf = (uint8_t*)msg;

    strobe_light_timing_spec_t spec = strobe_light_get_spec();

    msg->on_time  = spec.on_time.tv_sec  * 1000000 + spec.on_time.tv_usec;
    msg->off_time = spec.off_time.tv_sec * 1000000 + spec.off_time.tv_usec;

    return sizeof(ws_strobe_message_t);
}

void ws_strobe_message_handler_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= sizeof(ws_strobe_message_t))
    {
        ws_strobe_message_t* msg = (ws_strobe_message_t *)buf;
        strobe_light_timing_spec_t strobe_timing;
        strobe_timing.on_time.tv_sec   = msg->on_time / 1000000;
        strobe_timing.on_time.tv_usec  = msg->on_time % 1000000;
        strobe_timing.off_time.tv_sec  = msg->off_time / 1000000;
        strobe_timing.off_time.tv_usec = msg->off_time % 1000000;

        strobe_light_management_module_set_specs(&strobe_timing);
    }
}