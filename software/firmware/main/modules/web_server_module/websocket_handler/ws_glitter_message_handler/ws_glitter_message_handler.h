#pragma once

#include <stdint.h>
#include "mongoose.h"

size_t ws_glitter_message_handler_get_binary(uint8_t *buf, size_t buf_size);
void ws_glitter_message_handler_handle(uint8_t *buf, size_t buf_size);