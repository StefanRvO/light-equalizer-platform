#include "esp_err.h"
#include "esp_log.h"
#include "ws_glitter_message_handler.h"
#include "modules/light_management_module/glitter_light_management_module/glitter_light_management_module.h"

__attribute__((unused)) static const char *TAG = "WS_GLITTER";

typedef struct __attribute__((packed))
{
    uint32_t update_rate;
    bool random_color;
    float percent_on;
} ws_glitter_message_t;


size_t ws_glitter_message_handler_get_binary(uint8_t *buf, size_t buf_size)
{
    if(buf_size < sizeof(ws_glitter_message_t))
    {
        return 0;
    }
    ws_glitter_message_t* msg = (ws_glitter_message_t*) buf;
    buf = (uint8_t*)msg;

    glitter_light_data_t data;
    glitter_light_management_module_get_data(&data);

    msg->update_rate  = data.update_rate.tv_sec  * 1000000 + data.update_rate.tv_usec;
    msg->random_color = data.random_color;
    msg->percent_on   = data.percent_on;

    return sizeof(ws_glitter_message_t);
}

void ws_glitter_message_handler_handle(uint8_t *buf, size_t buf_size)
{
    if(buf_size >= sizeof(ws_glitter_message_t))
    {
        ws_glitter_message_t* msg = (ws_glitter_message_t *)buf;
        glitter_light_data_t data;
        data.update_rate.tv_sec   = msg->update_rate / 1000000;
        data.update_rate.tv_usec  = msg->update_rate % 1000000;
        data.random_color         = msg->random_color;
        data.percent_on           = msg->percent_on;

        glitter_light_management_module_set_data(&data);
    }
}