#pragma once

#include "mongoose.h"
#include "modules/c_link.h"

typedef enum
{
    LIGHT_MODE,
    STROBE,
    GLITTER,
    NODE_INFO,
    FREQUENCY_INFO,
    SAMPLES,
    TETRIS_MOVE,
    WIFI_SETTINGS,
    SCAN_WIFI,
    OTA_DATA,
    PING,
    PULSE_SETTINGS,
    ONBOARD_LED_STRENGTH,
    GAME_OF_LIFE_SETTINGS,
    RUNNING_SECTIONS,
    RAINBOW,
    FREQUENCY_SETTINGS,
    SHOOTING_SETTINGS,
    WEBSOCKET_PACKAGE_TYPE_CNT,
} ws_package_type_t;

void websocket_handler_init(void);

void websocket_handler_handle_msg(struct mg_connection *nc, struct websocket_message *wm);

void websocket_handler_step(struct mg_connection *nc);
void websocket_handler_publish_light_mode(void);
void websocket_handler_publish_strobe(void);
void websocket_handler_publish_glitter(void);
void websocket_handler_publish_node_info(void);
void websocket_handler_publish_frequency_info(void);
void websocket_handler_publish_audio_samples(void);
void websocket_handler_publish_wifi_settings(void);
void websocket_handler_publish_onboard_led_strength(void);
void websocket_handler_publish_gol_settings(void);
void websocket_handler_publish_running_sections_settings(void);
void websocket_handler_publish_frequency_settings(void);

C_LINK void websocket_handler_reset_ota(void);
C_LINK void websocket_handler_publish_pulse_settings(void);
C_LINK void websocket_handler_publish_rainbow_settings(void);
C_LINK void websocket_handler_publish_shooting_settings(void);