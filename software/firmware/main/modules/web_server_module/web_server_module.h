#pragma once
#include "modules/c_link.h"

C_LINK int web_server_module_init(void);
C_LINK void web_server_task(void *parameters);