#include "node_info_module.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_mesh.h"
#include "esp_system.h"

#include "modules/config_reader_module/config_reader_module.h"
#include "modules/light_control_module/strobe_light_control_module/strobe_light_control_module.h"
#include "modules/light_control_module/light_control_module.hpp"
#include "modules/protocol_module/protocol_module.h"
#include "modules/mesh_module/node/mesh_node_module.h"
#include "modules/mesh_module/root/mesh_root_module.h"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"
#include "modules/firmware_sync_module/firmware_sync_module.h"
#include "modules/filesystem_sync_module/filesystem_sync_module.h"
#include "modules/led_strip_module/led_strip_module.hpp"
#include "nvs_flash.h"
#include "nvs.h"

#define CLEANUP_INTERVAL_MS 1000
#define PUBLISH_INTERVAL_MS 1000

static node_info_hash_entry_t *node_info_ht = NULL;
static TickType_t next_cleanup = 0;
static SemaphoreHandle_t hash_table_sem = NULL;
static TickType_t next_publish = 0;

__attribute__((unused)) static const char *TAG = "NODE_INFO";

void trigger_publish(void);

#define NODE_TIMEOUT_S 10

void node_info_module_init(void)
{
    hash_table_sem = xSemaphoreCreateBinary();
    xSemaphoreGive(hash_table_sem);
}


void node_info_module_handle_info_message(light_mesh_message_t *msg)
{   //Update the node info on this node and reset last connect time
    node_info_hash_entry_t *node_info_entry = malloc(sizeof(node_info_hash_entry_t));
    memcpy(&node_info_entry->node_info, msg->payload, msg->payload_len);
    memcpy(node_info_entry->mac_address, node_info_entry->node_info.mac_address, sizeof(node_info_entry->mac_address));
    node_info_entry->last_contact  = xTaskGetTickCount();
    gettimeofday(&node_info_entry->received_time, NULL);
    node_info_hash_entry_t *node_info_replaced = NULL;

    xSemaphoreTake(hash_table_sem, portMAX_DELAY);
    HASH_REPLACE(hh, node_info_ht, mac_address, sizeof(node_info_entry->mac_address), node_info_entry, node_info_replaced);
    xSemaphoreGive(hash_table_sem);

    if(node_info_replaced)
    {
        free(node_info_replaced);
    }
}

void node_info_module_get_infos(node_info_hash_entry_t *infos, size_t max_nodes, size_t *nodes)
{
    size_t index = 0;
    node_info_hash_entry_t* current, *tmp;
    xSemaphoreTake(hash_table_sem, portMAX_DELAY);
    HASH_ITER(hh, node_info_ht, current, tmp)
    {
        if(index >= max_nodes)
        {
            break;
        }
        memcpy(&infos[index], current, sizeof(*current));
        index++;
    }
    xSemaphoreGive(hash_table_sem);
    *nodes = index;
}

void node_info_module_get_info_pieces(ws_node_info_message_piece_t* pieces, size_t max_nodes, size_t *nodes)
{
    size_t index = 0;
    node_info_hash_entry_t* current, *tmp;
    ws_node_info_message_piece_t *node_info_piece = pieces;
    xSemaphoreTake(hash_table_sem, portMAX_DELAY);
    HASH_ITER(hh, node_info_ht, current, tmp)
    {
        if(index >= max_nodes)
        {
            break;
        }
        memcpy(node_info_piece->mac_addr,    current->mac_address, sizeof(node_info_piece->mac_addr));
        memcpy(node_info_piece->node_name,   current->node_info.node_name, sizeof(current->node_info.node_name));
        memcpy(node_info_piece->parent_addr, current->node_info.parent_address, sizeof(node_info_piece->parent_addr));

        node_info_piece->name_len           = strlen(node_info_piece->node_name);
        node_info_piece->node_time          = current->node_info.node_time;
        node_info_piece->mean_air_time      = current->node_info.mean_air_time;
        node_info_piece->board_type         = current->node_info.board_type;
        node_info_piece->strobe_offset      = current->node_info.strobe_offset.tv_sec * 1000000 + current->node_info.strobe_offset.tv_usec;
        node_info_piece->led_length       = current->node_info.led_length;
        node_info_piece->layer              = current->node_info.layer;
        node_info_piece->local_mode         = current->node_info.local_mode;
        node_info_piece->is_mode_global     = current->node_info.is_mode_global;
        node_info_piece->panel_height  = current->node_info.panel_height;
        node_info_piece->panel_width   = current->node_info.panel_width;
        node_info_piece->light_type         = current->node_info.light_type;
        node_info_piece->color_order   = current->node_info.color_order;
        node_info_piece->fw_flash_progress  = current->node_info.fw_flash_progress;
        node_info_piece->fs_flash_progress  = current->node_info.fs_flash_progress;
        node_info_piece->channel_order      = current->node_info.channel_order;

        memcpy(&node_info_piece->ring,  &current->node_info.ring,  sizeof(node_info_piece->ring));
        index++;
        node_info_piece++;
    }
    xSemaphoreGive(hash_table_sem);
    *nodes = index;

}

void node_info_module_send_info(void)
{
    if(xTaskGetTickCount() > next_publish)
    {
        node_info_t node_info;
        pixelColor_t color;
        light_mode_type_t global_mode;
        float strength;
        mesh_addr_t parent_bssid;
        esp_mesh_get_parent_bssid(&parent_bssid);

        light_management_module_get_light_mode(&global_mode, 
                                               //&node_info.local_mode,
                                               //&node_info.is_mode_global, 
                                               &strength, 
                                               &color);

        node_info.node_time         = esp_mesh_get_tsf_time();
        strncpy(node_info.node_name, config_reader_get_name(), sizeof(node_info.node_name) -1);
        node_info.board_type        = config_reader_get_board_type();
        node_info.strobe_offset     = strobe_light_control_module_get_offset();
        node_info.layer             = esp_mesh_get_layer();
        node_info.light_type        = config_reader_get_light_type();
        node_info.fw_flash_progress = firmware_sync_get_synced_part();
        node_info.fs_flash_progress = filesystem_sync_get_synced_part();
        node_info.channel_order     = led_module_get_channel_order();

        memcpy(node_info.parent_address, parent_bssid.addr, sizeof(node_info.parent_address));
        esp_efuse_mac_get_default(node_info.mac_address);
        led_module_get_led_len(&node_info.led_length);
        led_module_get_panel_size(&node_info.panel_height, &node_info.panel_width);
        led_module_get_ring(&node_info.ring);
        color_order_t order;
        led_module_get_color_order(&order);
        node_info.color_order  = order;
        node_info.local_mode = light_control_module_get_local_mode();
        node_info.is_mode_global = light_control_module_is_mode_global();

        if(config_reader_is_root())
        {
            root_send_to_root(NODE_INFO_MESSAGE, &node_info, sizeof(node_info));
        }
        else
        {
            send_to_root(NODE_INFO_MESSAGE, &node_info, sizeof(node_info));
        }
        websocket_handler_publish_node_info();
        next_publish = xTaskGetTickCount() + PUBLISH_INTERVAL_MS;
    }
}

void node_info_module_set_info(node_info_hash_entry_t *info)
{
    uint8_t root_addr[6] = {0};
    esp_efuse_mac_get_default(root_addr);
    int res = memcmp(root_addr, info->mac_address, sizeof(root_addr));
    if(res)
    {   
        send_to_node(NODE_INFO_SET_MESSAGE, &info->node_info, sizeof(info->node_info), info->mac_address);
    }
    else
    {
        root_send_to_root(NODE_INFO_SET_MESSAGE, &info->node_info, sizeof(info->node_info));
    }
    trigger_publish();
}

void node_info_module_handle_set_info_message(light_mesh_message_t *msg)
{
    node_info_t *node_info = (node_info_t*)msg->payload;

    strobe_light_control_module_set_offset(&node_info->strobe_offset);
    led_module_set_led_len(node_info->led_length);
    led_module_set_panel_size(node_info->panel_height,  node_info->panel_width);
    led_module_set_color_order(node_info->color_order);

    light_control_set_local_mode(node_info->local_mode);
    light_control_set_is_global_mode(node_info->is_mode_global);
    led_module_set_ring(&node_info->ring, false);
    led_module_set_channel_order(node_info->channel_order);
    config_reader_set_light_type(node_info->light_type);
    config_reader_set_name(node_info->node_name);
}

void node_cleanup_step(void)
{
    if(xTaskGetTickCount() > next_cleanup)
    {
        node_info_hash_entry_t* current, *tmp;
        xSemaphoreTake(hash_table_sem, portMAX_DELAY);
        TickType_t cur_tick = xTaskGetTickCount();
        HASH_ITER(hh, node_info_ht, current, tmp)
        {
            if(cur_tick - current->last_contact > NODE_TIMEOUT_S * 1000)
            {
                ESP_LOGD(TAG,"Cleaned " MACSTR, MAC2STR(current->mac_address));
                HASH_DEL(node_info_ht, current);
                free(current);
            }
        }
        xSemaphoreGive(hash_table_sem);
        next_cleanup = xTaskGetTickCount() + CLEANUP_INTERVAL_MS;
    }
}

void trigger_publish(void)
{
    next_publish = 0;
}