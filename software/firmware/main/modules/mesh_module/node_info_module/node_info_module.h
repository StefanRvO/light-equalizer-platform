#pragma once
#include <sys/time.h>
#include <stdint.h>
#include <uthash.h>
#include "modules/protocol_module/protocol_module.h"
#include "modules/config_reader_module/config_reader_module.h"
#include "modules/light_management_module/light_management_module.h"
#include "libs/led_ring/led_ring.h"
#include "modules/c_link.h"

typedef struct
{
    uint64_t node_time;
    float mean_air_time;
    char node_name[20];
    board_type_t board_type;
    struct timeval strobe_offset;
    uint16_t led_length;
    uint8_t layer;
    light_mode_type_t local_mode;
    bool is_mode_global;
    uint8_t mac_address[6];
    uint8_t parent_address[6];
    uint8_t panel_height;
    uint8_t panel_width;
    led_ring_t ring;
    uint8_t light_type;
    uint8_t color_order;
    float fw_flash_progress;
    float fs_flash_progress;
    uint8_t channel_order;
} node_info_t;

typedef struct __attribute__((packed))
{
    uint8_t mac_addr[6];
    uint8_t parent_addr[6];
    double node_time;
    float mean_air_time;
    uint8_t name_len;
    char node_name[20];
    uint8_t board_type;
    uint32_t strobe_offset;
    uint16_t led_length;
    uint8_t local_mode;
    uint8_t is_mode_global;
    uint8_t layer;
    uint8_t panel_height;
    uint8_t panel_width;
    led_ring_t ring;
    uint8_t light_type;
    uint8_t color_order;
    float fw_flash_progress;
    float fs_flash_progress;
    uint8_t channel_order;
} ws_node_info_message_piece_t;


typedef struct 
{
    uint8_t mac_address[6];
    node_info_t node_info;
    TickType_t last_contact;
    struct timeval received_time;
    UT_hash_handle hh;
} node_info_hash_entry_t;

C_LINK void node_info_module_init(void);
C_LINK void node_info_module_handle_info_message(light_mesh_message_t *msg);
C_LINK void node_cleanup_step(void);
C_LINK void node_info_module_get_infos(node_info_hash_entry_t *infos, size_t max_nodes, size_t *nodes);
C_LINK void node_info_module_get_info_pieces(ws_node_info_message_piece_t* pieces, size_t max_nodes, size_t *nodes);
C_LINK void node_info_module_send_info(void);
C_LINK void node_info_module_handle_set_info_message(light_mesh_message_t *msg);
C_LINK void node_info_module_set_info(node_info_hash_entry_t *info);