#include "esp_err.h"
#include "esp_log.h"

#include "modules/protocol_module/protocol_module.h"
#include "modules/protocol_module/message_handler/message_handler_module.h"
#include "esp_mesh.h"
#include "modules/mesh_module/mesh_module.h"
#include <string.h>

#define QUEUE_LENGHT 5

__attribute__((unused)) static const char *TAG = "MESH MODULE_ROOT";

static void root_send_task(void *arg);

static QueueHandle_t root_send_queue    = NULL;

int mesh_root_module_init(void)
{
    message_handle_module_init();
    root_send_queue    = xQueueCreate(QUEUE_LENGHT, sizeof(light_mesh_message_t));
    ESP_ERROR_CHECK(esp_mesh_set_type(MESH_ROOT));  
    xTaskCreatePinnedToCore(root_send_task,    "root_send_task", 2 * 1024,
                NULL, 20, NULL, 0);

    return 0;
}


void root_receive_task(void)
{
    uint8_t buf[1500];
    mesh_data_t data;
    data.data   = buf;
    data.size   = 1500;
    data.proto  = MESH_PROTO_BIN;
    data.tos    = MESH_TOS_DEF;
    int flag = 0;
    mesh_addr_t src;
    light_mesh_message_t light_mesh_msg;
    if (!mesh_is_started()) {
        return;
    }

    data.size = 1500;
    esp_err_t ret = esp_mesh_recv(&src, &data, 0, &flag, NULL, 0);
    if(ret == ESP_ERR_MESH_TIMEOUT)
    {
        return;
    }
    if(ret != ESP_OK)
    {
        ESP_LOGI(TAG, "Failed to recv: %s",  esp_err_to_name(ret));
    }
    else
    {
        protocol_module_esp_mesh_to_light_mesh(&light_mesh_msg, &src, data.size, data.data);
        message_handler_module_handle_message(&light_mesh_msg);
    }
}

void root_send_task(void *arg)
{
    mesh_data_t data;
    data.data                        = malloc(1500);
    data.size                        = 1500;
    data.proto                       = MESH_PROTO_BIN;
    data.tos                         = MESH_TOS_DEF;

    mesh_addr_t dest_addr;
    light_mesh_message_t light_mesh_msg;
    ESP_LOGD(TAG,"Root is running");
    while(1) {
        xQueueReceive(root_send_queue, &light_mesh_msg, portMAX_DELAY);
        if (!(mesh_is_started() && esp_mesh_get_total_node_num() > 1) ) {
            vTaskDelay(5 / portTICK_RATE_MS);
            free(light_mesh_msg.payload);
            continue;
        }

        protocol_module_light_mesh_to_esp_mesh(&light_mesh_msg, &dest_addr, &data.size, data.data);
        free(light_mesh_msg.payload);
        esp_mesh_send(&dest_addr, &data, MESH_DATA_P2P | MESH_DATA_NONBLOCK | MESH_DATA_FROMDS, NULL, 0);
    }
    ESP_LOGW(TAG, "Root receive is exit");

    free(data.data);
    vTaskDelete(NULL);
}

void send_to_node(light_mesh_message_type_t message_type, void *payload, uint16_t payload_len, uint8_t *node_addr)
{
    light_mesh_message_t light_mesh_msg;
    memcpy(light_mesh_msg.dest_id, node_addr, 6);
    light_mesh_msg.type                  = (uint32_t)message_type;
    light_mesh_msg.payload               = malloc(payload_len);
    memcpy(light_mesh_msg.payload, payload, payload_len);
    light_mesh_msg.payload_len           = payload_len;
    if(xQueueSendToBack(root_send_queue,  &light_mesh_msg, 0) != pdTRUE)
    {
        free(light_mesh_msg.payload);
    }
}

void root_send_to_root(light_mesh_message_type_t message_type, void *payload, uint16_t payload_len)
{
    light_mesh_message_t light_mesh_msg;
    light_mesh_msg.type                  = (uint32_t)message_type;
    light_mesh_msg.payload               = malloc(payload_len);
    memcpy(light_mesh_msg.payload, payload, payload_len);
    light_mesh_msg.payload_len           = payload_len;
    message_handler_module_handle_message(&light_mesh_msg);
}

void send_to_all(light_mesh_message_type_t message_type, void *payload, uint16_t payload_len)
{
    light_mesh_message_t light_mesh_msg;
    for(uint8_t i = 0; i < 6; i++)         light_mesh_msg.dest_id[i] = 0xFF;
    light_mesh_msg.type                  = (uint32_t)message_type;
    //TODO: Skip sending if too many packages are queued.
    light_mesh_msg.payload               = malloc(payload_len);
    if(light_mesh_msg.payload == NULL)
    {
        ESP_LOGI(TAG, "malloc() failed to allocate %d bytes", payload_len);
        return;
    }
    memcpy(light_mesh_msg.payload, payload, payload_len);
    light_mesh_msg.payload_len = payload_len;
    if(xQueueSendToBack(root_send_queue,  &light_mesh_msg, 0) != pdTRUE)
    {
        free(light_mesh_msg.payload);
    }
}