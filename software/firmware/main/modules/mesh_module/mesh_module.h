#pragma once
#include "esp_wifi_types.h"
#include "modules/c_link.h"

C_LINK void print_wifi_mode(void);

C_LINK int mesh_module_init(void);
C_LINK bool mesh_is_connected(void);
C_LINK bool mesh_is_started(void);
C_LINK void mesh_module_handle_disconnected_timeout(void);
C_LINK void mesh_module_wifi_scan_step(void);
C_LINK const wifi_ap_record_t* mesh_module_get_scan_info(uint16_t *cnt);