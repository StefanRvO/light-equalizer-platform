#include "mesh_module.h"
#include "node/mesh_node_module.h"
#include "root/mesh_root_module.h"
#include "modules/mdns_module/mdns_module.h"

#include "modules/config_reader_module/config_reader_module.h"
#include "modules/web_server_module/websocket_handler/websocket_handler.h"

#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_mesh.h"
#include "esp_mesh_internal.h"
#include "esp_err.h"
#include "esp_wifi_types.h"
#include <string.h>

__attribute__((unused)) static const char *TAG = "MESH MODULE";

#define AP_SSID_NAME  "lightcontrol"
#define AP_SSID_PASS  "testpass"

static const uint8_t MESH_ID[6] = { 0x77, 0x77, 0x77, 0x77, 0x77, 0x77};
static bool is_running = false;
static bool is_mesh_connected = false;

static bool in_ap_mode = true;

static TickType_t last_connected_tick = 0;
static TickType_t last_connect_attempt_tick = 5000;

static mesh_addr_t mesh_parent_addr;
static int mesh_layer = -1;
static esp_netif_t *netif_sta = NULL;
static esp_netif_t *netif_ap  = NULL;

static wifi_ap_record_t scan_info[5];
static uint16_t scan_count = 0;


static void mesh_event_handler(void *arg, esp_event_base_t event_base,
                        int32_t event_id, void *event_data);

static void wifi_init_softap(void);
static void wifi_event_handler(void* arg, esp_event_base_t event_base,
                                    int32_t event_id, void* event_data);

static void wifi_init_mesh(void);
static void handle_scan_done(void);
static void do_scan(void);

int mesh_module_init(void)
{
    memset(scan_info, 0, sizeof(scan_info));

    ESP_LOGD(TAG,"MESH MODULE INIT");    
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    mdns_module_add_services();


    wifi_init_config_t config = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&config));
    
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_FLASH));

    wifi_init_mesh();

    int ret = 0;
    if (config_reader_is_root()) {
        ret |= mesh_root_module_init();
    } else {
        ret |= mesh_node_module_init();
    }
    return ret;
}


void wifi_init_mesh(void)
{
    in_ap_mode = false;
    ESP_ERROR_CHECK(esp_wifi_stop());
    esp_netif_destroy(netif_ap);
    esp_netif_destroy(netif_sta);

    ESP_ERROR_CHECK(esp_netif_create_default_wifi_mesh_netifs(&netif_sta, &netif_ap));
    
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_ERROR_CHECK(esp_mesh_init());
    ESP_ERROR_CHECK(esp_mesh_fix_root(true));

    ESP_ERROR_CHECK(esp_event_handler_register(MESH_EVENT, ESP_EVENT_ANY_ID, &mesh_event_handler, NULL));
    ESP_ERROR_CHECK(esp_mesh_set_max_layer(5));
    ESP_ERROR_CHECK(esp_mesh_set_ap_assoc_expire(60));

    mesh_cfg_t cfg = MESH_INIT_CONFIG_DEFAULT();
    cfg.channel         = 0; //force network scan on startup
    memcpy((uint8_t *) &cfg.mesh_id, MESH_ID, 6);
    if(config_reader_is_root())
    {
        char *sta_ssid;
        char *sta_pass;
        config_reader_get_sta(&sta_ssid, &sta_pass);
        cfg.router.ssid_len = strlen(sta_ssid);
        memcpy((uint8_t *) &cfg.router.ssid,     sta_ssid, cfg.router.ssid_len);
        memcpy((uint8_t *) &cfg.router.password, sta_pass, strlen(sta_pass));
        if(strlen(sta_pass) < 8)
        {
            memset((uint8_t *)&cfg.router.password, 0, sizeof(cfg.router.password));
        }

    }

    cfg.router.allow_router_switch = true;
    cfg.mesh_ap.max_connection = 4;
    cfg.mesh_ap.nonmesh_max_connection = 2;

    ESP_LOGI(TAG, "%s %s %d", cfg.router.password, cfg.router.ssid, cfg.router.ssid_len);


    ESP_ERROR_CHECK(esp_mesh_set_config(&cfg));
    ESP_ERROR_CHECK(esp_mesh_disable_ps());

    ESP_ERROR_CHECK(esp_mesh_start());
    is_running = true;
}

void wifi_init_softap(void)
{
    if(mesh_is_started())
    {
        is_running = false;
        ESP_ERROR_CHECK(esp_mesh_stop());
        ESP_ERROR_CHECK(esp_mesh_deinit());
    }
    ESP_ERROR_CHECK(esp_wifi_stop());
    vTaskDelay(100 / portTICK_RATE_MS);
    esp_netif_destroy(netif_ap);
    esp_netif_destroy(netif_sta);
    netif_sta = esp_netif_create_default_wifi_sta();
    netif_ap = esp_netif_create_default_wifi_ap();

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &wifi_event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT,   ESP_EVENT_ANY_ID, &wifi_event_handler, NULL));

    wifi_config_t ap_config = {};
    memset(&ap_config, 0, sizeof(wifi_config_t));

    snprintf((char *)ap_config.ap.ssid, sizeof(ap_config.ap.ssid), "%s-%s", AP_SSID_NAME, config_reader_get_name());
    strcpy((char *)ap_config.ap.password, AP_SSID_PASS);

    ap_config.ap.authmode = WIFI_AUTH_WPA2_PSK;
    ap_config.ap.ssid_len = strlen((char *)ap_config.ap.ssid);
    ap_config.ap.max_connection = 5;
    ap_config.ap.channel = 5;

    if(strlen((char*)ap_config.ap.password) < 8)
    {
        memset((uint8_t *)&ap_config.ap.password, 0, sizeof(ap_config.ap.password));
        ap_config.ap.authmode = WIFI_AUTH_OPEN;
    }

    wifi_config_t sta_config = {};
    memset(&sta_config, 0, sizeof(wifi_config_t));

    char *sta_ssid;
    char *sta_pass;
    config_reader_get_sta(&sta_ssid, &sta_pass);

    memcpy((uint8_t *) sta_config.sta.ssid,     sta_ssid, sizeof(sta_config.sta.ssid));
    memcpy((uint8_t *) sta_config.sta.password, sta_pass, sizeof(sta_config.sta.password));
    if(strlen((char*)sta_config.sta.password) < 8)
    {
        memset((uint8_t *)&sta_config.sta.password, 0, sizeof(sta_config.sta.password));
    }

    ESP_LOGI(TAG, "%s -- %s %d %d", sta_ssid, sta_pass, sizeof(sta_config.sta.ssid), sizeof(sta_config.sta.password));
    ESP_LOGI(TAG, "%s -- %s %d %d %d %d", ap_config.ap.ssid, ap_config.ap.password, ap_config.ap.ssid_len, ap_config.ap.max_connection, ap_config.ap.channel, ap_config.ap.authmode);


    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &ap_config));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &sta_config));


    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_LOGI(TAG, "Set up STA+AP");
    esp_netif_dhcps_start(netif_ap);
    in_ap_mode = true;

}


void wifi_event_handler(void* arg, esp_event_base_t event_base,
                                    int32_t event_id, void* event_data)
{
    if (event_id == WIFI_EVENT_AP_STACONNECTED) {
        wifi_event_ap_staconnected_t* event = (wifi_event_ap_staconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" join, AID=%d",
                 MAC2STR(event->mac), event->aid);
    } else if (event_id == WIFI_EVENT_AP_STADISCONNECTED) {
        wifi_event_ap_stadisconnected_t* event = (wifi_event_ap_stadisconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" leave, AID=%d",
                 MAC2STR(event->mac), event->aid);
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
            ESP_LOGI(TAG, "retry to connect to the AP");
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_SCAN_DONE) {
        ESP_LOGI(TAG, "Scan done");
        handle_scan_done();
    }
}

void mesh_event_handler(void *arg, esp_event_base_t event_base,
                        int32_t event_id, void *event_data)
{
    mesh_addr_t id = {0,};
    static uint8_t last_layer = 0;
    ESP_LOGD(TAG, "esp_event_handler:%d", event_id);

    switch (event_id) {
    case MESH_EVENT_STARTED: {
        esp_mesh_get_id(&id);
        ESP_LOGI(TAG, "<MESH_EVENT_MESH_STARTED>ID:"MACSTR"", MAC2STR(id.addr));
        is_mesh_connected = false;
        mesh_layer = esp_mesh_get_layer();
    }
    break;
    case MESH_EVENT_STOPPED: {
        ESP_LOGI(TAG, "<MESH_EVENT_STOPPED>");
        is_mesh_connected = false;
        mesh_layer = esp_mesh_get_layer();
    }
    break;
    case MESH_EVENT_CHILD_CONNECTED: {
        mesh_event_child_connected_t *child_connected = (mesh_event_child_connected_t *)event_data;
        ESP_LOGI(TAG, "<MESH_EVENT_CHILD_CONNECTED>aid:%d, "MACSTR"",
                 child_connected->aid,
                 MAC2STR(child_connected->mac));
    }
    break;
    case MESH_EVENT_CHILD_DISCONNECTED: {
        mesh_event_child_disconnected_t *child_disconnected = (mesh_event_child_disconnected_t *)event_data;
        ESP_LOGI(TAG, "<MESH_EVENT_CHILD_DISCONNECTED>aid:%d, "MACSTR"",
                 child_disconnected->aid,
                 MAC2STR(child_disconnected->mac));
    }
    break;
    case MESH_EVENT_PARENT_CONNECTED: {
        mesh_event_connected_t *connected = (mesh_event_connected_t *)event_data;
        esp_mesh_get_id(&id);
        mesh_layer = connected->self_layer;
        memcpy(&mesh_parent_addr.addr, connected->connected.bssid, 6);
        ESP_LOGI(TAG,
                 "<MESH_EVENT_PARENT_CONNECTED>layer:%d-->%d, parent:"MACSTR"%s, ID:"MACSTR"",
                 last_layer, mesh_layer, MAC2STR(mesh_parent_addr.addr),
                 esp_mesh_is_root() ? "<ROOT>" :
                 (mesh_layer == 2) ? "<layer2>" : "", MAC2STR(id.addr));
        last_layer = mesh_layer;
        is_mesh_connected = true;
        if (esp_mesh_is_root()) {
            esp_netif_dhcpc_start(netif_sta);
        }
    }
    break;
    case MESH_EVENT_PARENT_DISCONNECTED: {
        mesh_event_disconnected_t *disconnected = (mesh_event_disconnected_t *)event_data;
        ESP_LOGI(TAG,
                 "<MESH_EVENT_PARENT_DISCONNECTED>reason:%d",
                 disconnected->reason);
        is_mesh_connected = false;
        mesh_layer = esp_mesh_get_layer();
    }
    break;
    case MESH_EVENT_LAYER_CHANGE: {
        mesh_event_layer_change_t *layer_change = (mesh_event_layer_change_t *)event_data;
        mesh_layer = layer_change->new_layer;
        ESP_LOGI(TAG, "<MESH_EVENT_LAYER_CHANGE>layer:%d-->%d%s",
                 last_layer, mesh_layer,
                 esp_mesh_is_root() ? "<ROOT>" :
                 (mesh_layer == 2) ? "<layer2>" : "");
        last_layer = mesh_layer;
    }
    break;
    case MESH_EVENT_ROOT_ADDRESS: {
        mesh_event_root_address_t *root_addr = (mesh_event_root_address_t *)event_data;
        ESP_LOGI(TAG, "<MESH_EVENT_ROOT_ADDRESS>root address:"MACSTR"",
                 MAC2STR(root_addr->addr));
    }
    break;
    case MESH_EVENT_SCAN_DONE: {
        mesh_event_scan_done_t *scan_done = (mesh_event_scan_done_t *)event_data;
        ESP_LOGI(TAG, "<MESH_EVENT_SCAN_DONE>number:%d",
                 scan_done->number);
        handle_scan_done();
    }
    break;
    default:
        break;
    }
}

void mesh_module_handle_disconnected_timeout(void)
{
    if(netif_sta != NULL)
    {
        bool connected = esp_netif_is_netif_up(netif_sta);
        if(connected)
        {
            last_connected_tick = xTaskGetTickCount();
        }
        if(!connected && !in_ap_mode && xTaskGetTickCount() - last_connected_tick > 20000)
        {
            ESP_LOGI(TAG, "Not connected for 20 seconds, changing to AP+STA mode");
            wifi_init_softap();
        }
        else if(connected && in_ap_mode)
        {
            //Setup mesh mode
            ESP_LOGI(TAG, "Connected to station, but is in app mode, rebooting");
            //Should reinit during runtime, but this causes a crash currently..
            esp_restart();
        }
        else if(!connected && in_ap_mode)
        {
            static uint32_t connect_count = 0;
            if(xTaskGetTickCount() - last_connect_attempt_tick > 5000)
            {
                if(connect_count++ % 5 == 0)  {
                    do_scan();
                }
                else {
                    esp_wifi_connect();
                }
                last_connect_attempt_tick = xTaskGetTickCount();
            }
        }
    }

}

void mesh_module_wifi_scan_step()
{
    static TickType_t last_scan_tick = 0;
    if(xTaskGetTickCount() - last_scan_tick > 10000 && is_running)
    {
        do_scan();
        last_scan_tick = xTaskGetTickCount();
    }
}

void do_scan(void)
{
    ESP_LOGI(TAG, "Starting new scan");
    esp_wifi_scan_start(NULL, false);
}

void handle_scan_done(void)
{
    uint16_t size = sizeof(scan_info)/sizeof(scan_info[0]);
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&size, scan_info));
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_num(&scan_count));
}

const wifi_ap_record_t* mesh_module_get_scan_info(uint16_t *cnt)
{
    *cnt = scan_count;
    return scan_info;
}

bool mesh_is_connected(void)
{
    return is_mesh_connected;
}

bool mesh_is_started(void)
{
    return is_running;
}

void print_wifi_mode(void)
{
    // static uint32_t i = 0;
    // wifi_sta_list_t sta;
    // if(i++ % 500 == 0)
    // {
    //     esp_wifi_ap_get_sta_list(&sta);
    //     for(uint8_t i = 0; i < sta.num; i++)
    //     {
    //         ESP_LOGI(TAG, "rssi %d", sta.sta[i].rssi);
    //         ESP_LOGI(TAG, "11b %d", sta.sta[i].phy_11b);
    //         ESP_LOGI(TAG, "11g %d", sta.sta[i].phy_11g);
    //         ESP_LOGI(TAG, "11n %d", sta.sta[i].phy_11n);
    //         ESP_LOGI(TAG, "lr %d", sta.sta[i].phy_lr);
    //     }
    // }

}