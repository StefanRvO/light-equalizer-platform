#include "esp_err.h"
#include "esp_log.h"
#include "esp_mesh.h"

#include "modules/mesh_module/mesh_module.h"
#include "modules/protocol_module/protocol_module.h"
#include "modules/protocol_module/message_handler/message_handler_module.h"
#include <string.h>
#define QUEUE_LENGHT 5

__attribute__((unused)) static const char *TAG = "MESH MODULE NODE";

static void node_write_task(void *arg);

static QueueHandle_t node_send_queue    = NULL;
static mesh_addr_t root_addr;

int mesh_node_module_init(void)
{
    message_handle_module_init();
    node_send_queue    = xQueueCreate(QUEUE_LENGHT, sizeof(light_mesh_message_t));

    xTaskCreate(node_write_task, "node_write_task", 4 * 1024,
                NULL, 20, NULL);
    return 0;
}

void node_read_task(void)
{
    uint8_t buf[1500];
    mesh_data_t data;
    data.data  = buf;
    data.size  = 1500;
    data.proto = MESH_PROTO_BIN;
    data.tos   = MESH_TOS_P2P;
    int  flag = 0;
    light_mesh_message_t light_mesh_msg;

    while(1) {
        if (!mesh_is_connected()) {
            return;
        }

        if(esp_mesh_recv(&root_addr, &data, 0, &flag, NULL, 0) == ESP_ERR_MESH_TIMEOUT)
        {
            return;
        }

        protocol_module_esp_mesh_to_light_mesh(&light_mesh_msg, &root_addr, data.size, data.data);
        message_handler_module_handle_message(&light_mesh_msg);
    }
}

void node_write_task(void *arg)
{
    mesh_data_t data;
    data.data                        = malloc(1500);
    data.size                        = 1500;
    data.proto                       = MESH_PROTO_BIN;
    data.tos                         = MESH_TOS_P2P;

    mesh_addr_t dest_addr;
    light_mesh_message_t light_mesh_msg;

    for (;;) {
        xQueueReceive(node_send_queue, &light_mesh_msg, portMAX_DELAY);

        if (!mesh_is_connected()) {
            continue;
        }

        protocol_module_light_mesh_to_esp_mesh(&light_mesh_msg, &dest_addr, &data.size, data.data);
        free(light_mesh_msg.payload);
        if(mesh_is_connected())
        {
            esp_mesh_send(NULL, &data, 0, NULL, 0);
        }
    }

    ESP_LOGW(TAG, "Node write task is exit");

    free(data.data);
    vTaskDelete(NULL);
}

void send_to_root(light_mesh_message_type_t message_type, void *payload, uint16_t payload_len)
{
    if(!mesh_is_connected())
    {
        return;
    }
    light_mesh_message_t light_mesh_msg;
    uint8_t root_addr[6] = {0};
    memcpy(light_mesh_msg.dest_id, root_addr, 6);
    light_mesh_msg.type                    = (uint32_t)message_type;
    light_mesh_msg.payload                 = malloc(payload_len);
    memcpy(light_mesh_msg.payload, payload, payload_len);
    light_mesh_msg.payload_len             = payload_len;
    if(xQueueSendToBack(node_send_queue,  &light_mesh_msg, 0) != pdTRUE)
    {
        free(light_mesh_msg.payload);
    }
}
