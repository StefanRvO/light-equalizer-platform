#pragma once
#include "modules/protocol_module/protocol_module.h"
#include "modules/c_link.h"

C_LINK int mesh_node_module_init(void);
C_LINK void send_to_root(light_mesh_message_type_t message_type, void *payload, uint16_t payload_len);
C_LINK void node_read_task(void);