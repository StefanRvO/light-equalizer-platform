#pragma once
#include <stdint.h>


typedef enum
{
    BOARD_TYPE_SINGLE_CHANNEL,
    BOARD_TYPE_DUAL_CHANNEL,
} board_type_t;

typedef enum 
{
    NODE_TYPE_ROOT,
    NODE_TYPE_MESH,
    NODE_TYPE_FORCE_ROOT,
} node_type_t;

typedef enum
{
    LIGHT_TYPE_STRING,
    LIGHT_TYPE_PANEL,
    LIGHT_TYPE_RING,
    LIGHT_TYPE_CNT
} light_type_t;

#ifdef __cplusplus
extern "C" {
#endif

void config_reader_module_init(void);
const char* config_reader_get_name(void);
board_type_t config_reader_get_board_type(void);
node_type_t config_reader_get_mesh_type(void);
bool config_reader_is_root(void);
void config_reader_set_name(char *_name);
void config_reader_set_sta(const char* ssid, const char* pass);
void config_reader_get_sta(char** ssid, char** pass);
void config_reader_set_light_type(uint8_t type);
light_type_t config_reader_get_light_type(void);
void config_reader_get_blob(const char* id, void* data, void* default_val, size_t size);
void config_reader_set_blob(const char* id, void* data, size_t size);
uint32_t config_reader_get_uint32(const char* id, uint32_t default_val);
void config_reader_set_uint32(const char* id, uint32_t val);
int32_t config_reader_get_int32(const char* id, int32_t default_val);
void config_reader_set_int32(const char* id, int32_t val);

#ifdef __cplusplus
} // closing brace for extern "C"

#endif