#include "esp_err.h"
#include "esp_log.h"
#include "esp_mesh.h"

#include "config_reader_module.h"
#include "driver/gpio.h"
#include "modules/gpio_utilities/gpio_utilities.h"
#include "modules/board_revision_module/board_revision_module.h"
#include "nvs.h"

#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>

#define delay_ms(ms) vTaskDelay((ms) / portTICK_RATE_MS)

__attribute__((unused)) static const char *TAG = "CONFIG_MODULE";

static char name[20];
static char sta_ssid[32] = {0};
static char sta_pass[64] = {0};
static node_type_t mesh_type;
static uint8_t light_type;

static node_type_t read_mesh_type(void);
static uint32_t get_master_pin(void);

void config_reader_module_init(void)
{
    ESP_LOGI(TAG, "CONFIG_READER_INIT %u", get_master_pin());
    gpio_utilities_setup_with_pull(get_master_pin(), GPIO_MODE_INPUT, 0, GPIO_PULLUP_ONLY);

    //Set up nvs for ID and type:
    int nvs_ok = 0;
    nvs_handle handle;
    nvs_open("LED_EQ", NVS_READONLY, &handle);
    size_t name_len = sizeof(name);
    nvs_ok |= nvs_get_str(handle, "CONFIG_NAME", name, &name_len);
    light_type = config_reader_get_uint32("CONFIG_TYPE", LIGHT_TYPE_STRING);

    if(nvs_ok != ESP_OK)
    {
        memcpy(name, "NEW_DEVICE\0", 11);
        config_reader_set_name(name);
    }
    nvs_ok = 0;
    size_t ssid_len = sizeof(sta_ssid);
    size_t pass_len = sizeof(sta_pass);

    nvs_ok |= nvs_get_str(handle, "STA_SSID", sta_ssid, &ssid_len);
    nvs_ok |= nvs_get_str(handle, "STA_PASS", sta_pass, &pass_len);
    if(nvs_ok != ESP_OK)
    {
        config_reader_set_sta("", "");
    }
    ESP_LOGI(TAG, "SSID: %s, PASS: %s", sta_ssid, sta_pass);
    nvs_commit(handle);
    nvs_close(handle);
    mesh_type = read_mesh_type();
}

const char* config_reader_get_name(void)
{
    return name;
}

board_type_t config_reader_get_board_type(void)
{
    return BOARD_TYPE_SINGLE_CHANNEL;
}


void config_reader_set_name(char *_name)
{
    strncpy(name, _name, sizeof(name) -1);
    nvs_handle handle;
    nvs_open("LED_EQ", NVS_READWRITE, &handle);
    nvs_set_str(handle, "CONFIG_NAME", name);
    nvs_commit(handle);
    nvs_close(handle);
}

void config_reader_set_sta(const char* ssid, const char* pass)
{
    strncpy(sta_ssid, ssid, sizeof(sta_ssid) -1);
    strncpy(sta_pass, pass, sizeof(sta_pass) -1);

    nvs_handle handle;
    nvs_open("LED_EQ", NVS_READWRITE, &handle);
    nvs_set_str(handle, "STA_SSID", sta_ssid);
    nvs_set_str(handle, "STA_PASS", sta_pass);
    nvs_commit(handle);
    nvs_close(handle);
}

void config_reader_get_sta(char** ssid, char** pass)
{
    *ssid = sta_ssid;
    *pass = sta_pass;
}

uint32_t get_master_pin(void)
{
    if(board_revision_module_get_revision() == BOARD_REVISION_V3_I2S)
    {
        return 21;
    }
    return 4;
}

node_type_t read_mesh_type(void)
{
    ESP_LOGI(TAG, "%s", name);
    if(strcmp(name, "FORCE_ROOT") == 0)
    {
        return NODE_TYPE_FORCE_ROOT;
    }
    ESP_LOGI(TAG, "MASTER_PIN_STATE %u", gpio_get_level(get_master_pin()));
    if(!gpio_get_level(get_master_pin()))
    {
        return NODE_TYPE_ROOT;
    }
    return NODE_TYPE_MESH;
}

bool config_reader_is_root(void)
{
    return mesh_type == NODE_TYPE_ROOT || mesh_type == NODE_TYPE_FORCE_ROOT;
}

node_type_t config_reader_get_mesh_type(void) {
    return mesh_type;
}

light_type_t config_reader_get_light_type(void) {
    return light_type;
}

void config_reader_get_blob(const char* id, void* data, void* default_val, size_t size)
{
    nvs_handle handle;
    nvs_open("LED_EQ", NVS_READWRITE, &handle);
    int ret = nvs_get_blob(handle, id, data, &size);
    nvs_close(handle);

    if(ret != ESP_OK)
    {
        ESP_LOGW(TAG, "Failed to load %s. Err: %s", id, esp_err_to_name(ret));
        memcpy(data, default_val, size);
        config_reader_set_blob(id, data, size);
    }
}

void config_reader_set_blob(const char* id, void* data, size_t size)
{
    nvs_handle handle;
    nvs_open("LED_EQ", NVS_READWRITE, &handle);
    nvs_set_blob(handle, id, data, size);
    nvs_commit(handle);
    nvs_close(handle);
}

uint32_t config_reader_get_uint32(const char* id, uint32_t default_val)
{
    nvs_handle handle;
    nvs_open("LED_EQ", NVS_READWRITE, &handle);
    uint32_t tmp;
    int ret = nvs_get_u32(handle, id, &tmp);
    nvs_close(handle);

    if(ret != ESP_OK)
    {
        ESP_LOGW(TAG, "Failed to load %s. Err: %s", id, esp_err_to_name(ret));
        tmp = default_val;
        config_reader_set_uint32(id, tmp);
    }
    ESP_LOGI(TAG, "loaded %s as %d", id, tmp);
    return tmp;
}

void config_reader_set_uint32(const char* id, uint32_t val)
{
    nvs_handle handle;
    nvs_open("LED_EQ", NVS_READWRITE, &handle);
    ESP_ERROR_CHECK(nvs_set_u32(handle, id, val));
    nvs_commit(handle);
    nvs_close(handle);
}

int32_t config_reader_get_int32(const char* id, int32_t default_val)
{
    nvs_handle handle;
    nvs_open("LED_EQ", NVS_READWRITE, &handle);
    int32_t tmp;
    int ret = nvs_get_i32(handle, id, &tmp);
    nvs_close(handle);

    if(ret != ESP_OK)
    {
        ESP_LOGW(TAG, "Failed to load %s. Err: %s", id, esp_err_to_name(ret));
        tmp = default_val;
        config_reader_set_int32(id, tmp);
    }
    ESP_LOGI(TAG, "loaded %s as %d", id, tmp);
    return tmp;
}

void config_reader_set_int32(const char* id, int32_t val)
{
    nvs_handle handle;
    nvs_open("LED_EQ", NVS_READWRITE, &handle);
    ESP_ERROR_CHECK(nvs_set_i32(handle, id, val));
    nvs_commit(handle);
    nvs_close(handle);
}


void config_reader_set_light_type(uint8_t type) {
    config_reader_set_uint32("CONFIG_TYPE", type);
    light_type = type;
}