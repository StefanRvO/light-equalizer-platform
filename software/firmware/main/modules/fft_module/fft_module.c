#include "fft_module.h"
#include "drivers/mcp3002/mcp3002.h"
#include "modules/sound_sampler_module/sound_sampler_module.h"

#include "modules/light_management_module/frequency_management_module/frequency_management_module.h"
#include "libs/goertzel_lib/goertzel_lib.h"
#include "libs/hann_window/hann_window.h"

#include <math.h>
#include "esp_err.h"
#include "esp_log.h"
#include "kiss_fftr.h"
#include <math.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#if defined(CONFIG_IDF_TARGET_ESP32) || defined(CONFIG_IDF_TARGET_ESP32S3)


#include "xtensa/core-macros.h"


static uint32_t get_index(uint32_t n, uint32_t N);
static float apply_window(uint32_t n, uint32_t N);
void apply_window_to_buf(sound_buffer_datatype_t *samples);
void reverse_window_to_buf(sound_buffer_datatype_t *samples);
void shift_samples(channel_samples_t *channel_data);

__attribute__((unused)) static const char *TAG = "fft_module";


static sound_samples_t samples = {0};
static kiss_fftr_cfg kiss_cfg;
TaskHandle_t fft_task;
static bool first = true;
void fft_compute(void);
static float Q_rsqrt( float number );
float Q_rsqrt( float number )
{	
	const float x2 = number * 0.5F;
	const float threehalfs = 1.5F;

	union {
		float f;
		uint32_t i;
	} conv = {number}; // member 'f' set to value of 'number'.
	conv.i  = 0x5f3759df - ( conv.i >> 1 );
	conv.f  *= ( threehalfs - ( x2 * conv.f * conv.f ) );
	return conv.f;
}

uint32_t get_index(uint32_t n, uint32_t N)
{
    if(n < N/2)
    {
	    return n;
    }
    return N - 1 - n;
}

float apply_window(uint32_t n, uint32_t N)
{
    uint32_t index = get_index(n, N);
    return hann_window_coeffs[index];
}

esp_err_t fft_module_init(void)
{
    esp_err_t ret = OK;
    sound_sampler_module_init();
    samples.channel_data[0].sample_cnt = 0;
    samples.channel_data[1].sample_cnt = 0;

    samples.buf_size = SOUND_BUFFER_SIZE;
    kiss_cfg = kiss_fftr_alloc(SOUND_BUFFER_SIZE, 0, 0, 0);
    ESP_LOGI(TAG, "KISS WOULD LIKE: %d", kiss_fft_next_fast_size(SOUND_BUFFER_SIZE));

    return ret;
}

void fft_module_step(void *pvparams)
{
        if(first)
        {
            first = false;
            sound_sampler_module_flush();
        }
        sound_sampler_module_read(&samples);
        if(samples.channel_data[0].sample_cnt + samples.channel_data[1].sample_cnt >= samples.buf_size * 2)
        {  
            fft_compute();
        }
}

void fft_compute(void)
{
    kiss_fft_cpx fft_result[SOUND_BUFFER_SIZE/2 + 1];
    kiss_fft_scalar *fft_magnitude = (kiss_fft_scalar *)&fft_result; //Kind of a hack to safe memory.
    for(uint32_t i = 0; i < 2; i++)
    {
        channel_samples_t* channel_data = &samples.channel_data[i];
        sound_buffer_datatype_t *samples = channel_data->sample_buf;
        apply_window_to_buf(samples);
        kiss_fftr(kiss_cfg, samples, fft_result);
        reverse_window_to_buf(samples);
        
        // uint32_t max_bin = 0;
        for(uint32_t j = 0; j < SOUND_BUFFER_SIZE/2 + 1; j++)
        {
            fft_magnitude[j] = 1 / Q_rsqrt(fft_result[j].r * fft_result[j].r + fft_result[j].i * fft_result[j].i);
            // if(fabsf(fft_magnitude[j]) > fft_magnitude[max_bin] || max_bin < 2)
            // {
            //     max_bin = j;
            // }
        }
        fft_magnitude[0] = 0;
        // ESP_LOGI(TAG, "MAX %u %u %f", i, max_bin, fft_magnitude[max_bin]);
        frequency_management_module_set_frequencies(fft_magnitude, i, i == 1);
        shift_samples(channel_data);
        channel_data->sample_cnt = 0;
    }
}

void apply_window_to_buf(sound_buffer_datatype_t *samples)
{
    for(uint32_t i = 0; i < SOUND_BUFFER_SIZE; i++)
    {
        samples[i] *= apply_window(i, SOUND_BUFFER_SIZE);
    }
}

void reverse_window_to_buf(sound_buffer_datatype_t *samples)
{
    for(uint32_t i = 0; i < SOUND_BUFFER_SIZE; i++)
    {
        samples[i] /= apply_window(i, SOUND_BUFFER_SIZE);
    }
}

void shift_samples(channel_samples_t *channel_data)
{
    for(uint32_t i = 0; i < SOUND_BUFFER_SIZE / 2; i++)
    {
        channel_data->sample_buf[i] = channel_data->sample_buf[i + SOUND_BUFFER_SIZE / 2];
    }
    channel_data->sample_cnt = SOUND_BUFFER_SIZE / 2;
}
#endif