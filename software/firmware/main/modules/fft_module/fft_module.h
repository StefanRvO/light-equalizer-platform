#pragma once

#include <stdint.h>
#include "esp_err.h"
#include "esp_log.h"
#include "libs/goertzel_lib/goertzel_lib.h"
#include "modules/c_link.h"

typedef enum
{
    AUDIO_CHANNEL_LEFT  = 0,
    AUDIO_CHANNEL_RIGHT = 1,
    AUDIO_CHANNEL_CNT
} audio_channel_t;


C_LINK esp_err_t fft_module_init(void);
C_LINK void fft_module_reset_filter(audio_channel_t channel, uint8_t frequency_index);
C_LINK void fft_module_step();