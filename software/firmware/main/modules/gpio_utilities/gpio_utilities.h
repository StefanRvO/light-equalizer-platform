#pragma once

#include "driver/gpio.h"


void gpio_utilities_setup(int gpioNum, int gpioMode, int gpioVal);
void gpio_utilities_setup_with_pull(int gpioNum, int gpioMode, int gpioVal, gpio_pull_mode_t pull);
void gpio_utilities_set_pin_floating(int gpioNum);
void gpio_utilities_set_pin_high(int gpioNum);
void gpio_utilities_set_pin_low(int gpioNum);