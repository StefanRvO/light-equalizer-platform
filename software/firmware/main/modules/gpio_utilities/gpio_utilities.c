#include "driver/gpio.h"


void gpio_utilities_setup(int gpioNum, int gpioMode, int gpioVal) 
{
    gpio_num_t gpioNumNative = (gpio_num_t)(gpioNum);
    gpio_mode_t gpioModeNative = (gpio_mode_t)(gpioMode);
    gpio_pad_select_gpio(gpioNumNative);
    gpio_set_drive_capability(gpioNumNative, (gpio_drive_cap_t)0);
    gpio_set_direction(gpioNumNative, gpioModeNative);
    gpio_set_level(gpioNumNative, gpioVal);
}

void gpio_utilities_setup_with_pull(int gpioNum, int gpioMode, int gpioVal, gpio_pull_mode_t pull) {
    gpio_num_t gpioNumNative = (gpio_num_t)(gpioNum);
    gpio_mode_t gpioModeNative = (gpio_mode_t)(gpioMode);
    gpio_pad_select_gpio(gpioNumNative);
    gpio_set_drive_capability(gpioNumNative, (gpio_drive_cap_t)0);
    gpio_set_direction(gpioNumNative, gpioModeNative);
    gpio_set_level(gpioNumNative, gpioVal);
    gpio_set_pull_mode(gpioNumNative, pull);
}

void gpio_utilities_set_pin_floating(int gpioNum)
{
    gpio_utilities_setup_with_pull(gpioNum, GPIO_MODE_INPUT, 0, GPIO_FLOATING);
}

void gpio_utilities_set_pin_high(int gpioNum)
{
    gpio_utilities_setup(gpioNum, GPIO_MODE_OUTPUT, 1);
}

void gpio_utilities_set_pin_low(int gpioNum)
{
    gpio_utilities_setup(gpioNum, GPIO_MODE_OUTPUT, 1);
}