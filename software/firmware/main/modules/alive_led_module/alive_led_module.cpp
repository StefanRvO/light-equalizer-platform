#include "esp_err.h"
#include "esp_log.h"
#include "esp_mesh.h"
#include "esp_mesh_internal.h"

#include "drivers/alive_led_driver/alive_led_driver.h"
#include "libs/strobe_lib/strobe_lib.h"
#include "libs/color_utils/color_utils.h"
#include "modules/mesh_module/mesh_module.h"
#include "modules/debug_led_module/debug_led_module.hpp"
#include "modules/filesystem_sync_module/filesystem_sync_module.h"
#include "modules/firmware_sync_module/firmware_sync_module.h"
#include "tasks/master_task/master_task.hpp"
#include "modules/onboard_led_strength_module/onboard_led_strength_module.h"
#include <NeoPixelBus.h>

#define CONNECTED_OFF  950000
#define CONNECTED_ON  50000
#define DISCONNECTED_ON  500000
#define DISCONNECTED_OFF 500000

#define FS_SYNC_BLINK_PERIOD 100000

#define FW_SYNC_BLINK_PERIOD 30000

static strobe_light_data_t strobe_data; 

static void callback_on(void *data);
static void callback_off(void *data);
static RgbColor get_layer_color(uint8_t layer);

__attribute__((unused)) static const char *TAG = "ALIVE_LED_MODULE";


extern "C" void alive_led_module_init()
{
    strobe_lib_reset_struct(&strobe_data);
    strobe_data.timing_spec.on_time.tv_usec  = DISCONNECTED_ON;
    strobe_data.timing_spec.off_time.tv_usec = DISCONNECTED_OFF;
    strobe_data.strobe_on_func               = callback_on;
    strobe_data.strobe_off_func              = callback_off;

    alive_led_driver_init();
}

extern "C" void alive_led_module_step(bool on)
{
    strobe_lib_step(&strobe_data);
}

void callback_off(void *data)
{
    // ESP_LOGI(TAG, "OFF");
    alive_led_set(0);
    debug_led_module_set_off();
}

void callback_on(void *data)
{
    // ESP_LOGI(TAG, "ON");

    RgbColor color = get_layer_color(esp_mesh_get_layer());
    if(master_task_is_behind())
    {
        color = RgbColor(255, 0, 0);
    }
    RgbColor off_color(0,0,0);
    float strength = onboard_led_strenght_module_get();

    color = RgbColor::LinearBlend(off_color, color, strength);
    debug_led_module_set_color(color);
    alive_led_set(strength);

    if(mesh_is_connected())
    {
        strobe_data.timing_spec.on_time.tv_usec  = CONNECTED_ON;
        strobe_data.timing_spec.off_time.tv_usec = CONNECTED_OFF;
    }
    else
    {
        strobe_data.timing_spec.on_time.tv_usec  = DISCONNECTED_ON;
        strobe_data.timing_spec.off_time.tv_usec = DISCONNECTED_OFF;
    }

    if(file_system_sync_module_get_receive_offset() || file_system_sync_module_is_sending())
    {
        strobe_data.timing_spec.on_time.tv_usec  = FS_SYNC_BLINK_PERIOD;
        strobe_data.timing_spec.off_time.tv_usec = FS_SYNC_BLINK_PERIOD;
    }
    if(firmware_sync_module_get_receive_offset() || firmware_sync_module_is_sending())
    {
        strobe_data.timing_spec.on_time.tv_usec  = FW_SYNC_BLINK_PERIOD;
        strobe_data.timing_spec.off_time.tv_usec = FW_SYNC_BLINK_PERIOD;
    }
}

RgbColor get_layer_color(uint8_t layer)
{
    switch(layer)
    {
        case 0:
        {
            return RgbColor(255, 255, 255);
        }
        case 1:
        {
            return RgbColor(0, 255, 0);
        }
        case 2:
        {
            return RgbColor(0, 0, 255);
        }
        case 3:
        {
            return RgbColor(0, 255, 255);
        }
        case 4:
        {
            return RgbColor(255, 0, 255);
        }
        case 5:
        {
            return RgbColor(255, 255, 0);
        }
        case 6:
        {
            return RgbColor(128, 255, 255);
        }
    }
    return RgbColor(255, 50, 50);
}