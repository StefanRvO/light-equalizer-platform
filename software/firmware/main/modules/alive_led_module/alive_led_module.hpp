#pragma once
#include "modules/c_link.h"

C_LINK void alive_led_module_init(void);
C_LINK void alive_led_module_step(void);