#include "mdns.h"
#include "esp_log.h"

static const char *TAG = "MDNS_MODULE";

void mdns_module_add_services()
{
    uint8_t mac[6];
    char   *hostname;
    esp_read_mac(mac, ESP_MAC_WIFI_STA);
    if (-1 == asprintf(&hostname, "%s-%02X%02X%02X", "LightController", mac[3], mac[4], mac[5])) {
        abort();
    }
        //initialize mDNS
    ESP_ERROR_CHECK( mdns_init() );
    //set mDNS hostname (required if you want to advertise services)
    ESP_ERROR_CHECK( mdns_hostname_set(hostname) );
    ESP_LOGI(TAG, "mdns hostname set to: [%s]", hostname);
    //set default mDNS instance name
    ESP_ERROR_CHECK( mdns_instance_name_set("MDNS_LIGHTCONTROL") );

    //add our services

    mdns_txt_item_t serviceTxtData[1] = {
        {"board","esp32"},
    };

    ESP_ERROR_CHECK( mdns_service_add("lightcontrolconfig", "_ws", "_tcp", 80, serviceTxtData, 1));
    ESP_ERROR_CHECK( mdns_service_add("lightcontrolconfig", "_http", "_tcp", 80, serviceTxtData, 1));

    ESP_ERROR_CHECK(mdns_service_port_set("_ws", "_tcp", 80));

    //set txt data for service (will free and replace current data)
    ESP_ERROR_CHECK( mdns_service_txt_item_set("_ws", "_tcp", "path", "/") );

    free(hostname);
}