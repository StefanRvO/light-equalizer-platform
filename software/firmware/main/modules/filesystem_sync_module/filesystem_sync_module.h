#include "modules/protocol_module/protocol_module.h"


#ifdef __cplusplus
extern "C" {
#endif

void filesystem_sync_module_send_progress_step(void);
void filesystem_sync_module_send_data_step(void);
void filesystem_sync_module_handle_data(light_mesh_message_t *msg);
void filesystem_sync_module_handle_progress(light_mesh_message_t *msg);
bool file_system_sync_module_is_sending(void);
uint32_t file_system_sync_module_get_receive_offset(void);
void filesystem_sync_module_init(void);
uint32_t filesystem_sync_get_bytes_left_to_receive(void);
float filesystem_sync_get_synced_part(void);

#ifdef __cplusplus
}
#endif