#include <stdint.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "filesystem_sync_module.h"

extern "C"
{
    #include "modules/file_system_module/file_system_module.h"
    #include "modules/mesh_module/node/mesh_node_module.h"
    #include "modules/mesh_module/root/mesh_root_module.h"
    #include "libs/string_format_lib/string_format_lib.h"
}

#include "esp_err.h"
#include "esp_log.h"


#include "libs/sync_lib/sync_lib.hpp"

class FilesystemSyncer : public PartitionSyncer
{
    public:
        FilesystemSyncer(): PartitionSyncer(FS_SYNC_PROGRESS_MESSAGE,
                                            FS_DATA_MESSAGE,
                                            "FilesystemSync")
        {}
        virtual const esp_partition_t* get_target_partition() const
        {
            return file_system_get_target_partition();
        }
        virtual void handle_done_receiving()
        {
            file_system_update_flash_sha256();
        }
};

static FilesystemSyncer *syncer;

void filesystem_sync_module_init(void)
{
    syncer = new FilesystemSyncer();
    syncer->init();
}

void filesystem_sync_module_send_progress_step(void)
{
    syncer->send_progress_step();
}

void filesystem_sync_module_send_data_step(void)
{
    syncer->send_data_step();
}

void filesystem_sync_module_handle_data(light_mesh_message_t *msg)
{
    syncer->handle_data(msg);
}

void filesystem_sync_module_handle_progress(light_mesh_message_t *msg)
{
    syncer->handle_progress(msg);
}

bool file_system_sync_module_is_sending(void)
{
    return syncer->missing_send_chunks.size() != 0;
}

uint32_t file_system_sync_module_get_receive_offset(void)
{
    return syncer->get_next_receive_offset();
}

uint32_t filesystem_sync_get_bytes_left_to_receive(void)
{
    return syncer->missing_recv_chunks.get_total_size();
}

float filesystem_sync_get_synced_part(void)
{
    return 1 - (float)syncer->missing_recv_chunks.get_total_size() / (float) syncer->get_target_partition_size();
}