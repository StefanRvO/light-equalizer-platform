#pragma once
#include "libs/led_panel/led_panel.h"
#include "modules/c_link.h"

C_LINK bool fire_control_module_step(led_panel_t* panel, pixelColor_t color);
