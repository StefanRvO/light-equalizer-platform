#include "fire_control_module.h"

#include "libs/led_panel/led_panel.h"
#include "libs/color_utils/color_utils.h"
#include "libs/fire_lib/fire_lib.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#define UPDATE_PERIOD (44100 / 1100)
#define SPAWN_CHANGE 10

static TickType_t last_update = 0;

static const float blur_kernel[5][5] = {{0,    .0,   .0, .0, 0.},
                                        {0,    .245, .0, .0, 0.},
                                        {.245, .245, .0, .0, 0.},
                                        {0.,   .245, .0, .0, 0.},
                                        {0.,   .0,   .0, .0, 0.}};

static void update_fire(led_panel_t* panel, pixelColor_t color);

bool fire_control_module_step(led_panel_t* panel, pixelColor_t color)
{
    TickType_t cur = xTaskGetTickCount();
    if(cur > last_update + UPDATE_PERIOD)
    {
        last_update = cur;
        update_fire(panel, color);
        return true;
    }
    return false;
}

void update_fire(led_panel_t* panel, pixelColor_t color)
{
    pixelColor_t color_end   = {.r = 0,   .g = 0, .b =0, .w = 0};
    pixelColor_t bottom[panel->width];
    for(uint32_t x = 0; x < panel->width; x++)
    {
        if(x % 2 == rand() % 2)
        {
            pixelColor_t c = scale_color(&color, &color_end, (rand() % 10000) / 10000.);
            bottom[x] = c;
        }
    }
    fire_lib_step(panel, bottom, 5, 5, blur_kernel);
}