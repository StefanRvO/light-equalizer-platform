#include "esp_err.h"
#include "esp_log.h"

#include "glitter_light_control_module.h"

__attribute__((unused)) static const char *TAG = "GLITTER_LIGHT_CONTROL";

static glitter_light_data_t glitter_data; 
static TickType_t next_update;
static void update_leds(strand_t *strand, pixelColor_t color);

#define US_PER_S 1000000

int glitter_light_control_module_init(void)
{
    glitter_data.percent_on          = 50;
    glitter_data.update_rate.tv_sec  = 0;
    glitter_data.update_rate.tv_usec = 50000;
    glitter_data.random_color        = false;

    next_update                      = xTaskGetTickCount();
    return 0;
}

bool glitter_light_control_module_step(strand_t *strand, pixelColor_t color, bool new_mode)
{
    TickType_t cur_tick = xTaskGetTickCount();
    if(cur_tick >= next_update)
    {
        if(strand)  update_leds(strand, color);
        uint64_t update_interval_us = (uint64_t)glitter_data.update_rate.tv_sec * US_PER_S + glitter_data.update_rate.tv_usec;
        next_update = cur_tick + update_interval_us / 1000;
        return true;
    }
    return false;
}

static void update_leds(strand_t *strand, pixelColor_t color)
{
    for(uint32_t i = 0; i < strand->numPixels; i++)
    {
        if(rand() % 100 <= glitter_data.percent_on)
        {
            if(glitter_data.random_color)
            {
                strand->pixels[i].r = rand() % 2 ? 255 : 0;
                strand->pixels[i].g = rand() % 2 ? 255 : 0;
                strand->pixels[i].b = rand() % 2 ? 255 : 0;
            }
            else
            {
                strand->pixels[i] = color;
            }
        }
        else
        {
            strand->pixels[i].raw32 = 0;
        }
    }
}

void glitter_light_spec_recv(light_mesh_message_t *msg)
{
    glitter_light_data_t* data = (glitter_light_data_t*)msg->payload;
    glitter_light_control_module_set_data(data);
}

void glitter_light_control_module_set_data(glitter_light_data_t *data)
{
    glitter_data = *data;
}
