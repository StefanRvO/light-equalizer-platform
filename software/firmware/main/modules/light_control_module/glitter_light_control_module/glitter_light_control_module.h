#pragma once
#include "esp32_digital_led_lib.h"
#include "modules/protocol_module/protocol_module.h"
#include <time.h>
#include "modules/c_link.h"

typedef struct
{
    float percent_on;
    struct timeval update_rate;
    bool random_color;
} glitter_light_data_t;

C_LINK int glitter_light_control_module_init(void);
C_LINK bool glitter_light_control_module_step(strand_t *strand, pixelColor_t color, bool new_mode);

C_LINK void glitter_light_spec_recv(light_mesh_message_t *msg);
C_LINK void glitter_light_control_module_set_data(glitter_light_data_t *data);