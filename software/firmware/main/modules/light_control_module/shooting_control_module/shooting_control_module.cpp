#include "shooting_control_module.hpp"
#include <array>
#include <random>
#include <cstring>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#define UPDATE_PERIOD_MS 20

static shooting_settings_t shooting_settings;

static TickType_t last_update = 0;


typedef struct
{
    float speed; //Pixels pr second
    float fade_rate; //Fade in percent per pixel of tail
    float position;
    pixelColor_t color;
} shoot_instance_t;

void move_shoots(std::array<shoot_instance_t, 30>& shoots, float delta_t);
void spawn_shoots(std::array<shoot_instance_t, 30>& shoots, float delta_t);
void insert_shoot(std::array<shoot_instance_t, 30>& shoots, std::ranlux24_base& e2);
void display_shoots(std::array<shoot_instance_t, 30>& shoots, strand_t* strand);
void display_shoot(shoot_instance_t& shoot, strand_t* strand);

void move_shoots(std::array<shoot_instance_t, 30>& shoots, float delta_t, strand_t* strand)
{
    for(auto& shoot : shoots)
    {
        if(shoot.position >= 0)
        {
            shoot.position += shoot.speed * delta_t;
        }
        //Remove shoots when they get out of frame
        if(shoot.position > strand->numPixels + 100) {
            shoot.position = -1;
        }
    }
}

void spawn_shoots(std::array<shoot_instance_t, 30>& shoots, float delta_t)
{
    std::ranlux24_base e2(rand());
    std::uniform_real_distribution<float> dist_spawn(0, 1);
    float spawn_chance = delta_t * shooting_settings.spawn_rate;
    if(dist_spawn(e2) < spawn_chance) {
        insert_shoot(shoots, e2);
    }
}

void insert_shoot(std::array<shoot_instance_t, 30>& shoots, std::ranlux24_base& e2)
{
    const pixelColor_t colors[] = {
        {255, 255, 255, 0}, //white
        {255, 0,   0,   0}, //blue
        {255, 255, 0,   0}, //cyan
        {0,   255, 0,   0}, //green
        {255, 0,   255, 0}, //megenta
        {19,  69,  139, 0}, //brown
        {255, 255, 240, 0}, //azure
        {147, 20,  255, 0}, //pink
        {0,   0,   255, 0}, //red
    };

    for(auto& shoot : shoots)
    {
        if(shoot.position < 0)
        {
            std::uniform_real_distribution<float> dist_fade_rate(shooting_settings.fade_rate_min, shooting_settings.fade_rate_max);
            std::uniform_real_distribution<float> dist_speed(shooting_settings.speed_min, shooting_settings.speed_max);
            shoot.position = 0;
            shoot.fade_rate = dist_fade_rate(e2);
            shoot.speed = dist_speed(e2);
            shoot.color = colors[e2() % (sizeof(colors) / sizeof(colors[0]))];
            break;
        }
    }
}

void display_shoots(std::array<shoot_instance_t, 30>& shoots, strand_t* strand)
{
    memset(strand->pixels, 0x00, sizeof(strand->pixels[0]) * strand->numPixels);
    for(auto& shoot : shoots)
    {
        if(shoot.position >= 0)
        {
            display_shoot(shoot, strand);
        }
    }
}

void display_shoot(shoot_instance_t& shoot, strand_t* strand)
{
    float r = shoot.color.r;
    float g = shoot.color.g;
    float b = shoot.color.b;
    int16_t position = shoot.position;
    while(position >= 0 && (r >= 1 || g >= 1 || b >= 1))
    {
        if(position < strand->numPixels)
        {
            strand->pixels[position].r = strand->pixels[position].r + r < 255 ? strand->pixels[position].r + r : 255;
            strand->pixels[position].g = strand->pixels[position].g + g < 255 ? strand->pixels[position].g + g : 255;
            strand->pixels[position].b = strand->pixels[position].b + b < 255 ? strand->pixels[position].b + b : 255;
        }
        position--;
        r *= 1 - shoot.fade_rate;
        g *= 1 - shoot.fade_rate;
        b *= 1 - shoot.fade_rate;
    }
}

bool shooting_control_module_step(strand_t* strand, bool light_mode_changed)
{
    std::array<shoot_instance_t, 30>* shoots = (std::array<shoot_instance_t, 30>*)strand->state_buf;
    if(light_mode_changed)
    {
        shoot_instance_t tmp;
        tmp.position = -1;
        shoots->fill(tmp);
        last_update = 0;
    }

    TickType_t cur = xTaskGetTickCount();
    if(cur < last_update + UPDATE_PERIOD_MS)
    {
        return false;
    }
    
    float delta_t = (cur - last_update) / 1000.f;
    move_shoots(*shoots, delta_t, strand);
    spawn_shoots(*shoots, delta_t);
    display_shoots(*shoots, strand);

    last_update = cur;

    return true;
}

void shooting_control_module_set_settings(shooting_settings_t* settings)
{
    shooting_settings = *settings;
    if(shooting_settings.fade_rate_max <= shooting_settings.fade_rate_min)
    {
        shooting_settings.fade_rate_max = shooting_settings.fade_rate_min + 0.01f;
    }
    if(shooting_settings.speed_max <= shooting_settings.speed_min)
    {
        shooting_settings.speed_max = shooting_settings.speed_min + 0.01f;
    }
}