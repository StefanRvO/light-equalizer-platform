#pragma once

#include "esp32_digital_led_lib.h"
#include <stdbool.h>

typedef struct
{
    float spawn_rate; //Shoots per second
    float fade_rate_min;
    float fade_rate_max;
    float speed_min; //Pixels per seconds
    float speed_max; //Pixels per second
} shooting_settings_t;

bool shooting_control_module_step(strand_t* strand, bool light_mode_changed);
void shooting_control_module_set_settings(shooting_settings_t* settings);