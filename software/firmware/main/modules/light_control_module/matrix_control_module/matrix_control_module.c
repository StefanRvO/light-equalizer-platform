#include "libs/led_panel/led_panel.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#define UPDATE_PERIOD 100
#define SPAWN_PERCENT 20

static TickType_t last_update = 0;

static void update_matrix(led_panel_t* panel, pixelColor_t color);

bool matrix_control_lib_step(led_panel_t* panel, pixelColor_t color)
{
    TickType_t cur = xTaskGetTickCount();
    if(cur > last_update + UPDATE_PERIOD)
    {
        last_update = cur;
        update_matrix(panel, color);
        return true;
    }
    return false;
}

void update_matrix(led_panel_t* panel, pixelColor_t color)
{
    pixelColor_t off = {.raw32 = 0};

    for(uint32_t i = 0; i < panel->width; i++)
    {
        led_panel_shift_col(panel, i);
        led_panel_set_pixel(panel, i, panel->height - 1, rand() % 100 <= SPAWN_PERCENT ? &color : &off);
    }
}