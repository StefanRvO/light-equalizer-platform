#pragma once
#include "libs/led_panel/led_panel.h"
#include "modules/c_link.h"

C_LINK bool matrix_control_lib_step(led_panel_t* panel, pixelColor_t color);
