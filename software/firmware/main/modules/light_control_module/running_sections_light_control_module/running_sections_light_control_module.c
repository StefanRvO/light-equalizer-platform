#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "esp_err.h"
#include "esp_log.h"

#include "running_sections_light_control_module.h"
#include "libs/color_utils/color_utils.h"
#include <string.h>

__attribute__((unused)) static const char *TAG = "RUNNING_SECTIONS_LIGHT_CONTROL";

static TickType_t last_wake_time = 0;
uint32_t update_rate_ms = 100;
static uint32_t section_count = 1;
static uint32_t section_size  = 0;
static uint32_t start_pixel   = 0;

static void update_leds(strand_t *strand, pixelColor_t color, uint32_t start_pixel);
static void update_section(strand_t *strand, uint32_t start_pixel, pixelColor_t color);

bool running_sections_light_control_module_step(strand_t *strand, pixelColor_t color, bool new_mode)
{
    if(new_mode)
    {
        last_wake_time = xTaskGetTickCount();
    }
    while(last_wake_time < xTaskGetTickCount())
    {
        start_pixel++;
        last_wake_time += update_rate_ms;
    }

    if(strand)  update_leds(strand, color, start_pixel);
    return true;
}

void update_leds(strand_t *strand, pixelColor_t color, uint32_t start_pixel)
{
    const pixelColor_t colors[] = {
        {.r = 255, .g = 255, .b = 255, .w = 0}, //white
        {.r = 0,   .g = 0,   .b = 255, .w = 0}, //blue
        {.r = 0,   .g = 255, .b = 255, .w = 0}, //cyan
        {.r = 0,   .g = 255, .b = 0,   .w = 0}, //green
        {.r = 255, .g = 0,   .b = 255, .w = 0}, //megenta
        {.r = 139, .g = 69,  .b = 19,  .w = 0}, //brown
        {.r = 240, .g = 255, .b = 255, .w = 0}, //azure
        {.r = 255, .g = 20,  .b = 147, .w = 0}, //pink
        {.r = 255, .g = 0,   .b = 0,   .w = 0}, //red
    };

    memset(strand->pixels,    0x00, sizeof(strand->pixels[0]) * strand->numPixels);
    memset(strand->state_buf, 0x00, sizeof(strand->pixels[0]) * strand->numPixels);

    for(uint32_t i = 0; i < section_count; i++)
    {
        uint32_t section_start_pixel = ((float)i / (float)section_count) * strand->numPixels * 2 + start_pixel;
        pixelColor_t section_color   = colors[i % (sizeof(colors) / sizeof(colors[0]))];
        update_section(strand, section_start_pixel, section_color);
    }
}


void update_section(strand_t *strand, uint32_t start_pixel, pixelColor_t color)
{
    
    for(uint32_t i = 0; i < section_size; i++)
    {
        uint32_t position = (start_pixel + i) % (strand->numPixels * 2);
        if(position >= strand->numPixels)
        {
            position -= strand->numPixels;
            position = strand->numPixels - position - 1;
        }
        if(position < strand->numPixels)
        {
            uint8_t* color_cnt = strand->state_buf;
            color_cnt[position]++;
            strand->pixels[position] = scale_color(&strand->pixels[position], &color, 1.f / color_cnt[position]);
        }
        else
        {
            ESP_LOGI(TAG, "%d", position);
        }
        
    }
}


void running_sections_light_control_module_set(uint32_t _update_rate, uint32_t _section_size, uint32_t _section_count)
{
    update_rate_ms = _update_rate > 0 ? _update_rate : 1;
    section_size   = _section_size;
    section_count  = _section_count > 0 ? _section_count : 1;
}
