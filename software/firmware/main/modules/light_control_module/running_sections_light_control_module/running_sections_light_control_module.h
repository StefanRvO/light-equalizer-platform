#include "esp32_digital_led_lib.h"
#include <stdbool.h>

#include "modules/c_link.h"

C_LINK bool running_sections_light_control_module_step(strand_t *strand, pixelColor_t color, bool new_mode);

C_LINK void running_sections_light_control_module_set(uint32_t _update_rate, uint32_t _section_size, uint32_t _section_count);