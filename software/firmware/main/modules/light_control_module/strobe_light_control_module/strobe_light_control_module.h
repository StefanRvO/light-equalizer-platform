#pragma once
#include "esp32_digital_led_lib.h"
#include "modules/protocol_module/protocol_module.h"
#include "modules/c_link.h"

C_LINK int strobe_light_control_module_init(void);

C_LINK bool strobe_light_control_module_step(strand_t *strand, pixelColor_t color, bool light_mode_changed);

C_LINK void strobe_light_spec_recv(light_mesh_message_t *msg);

C_LINK void strobe_light_spec_set(strobe_light_timing_spec_t timing_spec);
C_LINK strobe_light_timing_spec_t strobe_light_get_spec(void);
C_LINK struct timeval strobe_light_control_module_get_offset(void);
C_LINK void strobe_light_control_module_set_offset(struct timeval *_offset);