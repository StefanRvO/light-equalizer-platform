#include "esp_err.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "libs/strobe_lib/strobe_lib.h"
#include "strobe_light_control_module.h"
#include "modules/config_reader_module/config_reader_module.h"

#define DEFAULT_STROBE_LIGHT_ON_US 50000
#define DEFAULT_STROBE_LIGHT_OFF_US 200000

__attribute__((unused)) static const char *TAG = "STROBE_LIGHT_CONTROL";

typedef struct 
{
    strand_t *strand;
    pixelColor_t color;
} callback_data_t;

static strobe_light_data_t strobe_data; 

static void on(strand_t *strand, pixelColor_t color);
static void off(strand_t *strand);
static void callback_on(callback_data_t *data);
static void callback_off(callback_data_t *data);


int strobe_light_control_module_init(void)
{
    strobe_lib_reset_struct(&strobe_data);
    strobe_data.timing_spec.on_time.tv_usec  = DEFAULT_STROBE_LIGHT_ON_US;
    strobe_data.timing_spec.off_time.tv_usec = DEFAULT_STROBE_LIGHT_OFF_US;
    strobe_data.strobe_on_func               = (strobe_func_t)callback_on;
    strobe_data.strobe_off_func              = (strobe_func_t)callback_off;

    strobe_data.offset.tv_usec = config_reader_get_int32("STROBE_OFF_S", 0);
    strobe_data.offset.tv_sec  = config_reader_get_int32("STROBE_OFF_US", 0);

    return 0;
}

struct timeval strobe_light_control_module_get_offset(void)
{
    return strobe_data.offset;
}

void strobe_light_control_module_set_offset(struct timeval *_offset)
{
    strobe_data.offset = *_offset;
    config_reader_set_int32("STROBE_OFF_S", (int32_t)strobe_data.offset.tv_usec);
    config_reader_set_int32("STROBE_OFF_US", (int32_t)strobe_data.offset.tv_sec);
}

bool strobe_light_control_module_step(strand_t *strand, pixelColor_t color, bool light_mode_changed)
{
    callback_data_t callback_data = {
        .strand   = strand,
        .color         = color
    };
    strobe_data.strobe_callback_data = &callback_data;
    return strobe_lib_step(&strobe_data);
}

void callback_on(callback_data_t *data)
{
    if(data->strand) on(data->strand, data->color);
}

void callback_off(callback_data_t *data)
{
    if(data->strand) off(data->strand);
}

void on(strand_t *strand, pixelColor_t color)
{
    for(uint32_t i = 0; i < strand->numPixels; i++) strand->pixels[i] = color;
}

void off(strand_t *strand)
{
    for(uint32_t i = 0; i < strand->numPixels; i++) strand->pixels[i].raw32 = 0;
}

void strobe_light_spec_recv(light_mesh_message_t *msg)
{
    strobe_light_timing_spec_message_t* spec_msg = (strobe_light_timing_spec_message_t*)msg->payload;
    strobe_light_spec_set(spec_msg->timing_spec);
}

void strobe_light_spec_set(strobe_light_timing_spec_t timing_spec)
{
    strobe_data.timing_spec = timing_spec;
}

strobe_light_timing_spec_t strobe_light_get_spec(void)
{
    return strobe_data.timing_spec;
}