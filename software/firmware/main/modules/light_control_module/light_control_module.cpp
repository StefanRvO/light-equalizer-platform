#include "light_control_module.hpp"
#include "modules/gpio_utilities/gpio_utilities.h"

#include "esp_err.h"
#include "esp_log.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>
#include "esp32_digital_led_lib.h"

#include "modules/gpio_utilities/gpio_utilities.h"
#include "modules/light_management_module/light_management_module.h"
#include "strobe_light_control_module/strobe_light_control_module.h"
#include "glitter_light_control_module/glitter_light_control_module.h"
#include "frequency_control_module/frequency_control_module.h"
#include "tetris_control_module/tetris_control_module.h"
#include "gol_control_module/gol_control_module.h"
#include "running_ring_control_module/running_ring_control_module.h"
#include "pulse_light_control_module/pulse_light_control_module.h"
#include "running_sections_light_control_module/running_sections_light_control_module.h"
#include "rainbow_control_module/rainbow_control_module.h"
#include "matrix_control_module/matrix_control_module.h"
#include "fire_control_module/fire_control_module.h"
#include "shooting_control_module/shooting_control_module.hpp"
#include "modules/config_reader_module/config_reader_module.h"
#include "modules/power_supply_module/power_supply_module.h"

#include "libs/led_panel/led_panel.h"
#include "libs/led_ring/led_ring.h"
#include "libs/gamma_correction/gamma_correction.h"
#include "libs/color_utils/color_utils.h"
#include "libs/mandelbrot_lib/mandelbrot_lib.h"

__attribute__((unused)) static const char *TAG = "LIGHT_CONTROL";

static light_mode_type_t light_mode  = LIGHT_MODE_OFF;
static light_mode_type_t global_mode = LIGHT_MODE_OFF;
static light_mode_type_t local_mode  = LIGHT_MODE_OFF;
static bool is_mode_global = true;
static bool light_mode_changed = true;
static float strength = 1.f;
static pixelColor_t color = {.raw32 = 0x00000000};

static TickType_t next_update;

static void update_mode(void);

void light_control_module_init(led_panel_t* panel)
{
    ESP_LOGI(TAG,"INIT LIGHT CONTROL");

    power_supply_module_init();
    power_supply_module_set(false, POWER_SUPPLY_LEFT);
    power_supply_module_set(false, POWER_SUPPLY_RIGHT);

    local_mode     = static_cast<light_mode_type_t>(config_reader_get_uint32("local_mode", LIGHT_MODE_OFF));
    is_mode_global = config_reader_get_uint32("is_mode_global", true);

    running_ring_control_init();
    next_update = xTaskGetTickCount() + 1000;

    int ret = 0;
    ret    |= strobe_light_control_module_init();
    ret    |= glitter_light_control_module_init();
    ret    |= frequency_control_module_init();
    ret    |= tetris_light_control_module_init(panel);
    ret    |= gol_light_control_module_init(panel);
    frequency_control_module_init();
    light_control_force_next_update();

}


void light_control_module_mode_recv(light_mesh_message_t *msg)
{
    light_mode_message_t *light_mode_msg = (light_mode_message_t *)msg->payload;
    light_control_module_set_global_light_mode(static_cast<light_mode_type_t>(light_mode_msg->global_mode),
                                               light_mode_msg->strength, light_mode_msg->color);
}

void light_control_module_set_global_light_mode(light_mode_type_t _light_mode, float _strength, pixelColor_t _color)
{
    if(light_mode != _light_mode)
    {
        light_mode_changed = true;
    }
    global_mode = _light_mode;
    strength    = _strength;
    color       = _color;
    update_mode();
}

void light_control_force_next_update(void)
{
    light_mode_changed = true;
}


bool light_control_step(strand_t* strand, led_panel_t* panel, led_ring_t* ring)
{
    if(xTaskGetTickCount() >= next_update)
    {
        next_update = xTaskGetTickCount() + 4;

        bool refresh = false;
        switch(light_mode)
        {
            case LIGHT_MODE_OFF:
                memset(strand->pixels, 0x00, sizeof(strand->pixels[0]) * strand->numPixels);
                refresh = true;
                break;
            case LIGHT_MODE_ON:
                fill_string(strand, color);
                refresh = true;
                break;
            case LIGHT_MODE_STROBE:
                refresh = strobe_light_control_module_step(strand, color, light_mode_changed);
                break;
            case LIGHT_MODE_GLITTER:
                refresh = glitter_light_control_module_step(strand, color, light_mode_changed);
                break;
            case LIGHT_MODE_FREQUENCY_PANEL:
            case LIGHT_MODE_FREQUENCY_RING:
            case LIGHT_MODE_FREQUENCY_STRING:
            case LIGHT_MODE_FREQUENCY_SINGLE_COLOR:
            case LIGHT_MODE_FREQUENCY_SINGLE_COLOR_BEAT:
            case LIGHT_MODE_FREQUENCY_PANEL_FALLING:
            case LIGHT_MODE_FIRE_AUDIO:
            case LIGHT_MODE_FREQUENCY_GLITTER_COLOR:
                refresh = frequency_control_module_step(strand, panel, ring, light_mode, color, light_mode_changed);
                break;
            case LIGHT_MODE_TETRIS:
                refresh = tetris_light_control_module_step(panel, light_mode_changed);
                break;
            case LIGHT_MODE_GAME_OF_LIFE:
                refresh = gol_light_control_module_step(panel, light_mode_changed);
                break;
            case LIGHT_MODE_RUNNING_RING:
                refresh = running_ring_control_step(ring, color, light_mode_changed);
                break;
            case LIGHT_MODE_PULSE:
                refresh = pulse_light_control_module_step(strand, panel, ring, color, light_mode_changed);
                break;
            case LIGHT_MODE_RUNNING_SECTIONS:
                refresh = running_sections_light_control_module_step(strand, color, light_mode_changed);
                break;
            case LIGHT_MODE_RAINBOW:
                refresh = rainbow_light_control_module_step(strand, color, light_mode_changed);
                break;
            case LIGHT_MODE_MATRIX:
                refresh = matrix_control_lib_step(panel, color);
                break;
            case LIGHT_MODE_FIRE:
                refresh = fire_control_module_step(panel, color);
                break;
            case LIGHT_MODE_SHOOTING:
                refresh = shooting_control_module_step(strand, light_mode_changed);
                break;
            default:
                break;
        }
        power_supply_module_set(light_mode != LIGHT_MODE_OFF, POWER_SUPPLY_LEFT);
        power_supply_module_set(light_mode != LIGHT_MODE_OFF, POWER_SUPPLY_RIGHT);

        if(refresh)
        {
            light_mode_changed = false;
            return true;
        }
    }
    return false;
}

void light_control_set_local_mode(light_mode_type_t _mode)
{
    if(local_mode == _mode)
    {
        return;
    }
    local_mode  = _mode;

    config_reader_set_uint32("local_mode", local_mode);
    update_mode();
}

void light_control_set_is_global_mode(bool is_global)
{
    if(is_global == is_mode_global)
    {
        return;
    }
    is_mode_global  = is_global;

    config_reader_set_uint32("is_mode_global", is_mode_global);
    update_mode();
}


void update_mode(void)
{
    light_mode_type_t prev_mode = light_mode;
    light_mode = is_mode_global ? global_mode : local_mode;
    if(prev_mode != light_mode)
    ESP_LOGI(TAG, "light mode changed from %d to %d", prev_mode, light_mode);
}

light_mode_type_t light_control_module_get_local_mode(void)
{
    return local_mode;
}

bool light_control_module_is_mode_global(void)
{
    return is_mode_global;
}

float light_control_module_get_strength(void)
{
    return strength;
}