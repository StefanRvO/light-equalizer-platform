#pragma once
#include "esp32_digital_led_lib.h"
#include "modules/light_management_module/frequency_management_module/frequency_management_module.h"
#include "libs/frequency_control_lib/frequency_control_lib.h"

void frequency_led_control_string_set(strand_t* panel, float *fft_bins, freq_power_func_t* power_func, freq_scaling_t* scaling, bool fill);
