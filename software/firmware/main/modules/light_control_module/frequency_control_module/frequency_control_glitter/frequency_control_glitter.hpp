#pragma once

#include "esp32_digital_led_lib.h"

#include "modules/light_management_module/frequency_management_module/frequency_management_module.h"
#include "modules/c_link.h"
#include "libs/frequency_control_lib/frequency_control_lib.h"

C_LINK bool frequency_control_glitter(strand_t* string, frequency_data_t* data, freq_scaling_t* scaling);