#include "frequency_control_glitter.hpp"
#include "modules/led_strip_module/led_strip_module.hpp"
#include "libs/color_utils/color_utils.h"
#include <random>
#include <algorithm>

#define UPDATE_INTERVAL_MS 50

static TickType_t next_update = 0;

static void set_pixel(pixelColor_t& pixel, const float* freq_parts, std::uniform_real_distribution<float>& dist_freq, std::ranlux24_base& e2);

bool frequency_control_glitter(strand_t* string, frequency_data_t* data, freq_scaling_t* scaling)
{
    freq_scaling_t scaling_mod = *scaling;
    scaling_mod.multiplier /= 2;

    TickType_t cur_tick = xTaskGetTickCount();
    if(cur_tick >= next_update)
    {
        next_update = cur_tick + UPDATE_INTERVAL_MS;
        float* fft_bins = led_module_get_channel_order() == CHANNELORDER_LR ? data->fft[0] : data->fft[1];
        float total_power = 0;
        float freq_parts[BINS_TO_SEND] = {0};

        for(uint32_t i = 0; i < BINS_TO_SEND; i++)
        {
            total_power += fft_bins[i];
            freq_parts[i] = total_power;
        }
        float scale = freq_control_lib_scale_power(total_power, &scaling_mod);
        scale = scale > 1. ? 1. : scale;

        std::ranlux24_base e2(rand());

        std::uniform_real_distribution<float> dist_on(0, 1);
        std::uniform_real_distribution<float> dist_freq(0, total_power);

        for(uint32_t i = 0; i < string->numPixels; i++)
        {
            if(dist_on(e2) < scale)
            {
                set_pixel(string->pixels[i], freq_parts, dist_freq, e2);
            }
            else
            {
                string->pixels[i].raw32 = 0;
            }
        }
        return true;
    }
    return false;
}

void set_pixel(pixelColor_t& pixel, const float* freq_parts, std::uniform_real_distribution<float>& dist_freq, std::ranlux24_base& e2)
{
    float random_num = dist_freq(e2);
    const float* index_ptr = std::lower_bound(freq_parts, freq_parts + BINS_TO_SEND, random_num);
    if(index_ptr == freq_parts + BINS_TO_SEND)
    {
        pixel.raw32 = 0;
        return;
    }
    uint32_t bin_num = (index_ptr - freq_parts);

    pixelColor_t colors[] = {{0,   0,   255, 0},
                             {0,   255, 0,   0},
                             {255, 0,   0,   0}};
    pixel = scale_color_arr(colors, sizeof(colors) / sizeof(colors[0]), (float)bin_num / (float)(BINS_TO_SEND));
}