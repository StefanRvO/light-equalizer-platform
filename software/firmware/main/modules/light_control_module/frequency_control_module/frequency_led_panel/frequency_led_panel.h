#pragma once
#include "esp32_digital_led_lib.h"
#include "modules/light_management_module/frequency_management_module/frequency_management_module.h"
#include "libs/led_panel/led_panel.h"
#include "libs/frequency_control_lib/frequency_control_lib.h"

void frequency_led_control_panel_set(led_panel_t* panel, float *fft_bins, freq_power_func_t* power_func, freq_scaling_t* scaling, bool fill);
void frequency_control_module_led_panel_set(led_panel_t* panel, frequency_data_t* data, frequency_data_t* historic_data,
                                            bool show_current, bool show_peaks, 
                                            freq_power_func_t *power_func, freq_scaling_t* scaling,
                                            pixelColor_t color);
