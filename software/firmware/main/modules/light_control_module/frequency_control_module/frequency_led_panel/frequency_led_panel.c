#include "frequency_led_panel.h"
#include "libs/led_panel/led_panel.h"
#include "libs/color_utils/color_utils.h"

#include "libs/math_lib/math_lib.h"
#include "libs/frequency_control_lib/frequency_control_lib.h"
#include "modules/led_strip_module/led_strip_module.hpp"
#include "esp_log.h"


void frequency_control_module_led_panel_set(led_panel_t* panel, frequency_data_t* data, frequency_data_t* historic_data,
                                            bool show_current, bool show_peaks, 
                                            freq_power_func_t *power_func, freq_scaling_t* scaling,
                                            pixelColor_t color)
{
        memset(panel->strand->pixels, 0x00, led_panel_get_size(panel) * sizeof(panel->strand->pixels[0]));
 
        panel->mirror_x = true;
        if(show_current)
        {
            frequency_led_control_panel_set(panel,  led_module_get_channel_order() == CHANNELORDER_LR ? data->fft[0] : data->fft[1], power_func, scaling, true);
        }
        if(show_peaks)
        {
            frequency_led_control_panel_set(panel,  led_module_get_channel_order() == CHANNELORDER_LR ? historic_data->fft[0] : historic_data->fft[1], power_func, scaling, false);
        }
        panel->mirror_x = false;
}


void frequency_led_control_panel_set(led_panel_t* panel, float *fft_bins, freq_power_func_t* power_func, freq_scaling_t* scaling, bool fill)
{
    float *composed = malloc(panel->height * sizeof(float));

    freq_control_lib_compose_bins(composed, fft_bins, panel->height);
    for(uint32_t i = 0; i < panel->height; i++)
    {
        if(fill)
        {
            pixelColor_t color_start = {.r = 255, .g = 0, .b =0, .w = 0};
            pixelColor_t color_end   = {.r = 0,   .g = 0, .b =255, .w = 0};
            pixelColor_t color = scale_color(&color_start, &color_end, i/(float)panel->height);
            
            led_panel_fill_row(panel, i, &color, freq_control_lib_scale_power(composed[i], scaling));
        }
        else
        {
            pixelColor_t color   = {.r = 0,   .g = 255, .b =0, .w = 0};
            led_panel_set_pixel(panel, freq_control_lib_scale_power(composed[i], scaling) * panel->width, i, &color);
        }
    }
    free(composed);
}