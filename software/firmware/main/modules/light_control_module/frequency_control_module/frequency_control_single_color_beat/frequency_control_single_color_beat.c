#include "frequency_control_single_color_beat.h"
#include "libs/color_utils/color_utils.h"

#include "libs/math_lib/math_lib.h"
#include "libs/color_utils/color_utils.h"
#include "libs/frequency_control_lib/frequency_control_lib.h"
#include "esp_log.h"
#include "modules/led_strip_module/led_strip_module.hpp"

#define BEAT_THRESHOLD 2

#define SPECTRUMS 7
static running_avg_t avg[SPECTRUMS];

static void update_avg(running_avg_t* avg, float* values, uint32_t cnt);

void frequency_led_control_single_color_beat_init(void)
{
    for(uint32_t channel = 0; channel < 2; channel++)
    {
        for(uint32_t i = 0; i < SPECTRUMS; i++)
        {
            avg[i].decay_rate = 0.25;
            avg[i].avg = 0;
        }
    }
}

void frequency_led_control_single_color_beat_set_string(strand_t* string, float *fft_bins, 
                                                        running_avg_t* avg, float*current)
{
    int32_t max = -1;
    for(uint32_t i = 0; i < SPECTRUMS; i++)
    {
        if(current[i] > (avg[i].avg_prev * BEAT_THRESHOLD) && current[i] > 5)
        {
            if(max == -1 || current[i] > current[max])
            {
                max = i;
            }
        }
    }

    pixelColor_t color_start = {.r = 255, .g = 0, .b =0, .w = 0};
    pixelColor_t color_end   = {.r = 0,   .g = 0, .b =255, .w = 0};
    pixelColor_t color_off   = {.raw32 = 0};

    pixelColor_t color = scale_color(&color_start, &color_end, (float)max/(float)SPECTRUMS);
    if(max != -1)
    {
        fill_string(string, color);
    }
    else
    {
        fill_string(string, color_off);
    }
}

void frequency_led_control_single_color_beat_set(strand_t* string, frequency_data_t *data)
{
    float *composed = malloc(SPECTRUMS * sizeof(float));
    
    freq_control_lib_compose_bins(composed, led_module_get_channel_order() == CHANNELORDER_LR ? data->fft[0] : data->fft[1], SPECTRUMS);

    update_avg(avg, composed, SPECTRUMS);

    frequency_led_control_single_color_beat_set_string(string,  led_module_get_channel_order() == CHANNELORDER_LR ? data->fft[0] : data->fft[1], avg, composed);

    free(composed);
}

void update_avg(running_avg_t* avg, float* values, uint32_t cnt)
{
    for(uint32_t i = 0; i < cnt; i++)
    {
        statistics_lib_update_running_avg(&avg[i], values[i]);
    }
}