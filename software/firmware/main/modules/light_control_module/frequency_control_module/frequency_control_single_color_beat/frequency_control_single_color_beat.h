#pragma once
#include "esp32_digital_led_lib.h"
#include "modules/light_management_module/frequency_management_module/frequency_management_module.h"
#include "libs/frequency_control_lib/frequency_control_lib.h"
#include "libs/statistic_lib/statistics_lib.h"
void frequency_led_control_single_color_beat_set(strand_t* string, frequency_data_t *data);

void frequency_led_control_single_color_beat_init(void);