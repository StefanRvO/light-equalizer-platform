#pragma once
#include "libs/led_panel/led_panel.h"
#include "libs/frequency_control_lib/frequency_control_lib.h"
#include "modules/light_management_module/frequency_management_module/frequency_management_module.h"

bool frequency_fire_control_module_step(led_panel_t* panel, frequency_data_t *data, freq_scaling_t* scaling);
