#include "frequency_fire_control_module.h"

#include "libs/led_panel/led_panel.h"
#include "libs/color_utils/color_utils.h"

#include "libs/fire_lib/fire_lib.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "modules/led_strip_module/led_strip_module.hpp"

#define UPDATE_PERIOD 40
#define SPAWN_CHANGE 10

static TickType_t last_update = 0;

static const float blur_kernel[5][5] = {{0,    .0,   .0, .0, 0.},
                                        {0,    .245, .0, .0, 0.},
                                        {.245, .245, .0, .0, 0.},
                                        {0.,   .245, .0, .0, 0.},
                                        {0.,   .0,   .0, .0, 0.}};


static void update_fire(led_panel_t* panel, float* bins, freq_scaling_t* scaling);
static void shuffle(pixelColor_t *array, size_t n);

bool frequency_fire_control_module_step(led_panel_t* panel, frequency_data_t *data, freq_scaling_t* scaling)
{

    TickType_t cur = xTaskGetTickCount();
    if(cur > last_update + UPDATE_PERIOD)
    {
        last_update = cur;
        update_fire(panel, led_module_get_channel_order() == CHANNELORDER_LR ? data->fft[0] : data->fft[1], scaling);
        return true;
    }
    return false;
}

void update_fire(led_panel_t* panel, float* bins, freq_scaling_t* scaling)
{
    float *composed = malloc(panel->width * sizeof(float));

    freq_control_lib_compose_bins(composed, bins, panel->width);
    pixelColor_t bottom[panel->width];

    for(uint32_t i = 0; i < panel->width; i++)
    {
        pixelColor_t color_start = {.r = 255, .g = 0, .b =0, .w = 0};
        pixelColor_t color_end   = {.r = 0,   .g = 0, .b =255, .w = 0};
        pixelColor_t color_off   = {.raw32 = 0};
        pixelColor_t color = scale_color(&color_start, &color_end, i/(float)panel->width);
        color = scale_color(&color_off, &color, freq_control_lib_scale_power(composed[i], scaling));
        bottom[i] = color;
    }
    shuffle(bottom, panel->width);
    fire_lib_step(panel, bottom, 5, 5, blur_kernel);
    free(composed);
}

void shuffle(pixelColor_t *array, size_t n)
{
    if (n > 1) 
    {
        size_t i;
        for (i = 0; i < n - 1; i++) 
        {
          size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
          pixelColor_t t = array[j];
          array[j] = array[i];
          array[i] = t;
        }
    }
}