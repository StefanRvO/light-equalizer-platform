#pragma once
#include "esp32_digital_led_lib.h"
#include "modules/light_management_module/frequency_management_module/frequency_management_module.h"
#include "modules/protocol_module/protocol_module.h"

#include "libs/led_panel/led_panel.h"
#include "libs/led_ring/led_ring.h"
#include "libs/frequency_control_lib/frequency_control_lib.h"

#include "modules/c_link.h"

C_LINK int frequency_control_module_init(void);

C_LINK bool frequency_control_module_step(strand_t* strand, led_panel_t* panel, led_ring_t* ring, light_mode_type_t light_mode, pixelColor_t color, bool new_mode);

C_LINK void frequency_control_module_set_frequencies(frequency_data_t* data, uint8_t seq_cnt);

C_LINK void frequency_control_module_recv(light_mesh_message_t *msg);

C_LINK void frequency_control_module_set_power_func(freq_power_func_t* func);
C_LINK void frequency_control_module_set_scaling(freq_scaling_t* _scaling);
C_LINK void frequency_control_module_set_decay_rate(float rate);
C_LINK void frequency_control_module_set_show_peaks(bool peaks);
C_LINK void frequency_control_module_set_show_current(bool current);
