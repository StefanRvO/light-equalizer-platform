#include "frequency_matrix_control.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>


#include "libs/color_utils/color_utils.h"
#include "libs/gamma_correction/gamma_correction.h"
#include "modules/led_strip_module/led_strip_module.hpp"

#define UPDATE_PERIOD 50

TickType_t last_update = 0;

static void update_matrix(led_panel_t* panel, float *bins, freq_scaling_t* scaling);

bool frequency_matrix_control_step(led_panel_t* panel, frequency_data_t *data, freq_scaling_t* scaling)
{
    TickType_t cur = xTaskGetTickCount();
    if(cur > last_update + UPDATE_PERIOD)
    {
        last_update = cur;
        update_matrix(panel, led_module_get_channel_order() == CHANNELORDER_LR ? data->fft[0] : data->fft[1], scaling);
        return true;
    }
    return false;
}

void update_matrix(led_panel_t* panel, float *bins, freq_scaling_t* scaling)
{
    float *composed = malloc(panel->width * sizeof(float));

    freq_control_lib_compose_bins(composed, bins, panel->width);

    for(uint32_t i = 0; i < panel->width; i++)
    {
        pixelColor_t color_start = {.r = 255, .g = 0, .b =0, .w = 0};
        pixelColor_t color_end   = {.r = 0,   .g = 0, .b =255, .w = 0};
        pixelColor_t color_off   = {.raw32 = 0};
        pixelColor_t color = scale_color(&color_start, &color_end, i/(float)panel->width);
        color = scale_color(&color_off, &color, freq_control_lib_scale_power(composed[i], scaling));
        color.r = gamma_table[color.r];
        color.g = gamma_table[color.g];
        color.b = gamma_table[color.b];

        led_panel_shift_col(panel, i);
        led_panel_set_pixel(panel, i, panel->height - 1, &color);
    }
    free(composed);
}