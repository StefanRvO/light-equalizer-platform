#include "libs/led_panel/led_panel.h"

#include "modules/light_management_module/frequency_management_module/frequency_management_module.h"
#include "libs/frequency_control_lib/frequency_control_lib.h"

bool frequency_matrix_control_step(led_panel_t* panel, frequency_data_t *data, freq_scaling_t* scaling);