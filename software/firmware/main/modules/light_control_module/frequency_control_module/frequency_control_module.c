#include "esp_err.h"
#include "esp_log.h"

#include "frequency_control_module.h"
#include "drivers/alive_led_driver/alive_led_driver.h"
#include "frequency_led_panel/frequency_led_panel.h"
#include "frequency_control_single_color_beat/frequency_control_single_color_beat.h"
#include "frequency_control_single_color/frequency_control_single_color.h"
#include "frequency_matrix_control/frequency_matrix_control.h"
#include "frequency_fire_control_module/frequency_fire_control_module.h"
#include "frequency_control_glitter/frequency_control_glitter.hpp"
#include "libs/half_float/half_float.h"
#include "libs/statistic_lib/statistics_lib.h"

__attribute__((unused)) static const char *TAG = "FREQUENCY_LIGHT_CONTROL";

#define HISTORIC_DATA_UPDATE_RATE 50

static frequency_data_t freq_data = {0};
static frequency_data_t freq_data_historic = {0};

static float peak_decay_rate = 0.1;
static bool show_peaks   = true;
static bool show_current = true;

static freq_power_func_t power_func = {.power = 16000, .exponent = 3, .y_cross = 0};
static freq_scaling_t scaling = {.log_base = 1.2, .multiplier = 1./28.};
static bool new_data = false;
static TickType_t next_historic_update = 0;

static void decay_historic_data(void);
static void refresh_historic_data(void);

int frequency_control_module_init(void)
{
    frequency_led_control_single_color_beat_init();
    return 0;
}

bool frequency_control_module_step(strand_t* strand, led_panel_t* panel, led_ring_t* ring, light_mode_type_t light_mode, pixelColor_t color, bool new_mode)
{
    bool ret = false;
    TickType_t current = xTaskGetTickCount();
    if(current >= next_historic_update)
    {
        decay_historic_data();
        next_historic_update = current + HISTORIC_DATA_UPDATE_RATE;
    }
    switch (light_mode)
    {
        case LIGHT_MODE_FREQUENCY_PANEL:
            if(new_data)
            {
                frequency_control_module_led_panel_set(panel, &freq_data, &freq_data_historic, show_current, show_peaks,  &power_func, &scaling,color);
                ret = true;
            }
            break;
        case LIGHT_MODE_FREQUENCY_STRING:
            break;
        
        case LIGHT_MODE_FREQUENCY_RING:
            break;
        case LIGHT_MODE_FREQUENCY_SINGLE_COLOR:
            if(new_data)
            {
                frequency_led_control_single_color_set(strand, &freq_data, &scaling);
                ret = true;
            }
            break;
        case LIGHT_MODE_FREQUENCY_SINGLE_COLOR_BEAT:
            if(new_data)
            {
                frequency_led_control_single_color_beat_set(strand, &freq_data);
                ret = true;
            }
            break;
        case LIGHT_MODE_FREQUENCY_PANEL_FALLING:
            ret = frequency_matrix_control_step(panel, &freq_data, &scaling);
            break;
        
        case LIGHT_MODE_FIRE_AUDIO:
            ret = frequency_fire_control_module_step(panel, &freq_data, &scaling);
            break;
        case LIGHT_MODE_FREQUENCY_GLITTER_COLOR:
            ret = frequency_control_glitter(strand, &freq_data, &scaling);
            break;
        default:
            break;
    }
    new_data = false;
    return ret;
}

void frequency_control_module_set_frequencies(frequency_data_t* data, uint8_t seq_cnt)
{
    if(seq_cnt != (uint8_t)(freq_data.seq_count + 1))
    {
        ESP_LOGI(TAG, "Data was lost, got seq %d, expected %d", data->seq_count, freq_data.seq_count + 1);
    }
    memcpy(&freq_data, data, sizeof(*data));
    freq_data.seq_count = seq_cnt;
    refresh_historic_data();
    new_data = true;
}

void frequency_control_module_recv(light_mesh_message_t *msg)
{
    frequency_data_message_t* data = (frequency_data_message_t*)msg->payload;
    half_float_to_float_arr(data->fft[0], freq_data.fft[0], sizeof(freq_data.fft[0]) / sizeof(freq_data.fft[0][0]));
    half_float_to_float_arr(data->fft[1], freq_data.fft[1], sizeof(freq_data.fft[1]) / sizeof(freq_data.fft[1][0]));

    frequency_control_module_set_frequencies(&freq_data, data->seq_count);
}

void frequency_control_module_set_power_func(freq_power_func_t* func)
{
    power_func = *func;
}

void frequency_control_module_set_scaling(freq_scaling_t* _scaling)
{
    scaling = *_scaling;
}

void frequency_control_module_set_decay_rate(float rate)
{
    peak_decay_rate = rate;
}

void frequency_control_module_set_show_peaks(bool peaks)
{
    show_peaks = peaks;
}

void frequency_control_module_set_show_current(bool current)
{
    show_current = current;
}

void decay_historic_data(void)
{
    for(uint32_t channel = 0; channel <= 1; channel++)
    {
        for(uint32_t bin = 0; bin < BINS_TO_SEND; bin++)
        {
            freq_data_historic.fft[channel][bin] *= (1- peak_decay_rate);
        }
    }
}

void refresh_historic_data(void)
{
    for(uint32_t channel = 0; channel <= 1; channel++)
    {
        for(uint32_t bin = 0; bin < BINS_TO_SEND; bin++)
        {
            if(freq_data_historic.fft[channel][bin] < freq_data.fft[channel][bin])
            {
                freq_data_historic.fft[channel][bin] = freq_data.fft[channel][bin];
            }
        }
    }
}