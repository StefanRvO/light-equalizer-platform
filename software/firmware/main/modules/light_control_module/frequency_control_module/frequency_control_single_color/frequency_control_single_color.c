#include "frequency_control_single_color.h"
#include "libs/color_utils/color_utils.h"

#include "libs/math_lib/math_lib.h"
#include "libs/color_utils/color_utils.h"
#include "libs/frequency_control_lib/frequency_control_lib.h"
#include "esp_log.h"


void frequency_led_control_single_color_set_string(strand_t* string, float *fft_bins, freq_scaling_t* scaling)
{
    //Find average bin weighted by power
    float total_power = 0;
    float sum = 0;
    for(uint32_t i = 0; i < BINS_TO_SEND; i++)
    {
        sum += fft_bins[i] * i;
        total_power += fft_bins[i];
    }
    float avg_bin = sum/total_power;
    pixelColor_t color_start = {.r = 255, .g = 0, .b =0, .w = 0};
    pixelColor_t color_end   = {.r = 0,   .g = 0, .b =255, .w = 0};
    pixelColor_t color_off   = {.raw32 = 0};

    pixelColor_t color = scale_color(&color_start, &color_end, avg_bin/(float)BINS_TO_SEND);
    float scale = freq_control_lib_scale_power(total_power, scaling);
    color = scale_color(&color_off, &color, scale);

    fill_string(string, color);

}

void frequency_led_control_single_color_set(strand_t* string, frequency_data_t *data, freq_scaling_t* scaling)
{
    frequency_led_control_single_color_set_string(string,  data->fft[0], scaling);
}