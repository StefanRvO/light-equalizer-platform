#include "pulse_panel_control_module.h"
#include <string.h>
#include "esp_err.h"
#include "esp_log.h"

#include "libs/color_utils/color_utils.h"

static void show(led_panel_t *panel, float progress, const pixelColor_t color);

__attribute__((unused)) static const char *TAG = "PULSE_LIGHT_PANEL";


void pulse_panel_control_module_step(led_panel_t* panel, float progress, const pixelColor_t color)
{
    show(panel, progress, color);
}

void show(led_panel_t *panel, float progress, const pixelColor_t color)
{
    memset(panel->strand->pixels, 0x00, led_panel_get_size(panel) * sizeof(panel->strand->pixels[0]));
    uint8_t row = progress / 2 * panel->height;
    uint8_t inner_row = row + 1;
    float strength = 1 - (progress / 2 * panel->height - row);
    float inner_strength = 1 - strength;

    pixelColor_t off = {.raw32 = 0};
    pixelColor_t outer_color = scale_color(&off, &color, strength);
    pixelColor_t inner_color = scale_color(&off, &color, inner_strength);
    led_panel_fill_row(panel, row, &outer_color, 1);
    led_panel_fill_row(panel, panel->height - row - 1, &outer_color, 1);
    if(inner_row < panel->height)
    {
        led_panel_fill_row(panel, inner_row, &inner_color, 1);
        led_panel_fill_row(panel, panel->height - inner_row - 1, &inner_color, 1);
    }
}