#pragma once
#include "libs/led_panel/led_panel.h"

void pulse_panel_control_module_step(led_panel_t* panel, float progress, const pixelColor_t color);