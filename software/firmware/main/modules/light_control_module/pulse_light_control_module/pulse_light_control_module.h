#include "libs/led_ring/led_ring.h"
#include "libs/led_panel/led_panel.h"

#include <stdbool.h>

#include "modules/c_link.h"

typedef struct {
    uint32_t pulse_time_ms;
    int8_t direction;
    bool single_shot;
} pulse_light_data_t;

C_LINK bool pulse_light_control_module_step(strand_t* strand, led_panel_t* panel, led_ring_t* ring, pixelColor_t color, bool new_mode);
 
C_LINK void pulse_light_control_module_set(pulse_light_data_t* data);