#pragma once
#include "libs/led_ring/led_ring.h"

void pulse_ring_control_module_step(led_ring_t* ring, float progress, const pixelColor_t color);