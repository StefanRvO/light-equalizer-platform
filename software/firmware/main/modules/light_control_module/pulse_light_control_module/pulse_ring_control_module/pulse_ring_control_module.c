#include "pulse_ring_control_module.h"
#include "libs/color_utils/color_utils.h"
#include <string.h>
#include "esp_err.h"
#include "esp_log.h"

__attribute__((unused)) static const char *TAG = "PULSE_RING";

static void show(led_ring_t *ring, float progress, const pixelColor_t color);

void pulse_ring_control_module_step(led_ring_t* ring, float progress, const pixelColor_t color)
{
    show(ring, progress, color);
}

void show(led_ring_t *ring, float progress, const pixelColor_t color)
{
    if(ring->rings == 0)
    {
        return;
    }
    memset(ring->strand->pixels, 0x00, led_ring_get_size(ring) * sizeof(ring->strand->pixels[0]));
    uint8_t outer_ring = ring->rings * progress;
    uint8_t inner_ring = outer_ring + 1;
    float outer_strength = 1 - ((ring->rings * progress) - outer_ring);
    float inner_strength = 1 - outer_strength;

    pixelColor_t off = {.raw32 = 0};
    pixelColor_t outer_color = scale_color(&off, &color, outer_strength);
    pixelColor_t inner_color = scale_color(&off, &color, inner_strength);
    if(inner_ring < ring->rings)
    {
        fill_ring(ring, outer_ring, &outer_color);
        fill_ring(ring, inner_ring, &inner_color);
    }
    else
    {
        fill_ring(ring, ring->rings - 1, &color);
    }
}
