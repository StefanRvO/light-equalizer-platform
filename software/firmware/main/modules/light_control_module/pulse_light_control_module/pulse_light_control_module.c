#include "pulse_light_control_module.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "esp_err.h"
#include "esp_log.h"

#include "modules/config_reader_module/config_reader_module.h"
#include "pulse_ring_control_module/pulse_ring_control_module.h"
#include "pulse_string_control_module/pulse_string_control_module.h"
#include "pulse_panel_control_module/pulse_panel_control_module.h"

static pulse_light_data_t pulse_data = {.pulse_time_ms = 1000, 
                                        .direction = 0,
                                        .single_shot = false
                                       };
static float progress         = 0;
static TickType_t next_update = 0;
static int8_t cur_direction   = 1;

__attribute__((unused)) static const char *TAG = "PULSE_LIGHT";


void pulse_light_control_module_set(pulse_light_data_t* data)
{
    pulse_data = *data;
    if(pulse_data.single_shot)
    {
        cur_direction = pulse_data.direction;
    }
}

bool pulse_light_control_module_step(strand_t* strand, led_panel_t* panel, led_ring_t* ring, pixelColor_t color, bool new_mode)
{
    TickType_t current_tick = xTaskGetTickCount();
    if(new_mode)
    {
        next_update = current_tick;
    }
    if(next_update <= current_tick)
    {
        next_update += 10;
        progress += 10. / pulse_data.pulse_time_ms * cur_direction;
        if(progress > 1)
        {
            progress = 1;
            if(!pulse_data.single_shot) cur_direction = -cur_direction;
        }
        if(progress < 0)
        {
            progress = 0;
            if(!pulse_data.single_shot) cur_direction = -cur_direction;
        }
        if(new_mode)
        {
            if(cur_direction == -1)
            {
                progress = 1;
            }
            else if(cur_direction == 1)
            {
                progress = 0;
            }
        }
        switch(config_reader_get_light_type())
        {
            case LIGHT_TYPE_STRING:
                pulse_string_control_module_step(strand, progress, color);
                break;
            case LIGHT_TYPE_PANEL:
                pulse_panel_control_module_step(panel, progress, color);
                break;
            case LIGHT_TYPE_RING:
                pulse_ring_control_module_step(ring, progress, color);
                break;
            default:
                break;
        }
        return true;
    }
    return false;
}