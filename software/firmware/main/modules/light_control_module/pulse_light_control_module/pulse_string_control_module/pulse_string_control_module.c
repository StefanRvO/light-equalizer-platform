#include "pulse_string_control_module.h"

#define PULSE_SIZE 4


static void show(strand_t *stand, float progress, const pixelColor_t color);

void pulse_string_control_module_step(strand_t* strand, float progress, const pixelColor_t color)
{
    show(strand, progress, color);
}

void show(strand_t *strand, float progress, const pixelColor_t color)
{
    memset(strand->pixels, 0x00, strand->numPixels * sizeof(strand->pixels[0]));
    int32_t position = progress / 2 * strand->numPixels;
    for(int32_t i = -PULSE_SIZE / 2; i <= PULSE_SIZE / 2; i++)
    {
        int32_t pos = position + i;
        if(pos > 0 && pos < strand->numPixels)
        {
            strand->pixels[pos] = color;
        }
        pos = strand->numPixels - pos;

        if(pos > 0 && pos < strand->numPixels)
        {
            strand->pixels[pos] = color;
        }
    }
}