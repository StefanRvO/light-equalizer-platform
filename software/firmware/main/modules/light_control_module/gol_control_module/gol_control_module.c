#include "gol_control_module.h"
#include "esp_err.h"
#include "esp_log.h"
#include "libs/game_of_life/game_of_life.h"
#include "libs/led_panel/led_panel.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

__attribute__((unused)) static const char *TAG = "GOL_LIGHT_CONTROL";

static game_of_life_t game;
static TickType_t next_update;
static uint32_t update_rate = 80;

static void display_game(game_of_life_t* game, led_panel_t* panel);
static void game_step(game_of_life_t* game, led_panel_t* panel);
static const pixelColor_t *map_color(uint8_t color_val);
void restart_if_changed(game_of_life_t* game, led_panel_t* panel, bool new_mode);

int gol_light_control_module_init(led_panel_t* panel)
{
    game.board  = panel->strand->state_buf;
    game_of_life_init(&game,  1, 1, true);
    next_update  = xTaskGetTickCount();
    return 0;
}

void gol_light_control_module_set_update_rate(uint32_t _update_rate)
{
    update_rate = _update_rate;
}

bool gol_light_control_module_step(led_panel_t* panel, bool new_mode)
{
    game.board  = panel->strand->state_buf;

    TickType_t cur_tick = xTaskGetTickCount();
    if(cur_tick >= next_update)
    {
        next_update = cur_tick + update_rate;

        restart_if_changed(&game, panel, new_mode);

        game_step(&game, panel);
        return true;
    }
    return false;
}

void restart_if_changed(game_of_life_t* game, led_panel_t* panel, bool new_mode)
{
    if(game->height != panel->height || game->width != panel->width || new_mode)
    {
        game_of_life_destroy(game);
        game_of_life_init(game, panel->width, panel->height, 1);
    }
}


void game_step(game_of_life_t* game, led_panel_t* panel)
{
    game_of_life_tick(game);
    display_game(game, panel);
}

const pixelColor_t *map_color(uint8_t color_val)
{
    static const pixelColor_t colors[] = {
        {.r = 0,   .g = 0,   .b = 0,   .w = 0}, //black
        {.r = 0,   .g = 0,   .b = 255, .w = 0}, //blue
        {.r = 0,   .g = 255, .b = 255, .w = 0}, //cyan
        {.r = 0,   .g = 255, .b = 0,   .w = 0}, //green
        {.r = 0,   .g = 255, .b = 0,   .w = 0}, //green
        {.r = 255, .g = 0,   .b = 255, .w = 0}, //megenta
        {.r = 255, .g = 0,   .b = 0,   .w = 0}, //red
        {.r = 255, .g = 0,   .b = 0,   .w = 0}, //red
        {.r = 255, .g = 0,   .b = 0,   .w = 0}, //red
        {.r = 255, .g = 0,   .b = 0,   .w = 0}, //red
    };
    return &colors[color_val];
}

void display_game(game_of_life_t* game, led_panel_t* panel)
{
    for(uint32_t i = 0; i < panel->width; i++)
    {
        for(uint32_t j = 0; j < panel->height; j++)
        {
            const pixelColor_t* color = map_color(0);
            if(game_of_life_get(game, i, j))
            {
                color = map_color(game_of_life_neighbour_cnt(game, i, j) + 1);
            }
            led_panel_set_pixel(panel, i, j, color);
        }
    }
}