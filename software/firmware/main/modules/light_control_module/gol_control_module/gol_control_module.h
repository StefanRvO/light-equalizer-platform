#pragma once
#include <stdbool.h>
#include "esp32_digital_led_lib.h"

#include "libs/led_panel/led_panel.h"

#ifdef __cplusplus
extern "C" {
#endif

int gol_light_control_module_init(led_panel_t* panel);
bool gol_light_control_module_step(led_panel_t* panel, bool new_mode);
void gol_light_control_module_set_update_rate(uint32_t _update_rate);


#ifdef __cplusplus
} // closing brace for extern "C"

#endif