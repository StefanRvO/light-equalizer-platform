#include "tetris_control_module.h"
#include "esp_err.h"
#include "esp_log.h"
#include "libs/tetris/tetris.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

__attribute__((unused)) static const char *TAG = "TETRIS_LIGHT_CONTROL";

static tetris_game game;
static tetris_move next_move;
static TickType_t next_update;

static void display_game(tetris_game* game, led_panel_t* panel);
static void game_step(tetris_game* game, led_panel_t* panel);
const static pixelColor_t *map_color(uint8_t color_val);
static void game_restart(tetris_game* game, led_panel_t* panel);
static void restart_if_changed(tetris_game* game, led_panel_t* panel, bool new_mode);

int tetris_light_control_module_init(led_panel_t* panel)
{
    game.board = panel->strand->state_buf;

    tg_init(&game,  1, 1);
    next_update  = xTaskGetTickCount();
    return 0;
}

bool tetris_light_control_module_step(led_panel_t* panel, bool new_mode)
{
    game.board  = panel->strand->state_buf;
    TickType_t cur_tick = xTaskGetTickCount();
    if(cur_tick >= next_update)
    {
        next_update = cur_tick + 10;

        restart_if_changed(&game,  panel,  new_mode);

        game_step(&game,  panel);
        next_move = TM_NONE;
        return true;
    }
    return false;
}

void restart_if_changed(tetris_game* game, led_panel_t* panel, bool new_mode)
{
    if(game->rows != panel->height || game->cols != panel->width || new_mode)
    {
        game_restart(game, panel);
    }
}

void game_step(tetris_game* game, led_panel_t* panel)
{
    if(!tg_tick(game, next_move))
    {
        game_restart(game, panel);
    }
    display_game(game, panel);
}

void game_restart(tetris_game* game, led_panel_t* panel)
{
    int width = panel->width;
    int height = panel->height;
    if(width * height > panel->strand->numPixels)
    {
        height = panel->strand->numPixels / width;
    }
    height = height < 5 ? 5 : height;
    width = width < 5 ? 5 : width;
    tg_init(game, height, width);
}

const pixelColor_t *map_color(uint8_t color_val)
{
    static const pixelColor_t colors[] = {
        {.r = 0,   .g = 0,   .b = 0,   .w = 0}, //black
        {.r = 0,   .g = 255, .b = 255, .w = 0}, //cyan
        {.r = 0,   .g = 0,   .b = 255, .w = 0}, //blue
        {.r = 255, .g = 255, .b = 255, .w = 0}, //white
        {.r = 255, .g = 255, .b = 0,   .w = 0}, //yellow
        {.r = 0,   .g = 255, .b = 0,   .w = 0}, //green
        {.r = 255, .g = 0,   .b = 255, .w = 0}, //megenta
        {.r = 255, .g = 0,   .b = 0,   .w = 0}, //red
    };
    return &colors[color_val];
}

void display_game(tetris_game* game, led_panel_t* panel)
{
    for(uint32_t i = 0; i < panel->width; i++)
    {
        for(uint32_t j = 0; j < panel->height; j++)
        {
            const pixelColor_t* color = map_color(tg_get(game, j, i));
            led_panel_set_pixel(panel, i, panel->height - 1 - j, color);
        }
    }
}

void tetris_control_module_set_move(uint8_t move)
{
    next_move = move;
}