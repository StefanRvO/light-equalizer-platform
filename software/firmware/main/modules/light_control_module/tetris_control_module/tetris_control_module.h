#pragma once
#include <stdbool.h>
#include "esp32_digital_led_lib.h"

#include "libs/led_panel/led_panel.h"

#include "modules/c_link.h"


C_LINK int tetris_light_control_module_init(led_panel_t* panel);
C_LINK bool tetris_light_control_module_step(led_panel_t* panel, bool new_mode);
C_LINK void tetris_control_module_set_move(uint8_t move);
