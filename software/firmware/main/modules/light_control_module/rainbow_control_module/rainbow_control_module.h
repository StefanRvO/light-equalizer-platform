#pragma once

#include "esp32_digital_led_lib.h"
#include "modules/c_link.h"

C_LINK bool rainbow_light_control_module_step(strand_t *strand, pixelColor_t color, bool new_mode);
C_LINK void rainbow_light_control_module_set(uint32_t _update_rate);