#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "esp_err.h"
#include "esp_log.h"

#include "libs/color_utils/color_utils.h"
#include "rainbow_control_module.h"

#include <string.h>

__attribute__((unused)) static const char *TAG = "RAINBOW_LIGHT_CONTROL";

static uint32_t update_rate_ms = 1;
static float progress = 0;

static void update_leds(strand_t *strand, pixelColor_t color, float progress);
static void update_led(pixelColor_t *pixel, pixelColor_t color, float progress);

bool rainbow_light_control_module_step(strand_t *strand, pixelColor_t color, bool new_mode)
{
    progress += update_rate_ms / 10000.;
    if(progress > 1)
    {
        progress -= 1;
    }

    if(strand) update_leds(strand, color, progress);
    return true;
}

void update_leds(strand_t *strand, pixelColor_t color, float progress)
{
    //Map the progress to all pixels on the strand and assign a color to it
    for(uint32_t i = 0; i < strand->numPixels; i++)
    {
        float led_progress = progress + (float)i / (float) strand->numPixels;
        led_progress -= (uint32_t)led_progress;
        update_led(&strand->pixels[i], color, led_progress);
    }
}

void update_led(pixelColor_t *pixel, pixelColor_t color, float progress)
{
    static const pixelColor_t colors[] = {
        {.r = 255, .g = 255, .b = 255, .w = 0}, //white
        {.r = 0,   .g = 0,   .b = 255, .w = 0}, //blue
        {.r = 0,   .g = 255, .b = 255, .w = 0}, //cyan
        {.r = 0,   .g = 255, .b = 0,   .w = 0}, //green
        {.r = 255, .g = 0,   .b = 255, .w = 0}, //megenta
        {.r = 139, .g = 69,  .b = 19,  .w = 0}, //brown
        {.r = 240, .g = 255, .b = 255, .w = 0}, //azure
        {.r = 255, .g = 20,  .b = 147, .w = 0}, //pink
        {.r = 255, .g = 0,   .b = 0,   .w = 0}, //red
    };

    const uint32_t color_cnt = sizeof(colors) / sizeof(colors[0]);
    uint32_t start_color = progress * color_cnt;
    uint32_t end_color = start_color + 1;
    *pixel = scale_color(&colors[start_color % color_cnt], 
                         &colors[end_color % color_cnt],
                         progress * color_cnt - start_color);
}
void rainbow_light_control_module_set(uint32_t _update_rate)
{
    update_rate_ms = _update_rate;
}
