#include "esp_err.h"
#include "esp_log.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "running_ring_control_module.h"
#include "libs/led_ring/led_ring.h"
#include <string.h>

__attribute__((unused)) static const char *TAG = "RUNNING_RING_CONTROL";

static TickType_t next_update = 0;
static uint8_t positions[MAX_RINGS] = {0};

static int8_t directions[MAX_RINGS];

static void update_positions(led_ring_t* ring, uint8_t* positions, pixelColor_t color);

void running_ring_control_init(void)
{
    for(uint8_t i = 0; i < sizeof(directions); i++)
    {
        directions[i] = i % 2 == 0 ? -1 : 1;
    }
}

bool running_ring_control_step(led_ring_t* ring, pixelColor_t color, bool new_mode)
{
    TickType_t current_tick = xTaskGetTickCount();
    if(current_tick >= next_update)
    {
        next_update = current_tick + 100;
        update_positions(ring, positions, color);
        return true;
    }
    return false;
}

void update_positions(led_ring_t* ring, uint8_t* positions, pixelColor_t color)
{
    memset(ring->strand->pixels, 0x00, sizeof(*ring->strand->pixels) * led_ring_get_size(ring));
    for(uint8_t i = 0; i < ring->rings; i++)
    {
        if(ring->ring_size[i] == 0)
        {
            continue;
        }
        
        positions[i] += 1;
        positions[i] %= ring->ring_size[i];
        led_ring_set_pixel(ring, i, positions[i], color);
    }
}