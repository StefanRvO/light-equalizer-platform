#pragma once

#include "esp32_digital_led_lib.h"
#include "libs/led_ring/led_ring.h"
#include "modules/c_link.h"


C_LINK void running_ring_control_init(void);
C_LINK bool running_ring_control_step(led_ring_t* ring, pixelColor_t color, bool new_mode);