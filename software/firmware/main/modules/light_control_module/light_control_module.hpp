#pragma once
#include "modules/protocol_module/protocol_module.h"
#include "modules/light_management_module/light_management_module.h"
#include "esp32_digital_led_lib.h"
#include "libs/led_ring/led_ring.h"
#include "modules/c_link.h"
#include "libs/led_panel/led_panel.h"

int print(int i, double d);

typedef enum
{
    COLORORDER_RGB,
    COLORORDER_RBG,
    COLORORDER_GBR,
    COLORORDER_GRB,
    COLORORDER_BGR,
    COLORORDER_BRG,
} color_order_t;

typedef enum
{
    CHANNELORDER_LR,
    CHANNELORDER_RL,
} channelorder_t;

C_LINK void light_control_module_init(led_panel_t* panel);
C_LINK void light_control_module_mode_recv(light_mesh_message_t *msg);
C_LINK void light_control_module_set_global_light_mode(light_mode_type_t _light_mode, float strength, pixelColor_t color);
C_LINK bool light_control_step(strand_t* strand, led_panel_t* panel, led_ring_t* ring);

C_LINK uint16_t light_control_module_get_led_length(void);
C_LINK uint16_t light_control_module_get_led_len_right(void);
C_LINK light_mode_type_t light_control_module_get_local_mode(void);
C_LINK bool light_control_module_is_mode_global(void);
C_LINK void light_control_set_local_mode(light_mode_type_t _mode);
C_LINK void light_control_set_is_global_mode(bool is_global);
C_LINK channelorder_t led_module_get_channel_order(void);
C_LINK void light_control_force_next_update(void);
C_LINK float light_control_module_get_strength(void);