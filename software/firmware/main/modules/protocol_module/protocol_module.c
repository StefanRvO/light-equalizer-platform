#include "protocol_module.h"
#include "esp_err.h"
#include "esp_log.h"

__attribute__((unused)) static const char *TAG = "PROTOCOL_MODULE";

int protocol_module_esp_mesh_to_light_mesh(light_mesh_message_t *light_mesh_msg,
                      mesh_addr_t *src_addr, uint16_t payload_len,
                      uint8_t *payload)
{
    for(uint8_t i = 0; i < 6; i++)
    {
        light_mesh_msg->src_id[i] = src_addr->addr[i];
    }
    light_mesh_msg->type = payload[0];
    light_mesh_msg->payload_len = payload_len;
    light_mesh_msg->payload = malloc(light_mesh_msg->payload_len);
    for(size_t i = 1; i < payload_len; i++)
    {
        light_mesh_msg->payload[i -1] = payload[i];
    }
    return 0;
}

int protocol_module_light_mesh_to_esp_mesh(light_mesh_message_t *light_mesh_msg, mesh_addr_t *dest_addr,
                                          uint16_t *payload_len, uint8_t *payload)
{
    for(uint8_t i = 0; i < 6; i++)
    {
        dest_addr->addr[i] = light_mesh_msg->dest_id[i];
    }
    *payload_len = light_mesh_msg->payload_len + 1;
    payload[0] = light_mesh_msg->type;
    for(size_t i = 0; i < *payload_len; i++)
    {
        payload[i + 1] = light_mesh_msg->payload[i];
    }
    return 0;
}

