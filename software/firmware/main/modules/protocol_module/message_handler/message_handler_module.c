#include "esp_err.h"
#include "esp_log.h"
#include "esp_mesh.h"
#include "esp_system.h"

#include "modules/protocol_module/protocol_module.h"
#include "modules/config_reader_module/config_reader_module.h"
#include "modules/light_control_module/light_control_module.hpp"
#include "modules/light_control_module/strobe_light_control_module/strobe_light_control_module.h"
#include "modules/light_control_module/glitter_light_control_module/glitter_light_control_module.h"
#include "modules/light_control_module/frequency_control_module/frequency_control_module.h"
#include "modules/mesh_module/node_info_module/node_info_module.h"
#include "modules/filesystem_sync_module/filesystem_sync_module.h"
#include "modules/firmware_sync_module/firmware_sync_module.h"
#include "modules/light_management_module/pulse_light_management_module/pulse_light_management_module.h"
#include "modules/onboard_led_strength_module/onboard_led_strength_module.h"
#include "modules/light_management_module/gol_management_module/gol_management_module.h"
#include "modules/light_management_module/running_sections_management_module/running_sections_management_module.h"
#include "modules/light_management_module/rainbow_management_module/rainbow_management_module.h"
#include "modules/light_management_module/frequency_settings_management_module/frequency_settings_management_module.h"
#include "modules/light_management_module/shooting_management_module/shooting_management_module.hpp"
__attribute__((unused)) static const char *TAG = "MESSAGE HANDLER";

static int handle_message_root(light_mesh_message_t *msg);
static int handle_message_node(light_mesh_message_t *msg);
static void handle_message(light_mesh_message_t *msg);

static uint8_t own_mac[6];

void message_handle_module_init(void)
{
    esp_efuse_mac_get_default(own_mac);
    ESP_LOGI(TAG,"OWN MAC: " MACSTR, MAC2STR(own_mac));
}

void message_handler_module_handle_message(light_mesh_message_t *msg)
{
    handle_message(msg);
    free(msg->payload);
}

void handle_message(light_mesh_message_t *msg)
{
    switch(msg->type)
    {
        case NODE_INFO_SET_MESSAGE:
            node_info_module_handle_set_info_message(msg);
            return;
        default:
            break;
    }

    if(memcmp(own_mac, msg->src_id, sizeof(own_mac)) == 0)
    {
        return;
    }

    if (config_reader_is_root()) 
    {
        if(handle_message_root(msg) == 0)
        {
            return;
        }
    } 
    else 
    {
        if(handle_message_node(msg) == 0)
        {
            return;
        }
    }
    ESP_LOGW(TAG, "UNHANDLED MESSAGE %u received", msg->type);
    return;
}

int handle_message_node(light_mesh_message_t *msg)
{
    switch(msg->type)
    {
        case LIGHT_MODE_MESSAGE:
            light_control_module_mode_recv(msg);
            return 0;
        case STROBE_LIGHT_TIMING_SPEC_MESSAGE:
            strobe_light_spec_recv(msg);
            return 0;
        case GLITTER_LIGHT_DATA_MESSAGE:
            glitter_light_spec_recv(msg);
            return 0;
        case FREQUENCY_INFO_MESSAGE:
            frequency_control_module_recv(msg);
            return 0;
        case FS_DATA_MESSAGE:
            filesystem_sync_module_handle_data(msg);
            return 0;
        case FW_DATA_MESSAGE:
            firmware_sync_module_handle_data(msg);
            return 0;
        case PULSE_SETTINGS_MESSAGE:
            pulse_light_management_module_handle_message(msg);
            return 0;
        case ONBOARD_LED_STRENGTH_MESSAGE:
            onboard_led_strength_handle_message(msg);
            return 0;
        case GAME_OF_LIFE_SETTINGS_MESSAGE:
            gol_management_module_handle_message(msg);
            return 0;
        case RUNNING_SECTIONS_MESSAGE:
            running_sections_management_module_handle_message(msg);
            return 0;
        case RAINBOW_MESSAGE:
            rainbow_management_module_handle_message(msg);
            return 0;
        case FREQUENCY_SETTINGS_MESSAGE:
            frequency_settings_management_handle_message(msg);
            return 0;
        case SHOOTING_MESSAGE:
            shooting_management_module_handle_message(msg);
            return 0;
        default:
            return 1;
    }
}

int handle_message_root(light_mesh_message_t *msg)
{
    switch(msg->type)
    {
        case NODE_INFO_MESSAGE:
            node_info_module_handle_info_message(msg);
            return 0;
        case FS_SYNC_PROGRESS_MESSAGE:
            filesystem_sync_module_handle_progress(msg);
            return 0;
        case FW_SYNC_PROGRESS_MESSAGE:
            firmware_sync_module_handle_progress(msg);
            return 0;
        default:
            return 1;
    }
}