#pragma once
#include "modules/protocol_module/protocol_module.h"
void message_handler_module_handle_messages(void);
void message_handler_module_handle_message(light_mesh_message_t *msg);
void message_handle_module_init(void);