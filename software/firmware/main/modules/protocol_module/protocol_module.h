#pragma once 
#include "modules/light_management_module/light_management_module.h"
#include "modules/light_management_module/strobelight_management_module/strobelight_management_module.h"
#include "esp_mesh.h"

#define MAX_CHUNKS 80


typedef struct {
    struct timeval cur_time;
    uint8_t seq_count;
} time_sync_message_t;

typedef enum {
    TIME_SYNC_MESSAGE,
    LIGHT_MODE_MESSAGE,
    STROBE_LIGHT_TIMING_SPEC_MESSAGE,
    GLITTER_LIGHT_DATA_MESSAGE,
    NODE_INFO_MESSAGE,
    NODE_INFO_SET_MESSAGE,
    FREQUENCY_INFO_MESSAGE,
    FS_SYNC_PROGRESS_MESSAGE,
    FS_DATA_MESSAGE,
    FW_SYNC_PROGRESS_MESSAGE,
    FW_DATA_MESSAGE,
    PULSE_SETTINGS_MESSAGE,
    ONBOARD_LED_STRENGTH_MESSAGE,
    GAME_OF_LIFE_SETTINGS_MESSAGE,
    RUNNING_SECTIONS_MESSAGE,
    FREQUENCY_SETTINGS_MESSAGE,
    RAINBOW_MESSAGE,
    SHOOTING_MESSAGE,
} light_mesh_message_type_t;

typedef struct __attribute__((packed)) {
    uint8_t dest_id[6];
    uint8_t src_id[6];
    uint8_t type;
    uint16_t payload_len;
    uint8_t *payload;
} light_mesh_message_t;

typedef struct __attribute__((packed))
{
    uint8_t global_mode;
    float strength;
    pixelColor_t color;
} light_mode_message_t;

typedef struct __attribute__((packed)) {
    uint32_t start;
    uint32_t end;
} sync_chunk_t;

typedef struct __attribute__((packed)) {
    uint8_t sha[32];
    uint32_t offset;
    uint32_t size;
    uint8_t data[1024];
} partition_data_message_t;

typedef struct __attribute__((packed)) {
    uint8_t sha[32];
    uint8_t chunks;
    sync_chunk_t missing_chunks[MAX_CHUNKS];
} partition_sync_progress_message_t;


typedef struct __attribute__((packed)) {
    char sha[32];
    uint32_t offset;
} fw_sync_progress_message_t;

typedef struct __attribute__((packed)) {
    uint32_t pulse_time_ms;
    uint8_t single_shot;
    int8_t direction;
} pulse_light_message_t;

typedef struct __attribute__((packed)) {
    uint32_t update_rate_ms;
    uint32_t section_count;
    uint32_t section_size;
} running_sections_message_t;

typedef struct __attribute__((packed)) {
    float exponent;
    float power;
    float y_cross;

    float scaling_log_base;
    float scaling_divider;

    float peak_decay_rate;
    uint8_t show_current;
    uint8_t show_peaks;
} frequency_settings_message_t;

typedef struct __attribute__((packed))
{
    strobe_light_timing_spec_t timing_spec;
} strobe_light_timing_spec_message_t;

int protocol_module_esp_mesh_to_light_mesh(light_mesh_message_t *light_mesh_msg,
                      mesh_addr_t *src_addr, uint16_t payload_len,
                      uint8_t *payload);

int protocol_module_light_mesh_to_esp_mesh(light_mesh_message_t *light_mesh_msg, mesh_addr_t *dest_addr,
                                          uint16_t *payload_len, uint8_t *payload);