#include "debug_led_module.hpp"
#include "esp_log.h"
#include <NeoPixelBus.h>


__attribute__((unused)) static const char *TAG = "DEBUG_LED";

static NeoPixelBus<NeoGrbFeature, NeoEsp32Rmt1Ws2812xMethod> strip(1, 15);

void debug_led_module_init(void)
{
    strip.Begin();
    strip.Show();
    ESP_LOGI(TAG, "INIT");

}
void debug_led_module_set_color(const RgbColor& color)
{   
    strip.SetPixelColor(0, color);
    strip.Show();
}

void debug_led_module_set_red(void)
{
    
    debug_led_module_set_color(RgbColor(255, 0, 0));
}

void debug_led_module_set_off(void)
{
    debug_led_module_set_color(RgbColor(0, 0, 0));
}