#pragma once
#include <NeoPixelBus.h>

#include "esp32_digital_led_lib.h"

void debug_led_module_init(void);
void debug_led_module_set_color(const RgbColor &color);
void debug_led_module_set_red(void);
void debug_led_module_set_off(void);