#pragma once
#include "esp32_digital_led_lib.h"
#include "libs/led_ring/led_ring.h"
#include "modules/c_link.h"
#include "modules/light_control_module/light_control_module.hpp"

C_LINK void led_strip_module_init(void);
C_LINK void led_strip_module_step(void);
C_LINK void led_module_get_led_len(uint16_t *length);

C_LINK void led_module_set_panel_size(uint8_t height, uint8_t width);

C_LINK void led_module_get_panel_size(uint8_t *height, uint8_t *width);

C_LINK void led_module_set_ring(led_ring_t* _ring, bool force);
C_LINK void led_module_set_channel_order(channelorder_t order);
C_LINK void led_module_get_ring(led_ring_t* _ring);
C_LINK void led_module_set_color_order(color_order_t _order);
C_LINK void led_module_set_led_len(uint16_t length);

C_LINK void led_module_get_color_order(color_order_t* _order);
