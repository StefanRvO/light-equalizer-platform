#include "led_strip_module.hpp"
#include "esp32_digital_led_lib.h"
#include <string.h>
#include <driver/rmt.h>
#include "modules/config_reader_module/config_reader_module.h"
#include "esp_err.h"
#include "esp_log.h"
#include <NeoPixelBus.h>
#include "modules/board_revision_module/board_revision_module.h"

#define LED_PIN 27

static uint16_t   led_length;
static led_panel_t panel;
static led_ring_t  ring;
static color_order_t color_order = COLORORDER_RGB;
static channelorder_t channel_order = CHANNELORDER_LR;
static channelorder_t channel_order_cur;

#if defined(CONFIG_IDF_TARGET_ESP32) || defined(CONFIG_IDF_TARGET_ESP32S3)

    #define NeoPixelMethod NeoEsp32I2s1Ws2812xMethod
#else
    #define NeoPixelMethod NeoEsp32Rmt0Ws2812xMethod
#endif

static NeoPixelBus<NeoRgbFeature, NeoPixelMethod> strip(501, LED_PIN);
static strand_t strand;
static pixelColor_t pixels[500];
static pixelColor_t state_buf[500];

__attribute__((unused)) static const char *TAG = "LED_STRIP";


static void adjust_strength(strand_t *strand, NeoPixelBus<NeoRgbFeature, NeoPixelMethod>* strip, color_order_t order);
static void update_lengths(bool force);
static void update_order(void);

void led_strip_module_init(void)
{
    memset(&ring, 0x00, sizeof(ring));  

    led_length = config_reader_get_uint32("led_length",  300);
    panel.height  = config_reader_get_uint32("lft_pnl_h", 1) ?: 1;
    panel.width   = config_reader_get_uint32("lft_pnl_w", 1) ?: 1;
    channel_order      = static_cast<channelorder_t>(config_reader_get_uint32("channel_order", CHANNELORDER_LR));

    config_reader_get_blob("ring", &ring, &ring, sizeof(ring));
    if(ring.rings > MAX_RINGS) ring.rings = 1;

    color_order     = static_cast<color_order_t>(config_reader_get_uint32("color_order", COLORORDER_RGB));

    ring.strand  = &strand;
    panel.strand = &strand;

    strand.numPixels = led_length;
    strand.pixels = pixels;
    strand.state_buf = state_buf;
    strip.Begin();
    strip.Show();

    update_lengths(true);

    led_module_set_ring(&ring, true);
    light_control_module_init(&panel);
}

void led_module_set_ring(led_ring_t* _ring, bool force)
{
    if(force || (memcmp(_ring, &ring, sizeof(ring) - 4) == 0))
    {
        return;
    }
    if(force || _ring->rings != ring.rings)
    {
        memset(&_ring->ring_size, 0x00, sizeof(_ring->ring_size));
    }
    if(_ring->rings > MAX_RINGS || led_ring_get_size(_ring) > led_length)
    {
        return;
    }

    memcpy(&ring, _ring, sizeof(ring) - 4);

    config_reader_set_blob("ring", &ring, sizeof(ring));
}

void led_module_get_ring(led_ring_t *_ring)
{
    memcpy(_ring, &ring, sizeof(*_ring));
}

void led_module_get_panel_size(uint8_t *height, uint8_t *width)
{
    *height  = panel.height;
    *width   = panel.width;
}

void led_module_get_led_len(uint16_t *length)
{
    *length = led_length;
}

void led_module_set_color_order(color_order_t _color_order)
{
    if(color_order != _color_order)
    {

        color_order = _color_order;
        config_reader_set_uint32("color_order", color_order);
    }
}

void led_module_get_color_order(color_order_t* _order)
{
    *_order  = color_order;
}

void led_module_set_panel_size(uint8_t height, uint8_t width)
{
  if(height  == panel.height && width   == panel.width)
  {
      return;
  }

  if(height * (uint32_t)width  > led_length || height  * (uint32_t)width <= 0)
  {
      return;
  }
  
  panel.height  = height;
  panel.width   = width;

  config_reader_set_uint32("lft_pnl_h", panel.height);
  config_reader_set_uint32("lft_pnl_w", panel.width);
}

void led_module_set_channel_order(channelorder_t order)
{
    if(order != channel_order)
    {
        channel_order = order;
        config_reader_set_uint32("channel_order", order);
    }
}

channelorder_t led_module_get_channel_order(void)
{
    return channel_order;
}

void update_lengths(bool force)
{
    if(led_length != strand.numPixels || force)
    {
        ESP_LOGI(TAG, "LED lenght changed, updating strands");
        strand.numPixels = led_length;
        light_control_force_next_update();
    }
}

void update_order(void)
{
    if(channel_order_cur != channel_order)
    {
        channel_order_cur = channel_order;
        update_lengths(true);
    }
}

void refresh_leds(void)
{
    adjust_strength(&strand, &strip, color_order);
    if(strip.CanShow())
    {
        strip.Show();
    }
}

void led_strip_module_step(void)
{
    update_order();
    update_lengths(false);

    if(light_control_step(&strand, &panel, &ring))
    {
        refresh_leds();
    }
}

void led_module_set_led_len(uint16_t length)
{
  if(length == led_length || length >= 500 || length == 0) 
  {
      return;
  }

  if(panel.height * panel.width > length)
  {
      return;
  }

  if(led_ring_get_size(&ring) > length)
  {
      return;
  }

  led_length  = length;
  config_reader_set_uint32("led_length",  led_length);
}


void adjust_strength(strand_t *strand, NeoPixelBus<NeoRgbFeature, NeoPixelMethod>* strip, color_order_t order)
{
    uint8_t index_offset = 0;
    switch(board_revision_module_get_revision())
    {
        case BOARD_REVISION_V3_I2S:
            index_offset = 1;
            break;
        case BOARD_REVISION_V1_I2S:
        case BOARD_REVISION_V2_I2S:
        case BOARD_REVISION_V1_SLAVE:
        default:
            index_offset = 0;
            break;
    }
    for(uint32_t i = 0; i < strip->PixelCount(); i++)
    {
        if(i >= index_offset && i < strand->numPixels + index_offset)
        {
            uint8_t r = strand->pixels[i - index_offset].r * light_control_module_get_strength();
            uint8_t g = strand->pixels[i - index_offset].g * light_control_module_get_strength();
            uint8_t b = strand->pixels[i - index_offset].b * light_control_module_get_strength();
            switch(order)
            {
                case COLORORDER_RGB:
                    strip->SetPixelColor(i, RgbColor(r, g, b));
                    break;
                case COLORORDER_RBG:
                    strip->SetPixelColor(i, RgbColor(r, g, b));
                    break;
                case COLORORDER_GBR:
                    strip->SetPixelColor(i, RgbColor(g, b, r));
                    break;
                case COLORORDER_GRB:
                    strip->SetPixelColor(i, RgbColor(g, r, b));
                    break;
                case COLORORDER_BGR:
                    strip->SetPixelColor(i, RgbColor(b, g, r));
                    break;
                case COLORORDER_BRG:
                    strip->SetPixelColor(i, RgbColor(b, g, r));
                    break;
            }
        }
        else
        {
            strip->SetPixelColor(i, RgbColor(0, 0, 0));
        }
    }
}