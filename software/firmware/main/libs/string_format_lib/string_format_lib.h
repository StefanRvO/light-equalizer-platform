#include <stdint.h>
#include <stddef.h>
#include "modules/c_link.h"

C_LINK void string_format_bin_to_hex(char* dst, const uint8_t* buf, size_t len);