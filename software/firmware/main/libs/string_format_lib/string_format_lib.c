#include "string_format_lib.h"
#include <string.h>
#include <stdio.h>

void string_format_bin_to_hex(char* dst, const uint8_t* buf, size_t len)
{
    for(size_t i = 0; i < len; i++)
    {
        sprintf(dst, "%02x", buf[i]);
        dst += 2;
    }
}