#pragma once

#include "esp32_digital_led_lib.h"
#include <stdint.h>
#include "modules/c_link.h"

C_LINK pixelColor_t scale_color(const pixelColor_t* start, const pixelColor_t* end, float part);
C_LINK pixelColor_t add_color_loop(const pixelColor_t color1, const pixelColor_t color2);
C_LINK pixelColor_t scale_color_arr(const pixelColor_t* color_array, uint32_t color_cnt, float part);
C_LINK void fill_string(strand_t* string, const pixelColor_t color);