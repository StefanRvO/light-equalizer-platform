#include "color_utils.h"

static uint8_t scaled_color(uint8_t start, uint8_t end, float part);

pixelColor_t scale_color(const pixelColor_t* start, const pixelColor_t* end, float part)
{
    if(part > 1) part = 1;
    if(part < 0) part = 0;

    pixelColor_t color;

    color.r = scaled_color(start->r, end->r, part);
    color.g = scaled_color(start->g, end->g, part);
    color.b = scaled_color(start->b, end->b, part);
    return color;
}
pixelColor_t scale_color_arr(const pixelColor_t* color_array, uint32_t color_cnt, float part)
{
    if(part < 0) part = 0;
    if(part > 1) part = 1;

    if(color_cnt <= 1)
    {
        return *color_array;
    }
    part *= color_cnt - 1;
    uint32_t part1 = (uint32_t)part;
    uint32_t part2 = ((uint32_t)part) + 1;
    part-= part1;
    while(part2 >= color_cnt)
    {
        part2--;
        part1--;
    }
    return scale_color(&color_array[part1], &color_array[part2], part);
}

uint8_t scaled_color(uint8_t start, uint8_t end, float part)
{
    uint16_t color = start;
    int16_t diff = (int16_t)end - (int16_t)start;
    color += diff * part;
    return color;
}

pixelColor_t add_color_loop(const pixelColor_t color1, const pixelColor_t color2)
{
    pixelColor_t end = color1;
    end.r += color2.r;
    end.g += color2.g;
    end.b += color2.b;
    return end;
}

void fill_string(strand_t* string, const pixelColor_t color)
{
    for(uint32_t i = 0; i < string->numPixels; i++)
    {
        string->pixels[i] = color;
    }
}