#pragma once
#include <stdint.h>

//Remember to update this if SOUND_BUFFER_SIZE is changed!

extern const float hann_window_coeffs[1024];

float hann_window(float a_0, uint32_t n, uint32_t N);