#pragma once

#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif
    uint16_t float_to_half_float(float f);
    float half_float_to_float(uint16_t hf);
    void half_float_to_float_arr(const uint16_t* hf, float* f, size_t cnt);
    void float_to_half_float_arr(uint16_t* hf, const float* f, size_t cnt);
#ifdef __cplusplus
}
#endif