#pragma once
#include <stdint.h>
#include <stdbool.h>
#include "esp32_digital_led_lib.h"

typedef struct
{
    uint8_t height;
    uint8_t width;
    strand_t* strand;
    bool mirror_x;
    bool mirror_y;
} led_panel_t;

uint32_t led_panel_get_size(led_panel_t *panel);
uint32_t led_panel_get_pixel_index(led_panel_t *panel, uint32_t x, uint32_t y);
pixelColor_t led_panel_get_pixel(led_panel_t *panel, uint32_t x, uint32_t y);
void led_panel_set_pixel(led_panel_t *panel, uint32_t x, uint32_t y, const pixelColor_t *color);
void led_panel_fill_col(led_panel_t *panel, uint32_t col, const pixelColor_t* color, float part);
void led_panel_fill_row(led_panel_t *panel, uint32_t row, const pixelColor_t* color, float part);
void led_panel_shift_col(led_panel_t *panel, uint32_t col);
void led_panel_shift_row(led_panel_t *panel, uint32_t row);

#ifndef __cplusplus
void led_panel_apply_kernel(led_panel_t* panel, int8_t m, int8_t n, const float kernel[m][n]);
#endif