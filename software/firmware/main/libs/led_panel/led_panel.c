#include "led_panel.h"

#include <string.h>
#include <stdio.h>

static void led_panel_apply_kernel_pixel(led_panel_t* panel, uint32_t x, uint32_t y, int8_t m, int8_t n, const float kernel[m][n]);

uint32_t led_panel_get_size(led_panel_t *panel)
{
    return (uint32_t)panel->height * (uint32_t)panel->width;
}

uint32_t led_panel_get_pixel_index(led_panel_t *panel, uint32_t x, uint32_t y)
{
    if(x > panel->width || y > panel->height)
    {
        return 0;
    }
    if(y % 2 == 0)
    {
        return y * panel->width + x;
    }
    else
    {
        return y * (uint32_t)panel->width + (panel->width - x - 1);
    }
}

void led_panel_set_pixel(led_panel_t *panel, uint32_t x, uint32_t y, const pixelColor_t *color)
{
    if(panel->mirror_x)
    {
        x = panel->width - x - 1;
    }
    if(panel->mirror_y)
    {
        y = panel->height - y - 1;
    }
    panel->strand->pixels[led_panel_get_pixel_index(panel, x, y)] = *color;
}

pixelColor_t led_panel_get_pixel(led_panel_t *panel, uint32_t x, uint32_t y)
{
    return panel->strand->pixels[led_panel_get_pixel_index(panel, x, y)];
}


void led_panel_fill_row(led_panel_t *panel, uint32_t row, const pixelColor_t* color, float part)
{
    if(part > 1) part = 1;
    if(part < 0) part = 0;
    for(uint32_t x = 0; x < panel->width * part; x++)
    {
        led_panel_set_pixel(panel, x, row, color);
    }
}

void led_panel_shift_row(led_panel_t *panel, uint32_t row)
{
    pixelColor_t tmp;
    for(uint32_t x = panel->width - 1; x > 0; x--)
    {
        tmp = led_panel_get_pixel(panel, x, row);
        led_panel_set_pixel(panel, x - 1, row, &tmp);
    }
}

void led_panel_shift_col(led_panel_t *panel, uint32_t col)
{
    pixelColor_t tmp;
    for(uint32_t y = 0; y < panel->height - 1; y++)
    {
        tmp = led_panel_get_pixel(panel, col, y + 1);
        led_panel_set_pixel(panel, col, y, &tmp);
    }
}

void led_panel_fill_col(led_panel_t *panel, uint32_t col, const pixelColor_t* color, float part)
{
    if(part > 1) part = 1;
    if(part < 0) part = 0;
    for(uint32_t y = 0; y < (uint32_t)(panel->height * part); y++)
    {
        led_panel_set_pixel(panel, col, y, color);
    }
}


//update each pixel by computing applying the given 3x3 kernel
void led_panel_apply_kernel(led_panel_t* panel, int8_t m, int8_t n, const float kernel[m][n])
{
    memcpy(panel->strand->state_buf, panel->strand->pixels, sizeof(panel->strand->pixels[0]) * led_panel_get_size(panel));

    for(uint32_t x = 0; x < panel->width; x++)
    {
        for(uint32_t y = 0; y < panel->height; y++)
        {
            led_panel_apply_kernel_pixel(panel, x, y, m, n, kernel);
        }
    }
}

void led_panel_apply_kernel_pixel(led_panel_t* panel, uint32_t x, uint32_t y, int8_t m, int8_t n, const float kernel[m][n])
{
    pixelColor_t color = {.raw32 = 0};
    for(int32_t _x = -m / 2; _x <= m / 2; _x++)
    {
        for(int32_t _y = -n / 2; _y <= m / 2; _y++)
        {
            int32_t coord_x = x + _x;
            int32_t coord_y = y + _y;
            if(coord_x < 0 || coord_y < 0 || coord_x + 1 > panel->width || coord_y + 1 > panel->height)
            {
                continue;
            }
            pixelColor_t coord_color = ((pixelColor_t*)panel->strand->state_buf)[led_panel_get_pixel_index(panel, coord_x, coord_y)];
            color.r += coord_color.r * kernel[_x + m / 2][_y + n / 2];
            color.g += coord_color.g * kernel[_x + m / 2][_y + n / 2];
            color.b += coord_color.b * kernel[_x + m / 2][_y + n / 2];
        }
    }
    led_panel_set_pixel(panel, x, y, &color);
}