#include "game_of_life.h"
#include <stdlib.h>
#include <string.h>
#include "esp_log.h"
#include "esp_system.h"

__attribute__((unused)) static const char *TAG = "GOL";


static void game_of_life_set(game_of_life_t* game, uint32_t x, uint32_t y, bool value);
static void seed_random_point(game_of_life_t* game);

void game_of_life_init(game_of_life_t* game, uint32_t width, uint32_t height, bool random)
{
    game->height = height;
    game->width  = width;
    uint32_t x_size = width / 8;
    while(x_size % 4 != 0 || x_size == 0)
    {
        x_size++;
    }
    game->x_size = x_size;
    if(random)
    {
        esp_fill_random(game->board, height * x_size / 4);
    }
}

void game_of_life_destroy(game_of_life_t* game)
{
}

void game_of_life_tick(game_of_life_t* game)
{
    game_of_life_t tmp;
    game_of_life_init(&tmp, game->width, game->height, false);
    tmp.board = malloc(tmp.height * tmp.x_size);

    memcpy(tmp.board, game->board, game->height * game->x_size);
    uint32_t total_neightbours = 0;
    for(uint32_t x = 0; x < game->width; x++)
    {
        for(uint32_t y = 0; y < game->height; y++)
        {
            uint8_t neighbours = game_of_life_neighbour_cnt(&tmp, x, y);
            total_neightbours += neighbours;
            if(neighbours < 2)
            {
                game_of_life_set(game, x, y, false);
            }
            else if(neighbours == 3)
            {
                game_of_life_set(game, x, y, true);
            }
            else if(neighbours > 3)
            {
                game_of_life_set(game, x, y, false);
            }
        }
    }
    free(tmp.board);
    if(esp_random() % 5 == 0)
    {
        seed_random_point(game);
    }
    if(total_neightbours == 0 && esp_random() % 20 == 0)
    {
        esp_fill_random(game->board, game->height * game->x_size);
    }
}

bool game_of_life_get(game_of_life_t* game, uint32_t x, uint32_t y)
{
    return (game->board[y + x / 32] & (1 << ( x % 32))) > 0;
}

void seed_random_point(game_of_life_t* game)
{
    uint32_t x = esp_random() % game->width;
    uint32_t y = esp_random() % game->height;
    if(game_of_life_neighbour_cnt(game, x, y))
    {
        game_of_life_set(game, x, y, true);
    }
}


void game_of_life_set(game_of_life_t* game, uint32_t x, uint32_t y, bool value)
{
    game->board[y + x / 32] &=  ~(1 << ( x % 32));
    game->board[y + x / 32] |= (value << ( x % 32));
}

uint8_t game_of_life_neighbour_cnt(game_of_life_t* game, uint32_t x, uint32_t y)
{
    uint32_t left  = (x == 0 ? game->width - 1 : x - 1);
    uint32_t right = (x == game->width - 1 ? 0 : x + 1);
    uint32_t down  = (y == 0 ? game->height - 1 : y - 1);
    uint32_t up    = (y == game->height - 1 ? 0 : y + 1);
    uint8_t cnt = 0;
    cnt += game_of_life_get(game, left, up);
    cnt += game_of_life_get(game, x, up);
    cnt += game_of_life_get(game, right, up);
    cnt += game_of_life_get(game, left, down);
    cnt += game_of_life_get(game, x, down);
    cnt += game_of_life_get(game, right, down);
    cnt += game_of_life_get(game, right, y);
    cnt += game_of_life_get(game, left, y);
    return cnt;
}

