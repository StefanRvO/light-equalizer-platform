#pragma once
#include <stdint.h>
#include <stdbool.h>

typedef struct
{
    uint32_t height;
    uint32_t width;
    uint32_t x_size;
    uint32_t* board;
} game_of_life_t;

void game_of_life_init(game_of_life_t* game, uint32_t width, uint32_t height, bool random);
void game_of_life_destroy(game_of_life_t* game);
void game_of_life_tick(game_of_life_t* game);
bool game_of_life_get(game_of_life_t* game, uint32_t x, uint32_t y);
uint8_t game_of_life_neighbour_cnt(game_of_life_t* game, uint32_t x, uint32_t y);