#include "math_lib.h"
#include "math.h"

float logx(float base, float value)
{
    return logf(value) / logf(base);
}