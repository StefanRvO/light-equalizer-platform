#include "math.h"
#include <string.h>
#include "drivers/ak5720/ak5720.h"

#include "mel_scale_lib.h"

float frequency_to_mel(float f)
{
    return 2410 * log10f(1 + f / 625);
}

float mel_to_frequency(float m)
{
    return 625 * (powf(10, m / 2410) - 1);
}

uint32_t mel_to_bin(float m, float bin_size)
{
    return mel_to_frequency(m) / bin_size;
}

void fft_to_mel_bins(float *fft_bins, float* mel_bins, uint32_t fft_size, uint32_t mel_size, float max_freq)
{
    const float fft_bin_size = (SAMPLE_RATE / 2.) / fft_size;
    const float max_mel      = frequency_to_mel(max_freq);
    const float mel_binsize  = max_mel / (mel_size + 1);
    uint32_t mel_bin = 0;
    for(float i = mel_binsize; i < max_mel - 1; i += mel_binsize)
    {
        const uint32_t first_bin = mel_to_bin(i - mel_binsize, fft_bin_size);
        const uint32_t last_bin  = mel_to_bin(i + mel_binsize, fft_bin_size);
        const uint32_t bin_cnt   = last_bin - first_bin + 1;
        const float scale_outer_bins   = 1. / bin_cnt;
        const float scale_center_bin   = 1.;
        float weight_sum = 0;

        for(uint32_t bin = first_bin; bin < first_bin + bin_cnt / 2; bin++)
        {
            const uint32_t bin_rel = bin - first_bin;
            const float part = (float)bin_rel / ((float)bin_cnt / 2.);
            const float scale = (scale_center_bin - scale_outer_bins) * part + scale_outer_bins;
            
            mel_bins[mel_bin] += fft_bins[bin] * scale;
            weight_sum += scale;
        }
        for(uint32_t bin = first_bin + bin_cnt / 2; bin <= last_bin; bin++)
        {
            const uint32_t bin_rel = last_bin - bin;
            const float part = (float)bin_rel / ((float)bin_cnt / 2.);
            const float scale = (scale_center_bin - scale_outer_bins) * part + scale_outer_bins;
            mel_bins[mel_bin] += fft_bins[bin] * scale;
            weight_sum += scale;
        }        
        mel_bins[mel_bin] /= weight_sum;
        mel_bins[mel_bin] *= bin_cnt;
        mel_bin++;
    }
}