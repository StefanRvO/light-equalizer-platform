#include <stdint.h>

float frequency_to_mel(float f);
float mel_to_frequency(float m);
uint32_t mel_to_bin(float m, float bin_size);
void fft_to_mel_bins(float *fft_bins, float* mel_bins, uint32_t fft_size, uint32_t mel_size, float max_freq);