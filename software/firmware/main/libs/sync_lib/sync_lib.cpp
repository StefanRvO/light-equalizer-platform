#include "sync_lib.hpp"
#include "esp_err.h"
#include "esp_log.h"
#include <string.h>
#include <algorithm>

extern "C"
{
    #include "modules/mesh_module/node/mesh_node_module.h"
    #include "modules/mesh_module/root/mesh_root_module.h"
    #include "libs/string_format_lib/string_format_lib.h"
}


SyncedChunks::SyncedChunks(sync_chunk_t* chunks, uint8_t chunk_cnt)
{
    for(uint8_t i = 0; i < chunk_cnt; i++)
    {
        pair<uint32_t, uint32_t> chunk((uint32_t)chunks[i].start, (uint32_t)chunks[i].end);
        this->push_back(chunk);
    }
}

bool SyncedChunks::remove_piece(uint32_t offset, uint32_t size)
{
    for(auto itt = this->begin(); itt != this->end(); itt++)
    {
        auto& chunk = *itt;
        if(offset >= chunk.first && offset <= chunk.second)
        {
            if(offset == chunk.first)
            {
                chunk.first += size;
                if(chunk.first >= chunk.second)
                {
                    this->erase(itt);
                }
            }
            else
            {
                pair<uint32_t, uint32_t> new_chunk(offset + size, chunk.second);
                chunk.second = offset;
                auto new_itt = itt;  
                if(new_chunk.first < new_chunk.second)
                {
                    new_itt++;
                    this->insert(new_itt, new_chunk);
                }
                if(chunk.second <= chunk.first)
                {
                    this->erase(itt);
                    ESP_LOGI("CHUNK", "Removed piece. %d", this->size());
                }
            }
            max_chunks = max((uint32_t)this->size(), max_chunks);
            return true;
        }
    }
    return false;
}

uint32_t SyncedChunks::get_total_size() const
{
    uint32_t sum = 0;
    for(auto itt = this->begin(); itt != this->end(); itt++)
    {
        sum += itt->second - itt->first;
    }
    return sum;
}

uint8_t SyncedChunks::serialize(sync_chunk_t* chunks) const 
{
    uint8_t i = 0;
    auto itt = this->begin();
    for(i = 0, itt = this->begin(); i < MAX_CHUNKS && itt != this->end(); i++, itt++)
    {
        chunks[i].start = itt->first;
        chunks[i].end   = itt->second;
    }
    return i;
}

uint32_t SyncedChunks::get_first_offset() const 
{
    if(this->size())
    {
        return this->begin()->first;
    }
    return 0;
}

void SyncedChunks::reset(uint32_t total_size)
{
    this->clear();
    this->push_back(pair<uint32_t, uint32_t>(0, total_size));
}

void SyncedChunks::splice(const SyncedChunks &chunksToSplice)
{
    this->insert(this->end(), chunksToSplice.begin(), chunksToSplice.end());
    SyncedChunks result;
    auto it = this->begin();
    auto current = *(it)++;
    while (it != this->end()){
       if (current.second >= it->first){
           current.second = std::max(current.second, it->second); 
       } else {
           result.push_back(current);
           current = *(it);
       }
       it++;
    }
    result.push_back(current);
    this->clear();
    this->insert(this->end(), result.begin(), result.end());
    max_chunks = max((uint32_t)this->size(), max_chunks);
}

void SyncedChunks::print_chunks() const
{
    const char* tag = "SyncedChunks";
    ESP_LOGI(tag, "chunks: %d", this->size());
    for(auto itt = this->begin(); itt != this->end(); itt++)
    {
        ESP_LOGI(tag, "%d-%d", itt->first, itt->second);
    }
}

PartitionSyncer::PartitionSyncer(light_mesh_message_type_t sync_message, 
                                light_mesh_message_type_t data_message,
                                const char* tag) :
    tag(tag),
    sync_message(sync_message),
    data_message(data_message)
{
    this->lock = xSemaphoreCreateBinary();
    xSemaphoreGive(this->lock);
}
    
void PartitionSyncer::init()
{
    update_sha256();
}

uint32_t PartitionSyncer::read_chunk(uint8_t* buf, size_t offset, size_t size)
{
    if(size == 0)
    {
        return 0;
    }

    const esp_partition_t* part = get_current_partition();
    uint32_t bytes_remaining = part->size - offset;
    ESP_ERROR_CHECK(esp_partition_read(part, offset, buf, bytes_remaining > size ? size : bytes_remaining));
    return bytes_remaining > size ? size : bytes_remaining;
}

uint32_t PartitionSyncer::write_chunk(uint8_t* buf, size_t offset, size_t size)
{
    if(size == 0)
    {
        return 0;
    }

    if(this->missing_recv_chunks.remove_piece(offset, size))
    {
        const esp_partition_t * part = get_target_partition();
        uint32_t bytes_remaining = part->size - offset;
        uint32_t bytes_to_write = bytes_remaining > size ? size : bytes_remaining;
        ESP_ERROR_CHECK(esp_partition_write(part, offset, buf, bytes_to_write));
        return bytes_to_write;
    }
    return 0;
}

void PartitionSyncer::update_sha256()
{
    const esp_partition_t *part = get_current_partition();
    ESP_ERROR_CHECK(esp_partition_get_sha256(part, sha256));
}

const esp_partition_t* PartitionSyncer::get_current_partition() const
{
    return this->get_target_partition();
}

uint32_t PartitionSyncer::get_target_partition_size()
{
    return get_target_partition()->size;
}

uint32_t PartitionSyncer::get_next_receive_offset() const
{
    return missing_recv_chunks.get_first_offset();
}

uint32_t PartitionSyncer::get_next_send_offset() const
{
    return missing_send_chunks.get_first_offset();
}

void PartitionSyncer::handle_progress(light_mesh_message_t *msg)
{
    partition_sync_progress_message_t* sync_msg = (partition_sync_progress_message_t *)msg->payload;
    xSemaphoreTake(lock, portMAX_DELAY);

    if(memcmp(sync_msg->sha, sha256, sizeof(sync_msg->sha)) != 0)
    {
        if(sync_msg->chunks == 0)
        {
            char local[65];
            char remote[65];
            string_format_bin_to_hex(local,  sha256, sizeof(sha256));
            string_format_bin_to_hex(remote, sync_msg->sha, sizeof(sync_msg->sha));

            this->missing_send_chunks.reset(this->get_target_partition_size());
            ESP_LOGI(tag, "Started sending partition. Local SHA %s, remote SHA %s",
                           local, remote);
        }
        SyncedChunks chunks(sync_msg->missing_chunks, sync_msg->chunks);
        this->missing_send_chunks.splice(chunks);
    }
    xSemaphoreGive(lock);
}

void PartitionSyncer::handle_data(light_mesh_message_t *msg)
{
    partition_data_message_t* data_msg = (partition_data_message_t *)msg->payload;

    if(memcmp(data_msg->sha, sha256, sizeof(data_msg->sha) != 0))
    {
        if(missing_recv_chunks.size() == 0)
        {
            this->erase_partition();
            missing_recv_chunks.reset(get_target_partition_size());
        }
        xSemaphoreTake(lock, portMAX_DELAY);
        this->write_chunk(data_msg->data, data_msg->offset, data_msg->size);
        if(missing_recv_chunks.size() == 0)
        {
            char local[65];
            char remote[65];
            string_format_bin_to_hex(local,  sha256, sizeof(sha256));
            string_format_bin_to_hex(remote, data_msg->sha, sizeof(data_msg->sha));

            ESP_LOGI(tag, "done receiving partition. Local SHA %s, remote SHA %s",
                           local, remote);
            this->handle_done_receiving();
            this->update_sha256();
            string_format_bin_to_hex(local,  sha256, sizeof(sha256));
            ESP_LOGI(tag, "Updated local SHA %s", local);

        }
        xSemaphoreGive(lock);
    }
}

void PartitionSyncer::send_data_step()
{
    TickType_t current_tick = xTaskGetTickCount();
    const uint32_t period = 30;

    if(missing_send_chunks.size() && current_tick - last_data_tick > period)
    {
       last_data_tick = current_tick;
       xSemaphoreTake(lock, portMAX_DELAY);
       partition_data_message_t msg;
       memcpy(msg.sha, sha256, sizeof(msg.sha));
       msg.size = read_chunk(msg.data, get_next_send_offset(), sizeof(msg.data));
       msg.offset = get_next_send_offset();
       send_to_all(this->data_message, (void*)&msg, sizeof(msg));
       this->missing_send_chunks.remove_piece(msg.offset, msg.size);
       if(msg.size == 0)
       {
           ESP_LOGI(tag, "Done Sending firmware");
       }
       xSemaphoreGive(lock);
    }
}

void PartitionSyncer::send_progress_step()
{
    TickType_t current_tick = xTaskGetTickCount();
    uint32_t period = 1000;
    if(missing_recv_chunks.size() > 0)
    {
        period = 200;
    }
    if(current_tick - last_progress_tick > period)
    {
        // char local[65];
        // string_format_bin_to_hex(local,  sha256, sizeof(sha256));
        // ESP_LOGI(tag, "%s", local);
        last_progress_tick = current_tick;
        xSemaphoreTake(lock, portMAX_DELAY);
        partition_sync_progress_message_t msg;
        memcpy(msg.sha, sha256, sizeof(msg.sha));
        msg.chunks = missing_recv_chunks.serialize(msg.missing_chunks);
        send_to_root(this->sync_message, (void*)&msg, sizeof(msg));
        xSemaphoreGive(lock);
    }
}

void PartitionSyncer::erase_partition()
{
    const esp_partition_t * part = get_target_partition();
    ESP_ERROR_CHECK(esp_partition_erase_range(part, 0, part->size));
    missing_recv_chunks.reset(get_target_partition_size());
}