#pragma once
#include <utility>
#include <list>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "modules/protocol_module/protocol_module.h"
#include "esp_ota_ops.h"

using namespace std;

class SyncedChunks : public list<pair<uint32_t, uint32_t> >
{
    public:
        SyncedChunks() : max_chunks(0) {};
        SyncedChunks(sync_chunk_t*chunks, uint8_t chunk_cnt);
        void splice(const SyncedChunks &chunksToSplice);
        uint8_t serialize(sync_chunk_t* chunks) const;
        uint32_t get_first_offset() const;
        uint32_t get_total_size() const;
        uint32_t max_chunks;
        void reset(uint32_t total_size);
        bool remove_piece(uint32_t offset, uint32_t size);
        virtual void print_chunks() const;

};

class PartitionSyncer
{
    public:
        PartitionSyncer(light_mesh_message_type_t sync_message, 
                        light_mesh_message_type_t data_message,
                        const char* tag);
        virtual void init();
        virtual void handle_progress(light_mesh_message_t* msg);
        virtual void handle_data(light_mesh_message_t* msg);
        virtual void send_data_step();
        virtual void send_progress_step();
        virtual uint32_t get_next_receive_offset() const;
        virtual uint32_t get_next_send_offset() const;
        virtual uint32_t read_chunk(uint8_t* buf, size_t offset, size_t size);
        virtual uint32_t write_chunk(uint8_t* buf, size_t offset, size_t size);
        virtual void erase_partition();
        virtual uint32_t get_target_partition_size();

        SyncedChunks missing_recv_chunks;
        SyncedChunks missing_send_chunks;
        uint8_t sha256[32];

    protected:
        const char* tag;
        light_mesh_message_type_t sync_message;
        light_mesh_message_type_t data_message;
        TickType_t last_progress_tick;
        TickType_t last_data_tick;
        SemaphoreHandle_t lock;
        virtual const esp_partition_t* get_target_partition() const = 0;
        virtual const esp_partition_t* get_current_partition() const;
        virtual void handle_done_receiving() = 0;
        virtual void update_sha256();
};
