#pragma once

#include <stdint.h>
#include "modules/c_link.h"

typedef struct
{
    float power; //End value
    float exponent;
    float y_cross;
} freq_power_func_t;

typedef struct
{
    float log_base;
    float multiplier;
} freq_scaling_t;

C_LINK int32_t freq_control_lib_compute_bin(float frequency);
C_LINK void freq_control_lib_compose_bins(float *composed, float *fft_bins, uint32_t bin_cnt);

C_LINK float freq_control_lib_scale_power(float value, freq_scaling_t* scaling);
C_LINK float freq_control_lib_compute_bin_frequency(uint32_t bin);
