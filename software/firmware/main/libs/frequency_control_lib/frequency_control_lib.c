#include "frequency_control_lib.h"
#include "drivers/ak5720/ak5720.h"
#include "esp_log.h"

#include "math.h"
#include "libs/math_lib/math_lib.h"
#include "modules/light_management_module/frequency_management_module/frequency_management_module.h"

__attribute__((unused)) static const char *TAG = "FREQ_CONTROL_LIB";

void freq_control_lib_compose_bins(float *composed, float *fft_bins, uint32_t bin_cnt)
{
    int32_t bin_start = -1;
    for(uint8_t bin = 0; bin < bin_cnt; bin++)
    {
        composed[bin] = 0;
        float part = ((float)bin + 1) / (float)bin_cnt;
        int32_t end_bin = part * BINS_TO_SEND;

        if (end_bin <= bin_start)
        {
            end_bin = bin_start + 1;
        }
        for(int32_t i = bin_start + 1 ; i <= end_bin; i++)
        {
            if(fft_bins[i] > composed[bin])
            {
                composed[bin] = fft_bins[i];
            }
        }
        bin_start = end_bin;
    }
}

float freq_control_lib_scale_power(float value, freq_scaling_t* scaling)
{
    return logx(scaling->log_base, value) * scaling->multiplier;
}