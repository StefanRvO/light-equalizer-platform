#pragma once

//Implement various statistical methods, e.g.:
//Welford online algorithm for mean and stddev
//Modification for Welfords algorithm for weighted samples

typedef struct
{
    double mean;
    double variance;
    float sample_weight;
    float weight;
    float max_weight;
} weighted_welford_data_t;

typedef struct
{
    float avg;
    float avg_prev;
    float decay_rate;
} running_avg_t;

#ifdef __cplusplus
extern "C" {
#endif
    void statistics_lib_reset_weighted_welford(weighted_welford_data_t *data, float sample_weight, float max_weight);
    void statistics_lib_update_weighted_welford(weighted_welford_data_t *data, float sample);
    double statistics_lib_get_weighted_welford_stddev(weighted_welford_data_t *data);
    float statistics_lib_update_running_avg(running_avg_t* avg, float value);
#ifdef __cplusplus
}
#endif