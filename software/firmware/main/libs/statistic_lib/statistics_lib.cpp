#include <cmath>
#include "statistics_lib.h"
#include "esp_log.h"
extern "C" void statistics_lib_reset_weighted_welford(weighted_welford_data_t *data, float sample_weight, float max_weight)
{
    data->mean                   = 0;
    data->variance               = 0;
    data->weight                 = 0;
    data->sample_weight          = sample_weight;
    data->max_weight             = max_weight;
}

extern "C" void statistics_lib_update_weighted_welford(weighted_welford_data_t *data, float sample)
{
    if(data->weight <= 0)
    {
        data->mean     = sample;
        data->variance = 0;
        data->weight = data->sample_weight;
        return;
    }
    //Update mean
    double old_mean = data->mean;
    double delta    = sample - data->mean;
    data->mean     += (delta * data->sample_weight) / data->weight;
    //Update variance
    data->variance  = (data->variance * data->weight + ((sample - old_mean) * (sample - data->mean) * data->sample_weight)) / (data->weight + data->sample_weight);
    data->weight    = std::fmin(data->weight + data->sample_weight, data->max_weight);
}

extern "C" double statistics_lib_get_weighted_welford_stddev(weighted_welford_data_t *data)
{
    return std::sqrt(data->variance);
}




extern "C" float statistics_lib_update_running_avg(running_avg_t* avg, float value)
{
    avg->avg_prev = avg->avg;   
    avg->avg = (1 - avg->decay_rate) * avg->avg + value * avg->decay_rate;
    return avg->avg;
}