#include "fire_lib.h"
void fire_lib_step(led_panel_t* panel, const pixelColor_t* bottom, int8_t m, int8_t n, const float kernel[m][n])
{
    led_panel_apply_kernel(panel, m, n, kernel);

    for(uint32_t x = 0; x < panel->width; x++)
    {
        pixelColor_t c = {.raw32 = 0};
        for(int8_t i = -1; i <= 1; i++)
        {
            int8_t _x = x + i;
            if(_x >= 0 && _x < panel->width)
            {
                c.r += bottom[_x].r * 0.300;
                c.g += bottom[_x].g * 0.300;
                c.b += bottom[_x].b * 0.300;
            }
        }
        led_panel_set_pixel(panel, x, 0, &c);
    }
}