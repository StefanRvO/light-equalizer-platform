#pragma once

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <sys/time.h>
#include "modules/c_link.h"
typedef enum
{
    STROBE_OFF,
    STROBE_ON
} strobe_state_t;

typedef struct
{
    struct timeval on_time;
    struct timeval off_time;
} strobe_light_timing_spec_t;

typedef void (*strobe_func_t)(void *);
typedef struct
{
    strobe_light_timing_spec_t timing_spec;
    TickType_t off_start_tick;
    TickType_t on_start_tick;
    strobe_state_t state;
    struct timeval offset;
    void *strobe_callback_data;
    strobe_func_t strobe_on_func;
    strobe_func_t strobe_off_func;
} strobe_light_data_t;

C_LINK void strobe_lib_reset_struct(strobe_light_data_t* strobe_data);
C_LINK void strobe_lib_compute_strobe_timming(strobe_light_data_t* strobe_data);
C_LINK bool strobe_lib_step(strobe_light_data_t* strobe_data);