#include "strobe_lib.h"
#include "esp_mesh.h"
#define US_PER_S 1000000

void strobe_lib_reset_struct(strobe_light_data_t* data)
{
    data->timing_spec.on_time.tv_sec   = 0;
    data->timing_spec.on_time.tv_usec  = 0;
    data->timing_spec.off_time.tv_sec  = 0;
    data->timing_spec.off_time.tv_usec = 0;
    data->offset.tv_sec                = 0;
    data->offset.tv_usec               = 0;
    data->on_start_tick                = xTaskGetTickCount();
    data->off_start_tick               = xTaskGetTickCount();
    data->state                        = STROBE_OFF;
    data->strobe_on_func               = NULL;
    data->strobe_off_func              = NULL;

}
//Compute when we should turn off and on the leds in
//the next cycle base on current time and timing spec
//Might also add an alignment option in the future
//So different devices can blink with offset phase
void strobe_lib_compute_strobe_timming(strobe_light_data_t* strobe_data)
{
    //Find nearest alignment in the past
    TickType_t cur_tick = xTaskGetTickCount();

    uint64_t total_us    = esp_mesh_get_tsf_time();
    uint64_t on_time_us  = (uint64_t)strobe_data->timing_spec.on_time.tv_sec  * US_PER_S + strobe_data->timing_spec.on_time.tv_usec;
    uint64_t off_time_us = (uint64_t)strobe_data->timing_spec.off_time.tv_sec * US_PER_S + strobe_data->timing_spec.off_time.tv_usec;
    uint64_t offset_us   = (uint64_t)strobe_data->offset.tv_sec * US_PER_S + strobe_data->offset.tv_usec;
    uint64_t full_cycle_time = on_time_us + off_time_us;
    total_us += offset_us;
    uint64_t us_alignment    = total_us - (total_us % (full_cycle_time));

    //If the allignment is less than half a cycle in the past,
    //use that and shorten cycle, else, extend the cycle
    //We will always aim at finishing the cycle on alignment.
    uint64_t next_cycle_end = 0;
    if(total_us - us_alignment < (full_cycle_time) / 2)
    {
        next_cycle_end = us_alignment + (full_cycle_time);
    }
    else
    {
        next_cycle_end = us_alignment + (full_cycle_time) * 2;
    }

    uint64_t adjusted_cycle_time = next_cycle_end - total_us;
    uint64_t adjusted_on_time_us = (on_time_us / (float)full_cycle_time) * adjusted_cycle_time;
    uint64_t adjusted_off_time_us = (off_time_us / (float)full_cycle_time) * adjusted_cycle_time;
    strobe_data->off_start_tick = cur_tick + adjusted_on_time_us / 1000;
    strobe_data->on_start_tick  = strobe_data->off_start_tick + adjusted_off_time_us / 1000;
}

bool strobe_lib_step(strobe_light_data_t* strobe_data)
{
    TickType_t cur_tick = xTaskGetTickCount();
    switch(strobe_data->state)
    {
        case STROBE_ON:
            if(cur_tick >= strobe_data->off_start_tick)
            {
                if(strobe_data->strobe_off_func != NULL)
                { 
                    strobe_data->strobe_off_func(strobe_data->strobe_callback_data);
                }
                strobe_data->state = STROBE_OFF;
                return true;
            }
            return false;
        case STROBE_OFF:
            if(cur_tick >= strobe_data->on_start_tick)
            {
                if(strobe_data->strobe_on_func != NULL)
                { 
                    strobe_data->strobe_on_func(strobe_data->strobe_callback_data);
                }
                strobe_lib_compute_strobe_timming(strobe_data);
                strobe_data->state = STROBE_ON;
                return true;
            }
            return false;
    }
    return false;
}