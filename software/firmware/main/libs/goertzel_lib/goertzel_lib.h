#pragma once

#include <stdint.h>

typedef struct {
    float s_prev;
    float s_prev2;
    float total_power;
    uint32_t n;
    float frequency;
    uint32_t sampling_freq;
    float coeff;

} goertzel_data_t;

void goertzel_lib_init(goertzel_data_t* data);

void goertzel_lib_reset(goertzel_data_t* data);

void goertzel_lib_update(float *samples, uint32_t sample_cnt, goertzel_data_t *data);

float goertzel_lib_get_power(const goertzel_data_t *data);