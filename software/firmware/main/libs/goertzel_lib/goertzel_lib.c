#include "esp_err.h"
#include "esp_log.h"

#include "goertzel_lib.h"
#include <math.h>

__attribute__((unused)) static const char *TAG = "SAMPLER_MODULE";

void goertzel_lib_init(goertzel_data_t* data)
{
    float normalized_freq = data->frequency / data->sampling_freq;
    data->coeff = 2 * cosf(2 * M_PI * normalized_freq);
}
void goertzel_lib_reset(goertzel_data_t* data)
{
    data->s_prev      = 0;
    data->s_prev2     = 0;
    data->n           = 0;
    data->total_power = 0;
}

void goertzel_lib_update(float *samples, uint32_t sample_cnt, goertzel_data_t *data)
{
    for(uint32_t i = 0; i < sample_cnt; i++)
    {
        float s           = (float)samples[i] + data->coeff * data->s_prev - data->s_prev2;
        data->s_prev2     = data->s_prev;
        data->s_prev      = s;
        data->total_power += samples[i] * samples[i];
        data->n++;
    }
}

float goertzel_lib_get_power(const goertzel_data_t *data)
{
    return data->s_prev2 * data->s_prev2 +
           data->s_prev  * data->s_prev  -
           data->s_prev  * data->s_prev2 * data->coeff;
}