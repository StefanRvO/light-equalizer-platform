#include <cstdlib>
#include <complex>
#include "mandelbrot_lib.h"

 
template<typename ColorType, typename ImageType>
 void draw_Mandelbrot(ImageType& image,                                   //where to draw the image
                      ColorType set_color, ColorType non_set_color,       //which colors to use for set/non-set points
                      double cxmin, double cxmax, double cymin, double cymax,//the rect to draw in the complex plane
                      unsigned int max_iterations)                          //the maximum number of iterations
{
  std::size_t const ixsize = image->height;
  std::size_t const iysize = image->width;
  for (std::size_t ix = 0; ix < ixsize; ++ix)
    for (std::size_t iy = 0; iy < iysize; ++iy)
    {
      std::complex<double> c(cxmin + ix/(ixsize-1.0)*(cxmax-cxmin), cymin + iy/(iysize-1.0)*(cymax-cymin));
      std::complex<double> z = 0;
      unsigned int iterations;
 
      for (iterations = 0; iterations < max_iterations && std::abs(z) < 2.0; ++iterations) 
        z = z*z + c;
 
      led_panel_set_pixel(image, iy, ix, (iterations == max_iterations) ? &set_color : &non_set_color);
 
    }
}

void draw_mandelbrot(led_panel_t*panel, double x_min, double x_max, double y_min, double y_max, pixelColor_t color)
{
    pixelColor_t color_off = {.raw32 = 0};
    draw_Mandelbrot<pixelColor_t, led_panel_t*>(panel, color, color_off, 
                                                x_min, x_max,
                                                y_min, y_max, 50);
}