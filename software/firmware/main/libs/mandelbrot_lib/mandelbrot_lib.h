#pragma once
#ifdef __cplusplus
extern "C" {
#endif

#include "libs/led_panel/led_panel.h"
void draw_mandelbrot(led_panel_t*panel, double x_min, double x_max, double y_min, double y_max, pixelColor_t color);

#ifdef __cplusplus
}
#endif