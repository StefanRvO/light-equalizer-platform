#include "led_ring.h"

static void fill_leds(led_ring_t *ring, uint8_t start, uint8_t length, const pixelColor_t *color);
static void fill_leds_part(led_ring_t *ring, uint8_t start, uint8_t length, const pixelColor_t *color, float part);
static uint32_t get_ring_offset(led_ring_t *ring, uint32_t ring_idx);

uint32_t led_ring_get_size(led_ring_t *ring)
{
    uint32_t sum = 0;
    for(uint8_t i = 0; i < ring->rings; i++)
    {
        sum += ring->ring_size[i];
    }
    return sum;
}

uint32_t get_ring_offset(led_ring_t *ring, uint32_t ring_idx)
{
    uint32_t offset = 0;
    if(ring_idx == 0)
    {
        return 0;
    }
    for(uint8_t i = 0; i < ring_idx; i++)
    {
        offset += ring->ring_size[i];
    }
    return offset;
}

void led_ring_set_pixel(led_ring_t *ring, uint8_t ring_idx, uint8_t pixel, pixelColor_t color)
{
    ring->strand->pixels[pixel + get_ring_offset(ring, ring_idx)] = color;
}

void fill_ring(led_ring_t *ring, uint8_t ring_idx, const pixelColor_t *color)
{
    fill_leds(ring, get_ring_offset(ring, ring_idx), ring->ring_size[ring_idx], color);
}

void fill_ring_part(led_ring_t *ring, uint8_t ring_idx, const pixelColor_t *color, float part)
{
    fill_leds_part(ring, get_ring_offset(ring, ring_idx), ring->ring_size[ring_idx], color, part);
}

void fill_leds(led_ring_t *ring, uint8_t start, uint8_t length, const pixelColor_t *color)
{
    for(uint8_t i = start; i < start + length; i++)
    {
        ring->strand->pixels[i] = *color;
    }
}

void fill_leds_part(led_ring_t *ring, uint8_t start, uint8_t length, const pixelColor_t *color, float part)
{
    if(part > 1) part = 1;
    if(part < 0) part = 0;
    for(uint8_t i = start; i < start + length * part; i++)
    {
        ring->strand->pixels[i] = *color;
    }
}