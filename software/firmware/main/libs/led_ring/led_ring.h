#pragma once

#include <stdint.h>
#include "esp32_digital_led_lib.h"

#include "modules/c_link.h"

#define MAX_RINGS 15
typedef struct __attribute__((packed))
{
    uint8_t rings;
    uint8_t ring_size[MAX_RINGS];
    strand_t* strand;
} led_ring_t;

C_LINK uint32_t led_ring_get_size(led_ring_t* ring);
C_LINK void fill_ring(led_ring_t *ring, uint8_t ring_idx, const pixelColor_t *color);
C_LINK void fill_ring_part(led_ring_t *ring, uint8_t ring_idx, const pixelColor_t *color, float part);
C_LINK void led_ring_set_pixel(led_ring_t *ring, uint8_t ring_idx, uint8_t pixel, const pixelColor_t color);