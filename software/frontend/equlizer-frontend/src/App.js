import MenuAppBar         from './MenuAppBar';
import StatusPage         from "./Pages/StatusPage";
import ModePage           from "./Pages/ModePage";
import StrobePage         from "./Pages/StrobePage";
import GlitterPage        from "./Pages/GlitterPage";
import NodesPage          from "./Pages/NodesPage";
import SamplePlot         from "./Pages/SamplePlot";
import Tetris             from "./Pages/Tetris";
import WifiSettings       from "./Pages/WifiSettings";
import OTAUpgradePage     from "./Pages/OTAUpgradePage";
import PulseSettings      from "./Pages/PulseSettings";
import OnboardLED         from "./Pages/OnboardLED";
import GameOfLifeSettings from "./Pages/GameOfLifeSettings";
import RunningSections    from "./Pages/RunningSections";
import Rainbow            from "./Pages/Rainbow";
import FrequencySettings  from "./Pages/FrequencySettings";
import ShootingPage       from "./Pages/ShootingPage";

import WebSocketHandler from "./WebSocketHandler/WebSocketHandler";
import React, { Component } from "react";

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.setPage = this.setPage.bind(this);

    this.state = {
      selectedPage: null
    };

    this.pages = [
      StatusPage,
      NodesPage,
      ModePage,
      StrobePage,
      GlitterPage,
      SamplePlot,
      Tetris,
      WifiSettings,
      PulseSettings,
      ShootingPage,
      OnboardLED,
      GameOfLifeSettings,
      RunningSections,
      Rainbow,
      FrequencySettings,
      OTAUpgradePage,
    ];
  }
  
  setPage(pagename)
  {
    this.setState({selectedPage: pagename});
  }

  render()
  {
    var selected = this.state.selectedPage;
    return (
        <div>
          <MenuAppBar 
            pages = {this.pages}
            setPage = {this.setPage}
          />

          {(selected === StatusPage.getPageName())         && <StatusPage         socket={this.socket} /> }
          {(selected === NodesPage.getPageName())          && <NodesPage          socket={this.socket} /> }
          {(selected === ModePage.getPageName())           && <ModePage           socket={this.socket} /> }
          {(selected === StrobePage.getPageName())         && <StrobePage         socket={this.socket} /> }
          {(selected === GlitterPage.getPageName())        && <GlitterPage        socket={this.socket} /> }
          {(selected === PulseSettings.getPageName())      && <PulseSettings      socket={this.socket} /> }
          {(selected === SamplePlot.getPageName())         && <SamplePlot         socket={this.socket} /> }
          {(selected === Tetris.getPageName())             && <Tetris             socket={this.socket} /> }
          {(selected === WifiSettings.getPageName())       && <WifiSettings       socket={this.socket} /> }
          {(selected === OnboardLED.getPageName())         && <OnboardLED         socket={this.socket} /> }
          {(selected === GameOfLifeSettings.getPageName()) && <GameOfLifeSettings socket={this.socket} /> }
          {(selected === RunningSections.getPageName())    && <RunningSections    socket={this.socket} /> }
          {(selected === Rainbow.getPageName())            && <Rainbow            socket={this.socket} /> }
          {(selected === OTAUpgradePage.getPageName())     && <OTAUpgradePage     socket={this.socket} /> }
          {(selected === FrequencySettings.getPageName())  && <FrequencySettings  socket={this.socket} /> }
          {(selected === ShootingPage.getPageName())       && <ShootingPage       socket={this.socket} /> }

          <WebSocketHandler
                ref ={WebSocketHandler => {
                this.socket = WebSocketHandler;
            }}/>
        
        </div>
    );
  }
}

export default App;