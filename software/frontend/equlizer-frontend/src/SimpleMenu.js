import React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    display: 'inline-block',
  },
});


class SimpleMenu extends React.Component {
  constructor(props)
  {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    
    this.state = {
      anchorEl: null,
      title: props.pages[2].getPageName(),
    };
    this.props.setPage(this.state.title);
  }

  handleClick(event) {
    this.setState({
      anchorEl: event.currentTarget,
      title: this.state.title,
    });
  }

  handleClose(event) {
    if(event.target.textContent)
    {
      this.props.setPage(event.target.textContent);
      this.setState({
        anchorEl: null,
        title: event.target.textContent,

      });
    }
    else
    {
      this.setState({"anchorEl" : null});
    }
  }
  
  renderMenuItem(item)
  {
    return(
      <MenuItem onClick={this.handleClose} key={item.getPageName()} >{item.getPageName()}</MenuItem>
    );
  }
  renderMenuItems()
  {
    const items = this.props.pages.map((page) =>
        this.renderMenuItem(page)
    );
    return items;
  }
  render()
  {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="Menu" onClick={this.handleClick}>
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" className={classes.title}>{this.state.title}</Typography>
        <Menu
          id="simple-menu"
          anchorEl={this.state.anchorEl}
          keepMounted
          open={Boolean(this.state.anchorEl)}
          onClose={this.handleClose}
        >
          {this.renderMenuItems()}
        </Menu>
      </div>
    );
  }
}

export default withStyles(styles)(SimpleMenu);