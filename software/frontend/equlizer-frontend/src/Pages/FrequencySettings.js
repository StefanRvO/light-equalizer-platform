import React, { Component } from "react";
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import MESSAGE_TYPES from './Types/MessageTypes'
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    margin: 'auto'
  },
  formControl: {
    margin: 'auto',
    width: '90%',
  },
  title: {
    flexGrow: 1,
  },    
});

  
class FrequencySettings extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleData = this.handleData.bind(this);
      this.handleOnChangeSlider = this.handleOnChangeSlider.bind(this);
      this.handleOnChangeCheckbox = this.handleOnChangeCheckbox.bind(this);

      this.msgType = MESSAGE_TYPES["FREQUENCY_SETTINGS"];
      
      props.socket.registerHandler(this.msgType, this.handleData);

      this.state = {
        power    : 0,
        exponent : 0,
        ycross   : 0,
        log_base : 0,
        divider  : 0,
        peak_decay_rate : 0,
        show_current : false,
        show_peaks   : false
      }
    }

    static getPageName()
    {
        return "Frequency Settings";
    }

    handleData(data)
    {
        this.setState({
            exponent        : data.readFloatLE(0),
            power           : data.readFloatLE(4),
            ycross          : data.readFloatLE(8),
            log_base        : data.readFloatLE(12),
            divider         : data.readFloatLE(16),
            peak_decay_rate : data.readFloatLE(20),
            show_current    : data.readUInt8(24),
            show_peaks      : data.readUInt8(25)
        });
    }

    handleOnChangeSlider = name => (e, value) => {
        this.setState({
            [name]: value
        });
        this.publishState();
    };

    handleOnChangeCheckbox(e, value)  {
      this.setState({
          [e.target.id]: value,
      }, this.publishState);
    };


    publishState()
    {
        var buf = new Buffer(27)
        buf.writeUInt8(this.msgType);
        buf.writeFloatLE(this.state.exponent, 1);
        buf.writeFloatLE(this.state.power,    5);
        buf.writeFloatLE(this.state.ycross,  9);
        buf.writeFloatLE(this.state.log_base, 13);
        buf.writeFloatLE(this.state.divider, 17);
        buf.writeFloatLE(this.state.peak_decay_rate, 21);
        buf.writeUInt8(this.state.show_current, 25);
        buf.writeUInt8(this.state.show_peaks, 26);
        this.props.socket.publish(buf);
    }
      render()
    {
        const { classes } = this.props;
        return(
            <div>
            <Typography align="center" variant="h5">Frequency Settings</Typography>
            <form className={classes.root} autoComplete="off">
            <FormControl className={classes.formControl} margin='normal'>
                <Typography id="exponent-label" gutterBottom>
                    Exponent
                </Typography>
                <Slider
                    value={this.state.exponent}
                    onChange={this.handleOnChangeSlider("exponent")}
                    onChangeCommitted={this.handleOnChangeSlider("exponent")}
                    aria-labelledby="exponent-label"
                    step={0.01}
                    min={0}
                    max={5}
                    marks
                    name='exponent'
                    valueLabelDisplay="auto"    
                />

                <Typography id="power-label" gutterBottom>
                    Max Frequency
                </Typography>
                <Slider
                    value={this.state.power}
                    onChange={this.handleOnChangeSlider("power")}
                    onChangeCommitted={this.handleOnChangeSlider("power")}
                    aria-labelledby="power-label"
                    step={10}
                    min={100}
                    max={22000}
                    marks
                    name='power'
                    valueLabelDisplay="auto"    
                />           

                <Typography id="ycross-label" gutterBottom>
                    Min Frequency
                </Typography>
                <Slider
                    value={this.state.ycross}
                    onChange={this.handleOnChangeSlider("ycross")}
                    onChangeCommitted={this.handleOnChangeSlider("ycross")}
                    aria-labelledby="ycross-label"
                    step={5}
                    min={0}
                    max={5000}
                    marks
                    name='ycross'
                    valueLabelDisplay="auto"    
                />           

                <Typography id="log-base-label" gutterBottom>
                    Scaling Logarithmic base
                </Typography>
                <Slider
                    value={this.state.log_base}
                    onChange={this.handleOnChangeSlider("log_base")}
                    onChangeCommitted={this.handleOnChangeSlider("log_base")}
                    aria-labelledby="log-base-label"
                    step={0.01}
                    min={1.01}
                    max={3}
                    marks
                    name='log-base'
                    valueLabelDisplay="auto"    
                />

                <Typography id="divider-label" gutterBottom>
                    Scaling Divider
                </Typography>
                <Slider
                    value={this.state.divider}
                    onChange={this.handleOnChangeSlider("divider")}
                    onChangeCommitted={this.handleOnChangeSlider("divider")}
                    aria-labelledby="divider-label"
                    step={1}
                    min={1}
                    max={100}
                    marks
                    name='divider'
                    valueLabelDisplay="auto"    
                />

                <Typography id="peak_decay-label" gutterBottom>
                    Peak Decay Rate
                </Typography>
                <Slider
                    value={this.state.peak_decay_rate}
                    onChange={this.handleOnChangeSlider("peak_decay_rate")}
                    onChangeCommitted={this.handleOnChangeSlider("peak_decay_rate")}
                    aria-labelledby="peak_decay-label-label"
                    step={.01}
                    min={0}
                    max={1}
                    marks
                    name='peak_decay_rate'
                    valueLabelDisplay="auto"    
                />
                <FormControlLabel
                  control={
                    <Checkbox
                    id="show_current"
                    onChange={this.handleOnChangeCheckbox}
                    color = "primary"
                    checked={!!this.state.show_current}
                  />
                  }
                  label="Show Current"
                />

                <FormControlLabel
                  control={
                    <Checkbox
                    id="show_peaks"
                    onChange={this.handleOnChangeCheckbox}
                    color = "primary"
                    checked={!!this.state.show_peaks}
                  />
                  }
                  label="Show Peaks"
                />


                </FormControl>

            </form>
            </div>
        );
    }
  }

  export default withStyles(styles)(FrequencySettings);