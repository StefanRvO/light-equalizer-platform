import React, { Component } from "react";
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import MESSAGE_TYPES from './Types/MessageTypes'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    margin: 'auto'
  },
  formControl: {
    margin: 'auto',
    width: '90%',
  },
  title: {
    flexGrow: 1,
  },    
});

  
class GameOfLifeSettings extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleData = this.handleData.bind(this);
      this.handleOnChangeSlider = this.handleOnChangeSlider.bind(this);

      this.msgType = MESSAGE_TYPES["GAME_OF_LIFE_SETTINGS"];
      
      props.socket.registerHandler(this.msgType, this.handleData);

      this.state = {
        update_rate : 0
      }
    }

    static getPageName()
    {
        return "Game Of Life";
    }

    handleData(data)
    {
        this.setState({
            update_rate  : data.readUInt32LE(0),
        });
    }

    handleOnChangeSlider = name => (e, value) => {
        this.setState({
            [name]: value
        });
        this.publishState();
    };

    publishState()
    {
        var buf = new Buffer(5)
        buf.writeUInt8(this.msgType);
        buf.writeUInt32LE(this.state.update_rate, 1);
        this.props.socket.publish(buf);
    }
      render()
    {
        const { classes } = this.props;
        return(
            <div>
            <Typography align="center" variant="h5">Game Of Life Settings</Typography>
            <form className={classes.root} autoComplete="off">
            <FormControl className={classes.formControl} margin='normal'>
                <Typography id="update_rate-label" gutterBottom>
                    update rate (ms)
                </Typography>
                <Slider
                    value={this.state.update_rate}
                    onChange={this.handleOnChangeSlider("update_rate")}
                    onChangeCommitted={this.handleOnChangeSlider("update_rate")}
                    aria-labelledby="update_rate-label"
                    step={1}
                    min={2}
                    max={1000}
                    marks
                    name='update_rate'
                    valueLabelDisplay="auto"    
                />           
                </FormControl>
            </form>
            </div>
        );
    }
  }

  export default withStyles(styles)(GameOfLifeSettings);