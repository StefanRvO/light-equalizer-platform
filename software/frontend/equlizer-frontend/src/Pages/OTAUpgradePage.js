import React, { Component } from "react";
import { withStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import ReactFileReader from 'react-file-reader';
import Grid from '@material-ui/core/Grid';

import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

import MESSAGE_TYPES from './Types/MessageTypes'


const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        width: 300,
      },
      formControl: {
        margin: 'auto',
        minWidth: 120,
      },
      title: {
        flexGrow: 1,
      },
      paper: {
        width: '100%',
        overflowX: 'auto',
      },

  });
  
class OTAUpgradePage extends Component {
    constructor(props, context) {
      super(props, context);

      this.handleFiles = this.handleFiles.bind(this);
      this.handleData  = this.handleData.bind(this);

      this.state = {
        fileArray : null,
        offset    : 0,
        size      : 1,
      }

      this.msgType = MESSAGE_TYPES["OTA_DATA"];
      props.socket.registerHandler(this.msgType, this.handleData);
    }

    static getPageName()
    {
        return "OTA Upgrade";
    }

    handleData(data)
    {
        this.setState({"offset" : 0})
    }

      handleFiles = files => {
        let fileReader = new FileReader();
        var page = this;
        fileReader.onload = function(event) {
            page.setState({"offset"    : 0});
            page.setState({"size"      : fileReader.result.byteLength});
            page.setState({"fileArray" : fileReader.result});

            var id = setInterval(function() {
                var data = new Buffer(page.state.fileArray.slice(page.state.offset, page.state.offset + 1500));
                var buf = new Buffer(1513);
                buf.writeUInt8(page.msgType);
                buf.writeUInt32LE(page.state.size, 1);
                buf.writeUInt32LE(page.state.offset, 5);
                buf.writeUInt32LE(data.byteLength, 9);
                data.copy(buf, 13);
                page.props.socket.publish(buf);
                
                page.setState({"offset" : page.state.offset + data.byteLength});        
                if(page.state.offset === page.state.size)
                {
                    clearInterval(id);
                }
            }, 40);            
          };

        fileReader.readAsArrayBuffer(files[0]);
      }
          

    render()
    {

      return(
        <div>

        <Grid   container
                direction="column"
                justify="center"
                alignItems="center">
        <Grid item xs={12}>
            <ReactFileReader fileTypes=".bin" base64={false} multipleFiles={false} handleFiles={this.handleFiles}>
                <Button variant="contained" color="primary">
                        Upload Firmware
                    </Button>
            </ReactFileReader>
        </Grid>
        <Grid item xs={3} zeroMinWidth>
                {(this.state.offset === 0) ? 
                <CircularProgressbar value={0} maxValue={1} text={`${0}%`} /> :
                <CircularProgressbar value={this.state.offset / this.state.size} maxValue={1} text={`${Math.floor(this.state.offset / this.state.size * 100)}%`} />
                }
        </Grid>
      </Grid>      
          </div>
      );
    }
  }

  export default withStyles(styles)(OTAUpgradePage);