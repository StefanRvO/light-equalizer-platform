import React from 'react'
import MESSAGE_TYPES from './Types/MessageTypes'



var sendIntervalFunc = null;

export default class Tetris extends React.Component {
    constructor(props){
        super(props);
        this.keyboardEventDown = this.keyboardEventDown.bind(this);
        this.keyboardEventUp = this.keyboardEventUp.bind(this);
        this.sendMove = this.sendMove.bind(this);

        this.msgType = MESSAGE_TYPES["TETRIS_MOVE"];

      }

    keyboardEventDown(event){
        clearInterval(sendIntervalFunc);
        sendIntervalFunc = setInterval(this.sendMove, 50, event.keyCode);
    }
    keyboardEventUp(event){
        clearInterval(sendIntervalFunc);
    }

    sendMove(key)
    {
        var move = 6;
        if(key === 37) move = 0
        else if(key === 39) move = 1
        else if(key === 38) move = 2
        else if(key === 40) move = 4
        else if(key === 32)
        {
            clearInterval(sendIntervalFunc);
            move = 5
        }
        else
        {
            return;
        }


        var buf = new Buffer(2)
        buf.writeUInt8(this.msgType, 0);
        buf.writeUInt8(move, 1);
        this.props.socket.publish(buf);

    }
    componentDidMount(){
        document.addEventListener("keydown", this.keyboardEventDown, false);
        document.addEventListener("keyup", this.keyboardEventUp, false);
    }
    componentWillUnmount(){
        document.removeEventListener("keydown", this.keyboardEventDown, false);
        document.removeEventListener("keyup", this.keyboardEventUp, false);
        clearInterval(sendIntervalFunc);
    }
    render(){
        return (   
            <div>
            </div>
        );
    }
    static getPageName()
    {
        return "Tetris";
    }
  }
