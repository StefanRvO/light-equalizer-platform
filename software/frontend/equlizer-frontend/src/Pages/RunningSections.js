import React, { Component } from "react";
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';

import MESSAGE_TYPES from './Types/MessageTypes'

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        margin: 'auto'
      },
      formControl: {
        margin: 'auto',
        width: '90%',
      },
      title: {
        flexGrow: 1,
      },    
  });

  
class RunningSections extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleData = this.handleData.bind(this);
      this.handleOnChangeSlider = this.handleOnChangeSlider.bind(this);

      this.msgType = MESSAGE_TYPES["RUNNING_SECTIONS"];
      
      props.socket.registerHandler(this.msgType, this.handleData);

      this.state = {
        period_time_ms   : 0,
        section_count    : 0,
        section_size     : 0,
      }
    }

    static getPageName()
    {
        return "Running Sections";
    }

    handleData(data)
    {
        this.setState({
            period_time_ms  : data.readUInt32LE(0),
            section_count   : data.readUInt32LE(4),
            section_size    : data.readUInt32LE(8),
        });
    }

    handleOnChangeSlider = name => (e, value) => {
        this.setState({
            [name]: value
        });
        this.publishState();
    };

    publishState()
    {
        var buf = new Buffer(13)
        buf.writeUInt8(this.msgType);
        buf.writeUInt32LE(this.state.period_time_ms, 1);
        buf.writeUInt32LE(this.state.section_count,  5);
        buf.writeUInt32LE(this.state.section_size,   9);
        this.props.socket.publish(buf);
    }
    
    render()
    {
        const { classes } = this.props;
        return(
            <div>
            <Typography align="center" variant="h5">Running Sections</Typography>
            <form className={classes.root} autoComplete="off">
            <FormControl className={classes.formControl} margin='normal'>
                <Typography id="period-time-label" gutterBottom>
                    Period time (ms)
                </Typography>
                <Slider
                    value={this.state.period_time_ms}
                    onChange={this.handleOnChangeSlider("period_time_ms")}
                    onChangeCommitted={this.handleOnChangeSlider("period_time_ms")}
                    aria-labelledby="period-time-label"
                    min={0}
                    max={500}
                    name='period_time_ms'
                    valueLabelDisplay="auto"    
                />           

                <Typography id="section-count-label" gutterBottom>
                Section Count
                </Typography>
                <Slider
                    value={this.state.section_count}
                    onChange={this.handleOnChangeSlider("section_count")}
                    onChangeCommitted={this.handleOnChangeSlider("section_count")}
                    aria-labelledby="section-count-label"
                    step={1}
                    min={0}
                    max={20}
                    marks
                    name='section_count'
                    valueLabelDisplay="auto"    
                />

                <Typography id="section-size-label" gutterBottom>
                Section Size
                </Typography>
                <Slider
                    value={this.state.section_size}
                    onChange={this.handleOnChangeSlider("section_size")}
                    onChangeCommitted={this.handleOnChangeSlider("section_size")}
                    aria-labelledby="section-size-label"
                    step={1}
                    min={1}
                    max={20}
                    marks
                    name='section_size'
                    valueLabelDisplay="auto"    
                />                
                </FormControl>
            </form>
            </div>
        );
    }
  }

  export default withStyles(styles)(RunningSections);