import React, { Component } from 'react'
import CanvasJSReact from '../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;


export default class FrequencyPlot extends Component {
	constructor(props) {
		super(props);
        this.updateChart = this.updateChart.bind(this);
        this.updateData  = this.updateData.bind(this);
        this.data = [{x: 1, y: 10}];   //dataPoints.
	}
	componentDidMount() {
        setInterval(this.updateChart, 50);
        this.props.parentObj.registerHandler(this.props.title, this.updateData);
    }
    updateData(data)
    {
        var plot = this;
        plot.data.length = 0;
        data.forEach(function (item, index)
        {
            plot.data.push(item);
        });
    }
	updateChart() {
        if(this.chart) this.chart.render();
	}
	render() {
		const options = {
			title :{
				text: this.props.title
            },
            axisY:{
                maximum: 20,
                minimum: 0
              },             
			data: [{
                type:"column",
				indexLabelFontColor: "#5A5757",
				indexLabelPlacement: "outside",
				dataPoints: this.data
			}]
		}
		return (
		<div>
			<CanvasJSChart options = {options}
				 onRef={ref => this.chart = ref}
			/>
		</div>
		);
	}
}