import React, { Component } from "react";
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import MESSAGE_TYPES from './Types/MessageTypes'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    margin: 'auto'
  },
  formControl: {
    margin: 'auto',
    width: '90%',
  },
  title: {
    flexGrow: 1,
  },    
});

  
class OnboardLED extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleData = this.handleData.bind(this);
      this.handleOnChangeSlider = this.handleOnChangeSlider.bind(this);

      this.msgType = MESSAGE_TYPES["ONBOARD_LED_STRENGTH"];
      
      props.socket.registerHandler(this.msgType, this.handleData);

      this.state = {
        strength : 0
      }
    }

    static getPageName()
    {
        return "OnboardLED";
    }

    handleData(data)
    {
        this.setState({
            strength  : data.readFloatLE(0),
        });
    }

    handleOnChangeSlider = name => (e, value) => {
        this.setState({
            [name]: value
        });
        this.publishState();
    };

    publishState()
    {
        var buf = new Buffer(5)
        buf.writeUInt8(this.msgType);
        buf.writeFloatLE(this.state.strength, 1);
        this.props.socket.publish(buf);
    }
      render()
    {
        const { classes } = this.props;
        return(
            <div>
            <Typography align="center" variant="h5">Onboard LED Settings</Typography>
            <form className={classes.root} autoComplete="off">
            <FormControl className={classes.formControl} margin='normal'>
                <Typography id="strength-label" gutterBottom>
                    strength
                </Typography>
                <Slider
                    value={this.state.strength}
                    onChange={this.handleOnChangeSlider("strength")}
                    onChangeCommitted={this.handleOnChangeSlider("strength")}
                    aria-labelledby="strength-label"
                    step={0.01}
                    min={0}
                    max={1}
                    marks
                    name='strength'
                    valueLabelDisplay="auto"    
                />           
                </FormControl>
            </form>
            </div>
        );
    }
  }

  export default withStyles(styles)(OnboardLED);