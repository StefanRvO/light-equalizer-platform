import React, { Component } from "react";
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import MODES from './Types/Modes'
import MESSAGE_TYPES from './Types/MessageTypes'

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        margin: 'auto'
      },
      formControl: {
        margin: 'auto',
        width: '90%',
      },
      title: {
        flexGrow: 1,
      },
  });

  
class ModePage extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleData = this.handleData.bind(this);
      this.handleOnChange = this.handleOnChange.bind(this);
      this.handleOnChangeSlider = this.handleOnChangeSlider.bind(this);

      this.msgType = MESSAGE_TYPES['LIGHT_MODE'];
      props.socket.registerHandler(this.msgType, this.handleData);

      this.state = {
        mode     : 0,
        strength : 0,
        r        : 0,
        g        : 0,
        b        : 0,
      }
    }

    static getPageName()
    {
        return "Modes";
    }

    handleData(data)
    {
        this.setState({
            mode     : data.readUInt8(0),
            strength : data.readFloatLE(1),
            r        : data.readUInt8(5),
            g        : data.readUInt8(6),
            b        : data.readUInt8(7),    
        });
    }

    handleOnChange = name => (e, value) => {
        this.setState({
          [name]: value.props.value,
      }, this.publishState);
    }
  
    handleOnChangeSlider = name => (e, value) => {
        this.setState({
            [name]: value
        });
        this.publishState();
    };

    publishState()
    {
        var buf = new Buffer(9)
        buf.writeUInt8(this.msgType);
        buf.writeUInt8(this.state.mode, 1);
        buf.writeFloatLE(this.state.strength, 2);
        buf.writeUInt8(this.state.r, 6);
        buf.writeUInt8(this.state.g, 7);
        buf.writeUInt8(this.state.b, 8);
        this.props.socket.publish(buf);
    }
      render()
    {
        const { classes } = this.props;
        const modeitems = Object.keys(MODES).map((value, index) => {
            return(
                <MenuItem key = {value} value={value}>{MODES[value]}</MenuItem>
            );
        });
        return(
            <div>
            <Typography align="center" variant="h5">Select Mode</Typography>
            <form className={classes.root} autoComplete="off">
            <FormControl className={classes.formControl} margin='normal'>
                <Typography id="mode-label" gutterBottom>
                Mode
                </Typography>
                <Select
                value={this.state.mode}
                onChange={this.handleOnChange("mode")}
                aria-labelledby="mode-label"
                inputProps={{
                    name: 'mode',
                    id: 'mode-simple',
                }}
                >
                {modeitems}
                </Select>
                <Typography id="strength-label" gutterBottom>
                Strength
                </Typography>
                <Slider
                    value={this.state.strength}
                    onChange={this.handleOnChangeSlider("strength")}
                    onChangeCommitted={this.handleOnChangeSlider("strength")}
                    aria-labelledby="strength-label"
                    step={0.05}
                    min={0}
                    max={1}
                    marks
                    name='strength'
                    valueLabelDisplay="auto"    
                />           
                <Typography id="red-label" gutterBottom>
                Red
                </Typography>
                <Slider
                    value={this.state.r}
                    onChange={this.handleOnChangeSlider("r")}
                    onChangeCommitted={this.handleOnChangeSlider("r")}
                    aria-labelledby="red-label"
                    step={5}
                    min={0}
                    max={255}
                    marks
                    name='red'
                    valueLabelDisplay="auto"    
                />
                <Typography id="green-label" gutterBottom>
                Green
                </Typography>
                <Slider
                    value={this.state.g}
                    onChange={this.handleOnChangeSlider("g")}
                    onChangeCommitted={this.handleOnChangeSlider("g")}
                    aria-labelledby="green-label"
                    step={5}
                    min={0}
                    max={255}
                    marks
                    name='green'
                    valueLabelDisplay="auto"    
                />
                <Typography id="blue-label" gutterBottom>
                Blue
                </Typography>
                <Slider
                    value={this.state.b}
                    onChange={this.handleOnChangeSlider("b")}
                    onChangeCommitted={this.handleOnChangeSlider("b")}
                    aria-labelledby="blue-label"
                    step={5}
                    min={0}
                    max={255}
                    marks
                    name='blue'
                    valueLabelDisplay="auto"    
                />
                </FormControl>
            </form>
            </div>
        );
    }
  }

  export default withStyles(styles)(ModePage);