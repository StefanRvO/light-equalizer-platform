import React from 'react';
import FrequencyPlot from "./FrequencyPlot"

import MESSAGE_TYPES from './Types/MessageTypes'

export default class StatusPage extends React.Component {
    constructor(props) {
      super(props);

      this.handleData = this.handleData.bind(this);
      this.msgType = MESSAGE_TYPES["FREQUENCY_INFO"];
      props.socket.registerHandler(this.msgType, this.handleData);
      this.msgHandlers = [];
    }

    handleData(data)
    {
        var channel          = data.readUInt8(0);
        var bins             = data.readUInt32LE(1);
        var sample_frequency = data.readUInt32LE(5);
        data = data.slice(9, data.length);
        var freq_data = []
        console.log(data.length, bins)
        var i = 0;
        for(; i < bins * 1./6; i++)
        {
            freq_data.push({x: i * sample_frequency / ((bins - 1) * 2), y: 10 * Math.log10(data.readFloatLE(4 * i))});
        }
        var j = 0;
        for(; j < bins * 1./12; j++)
        {
            freq_data.push({x: (i + j * 2) * sample_frequency / ((bins - 1) * 2), y: 10 * Math.log10(data.readFloatLE(4 * (i + j)))});
        }
        var k = 0;
        for(; k < bins * 1./24; k++)
        {
            freq_data.push({x: (i + j * 2 + k * 4) * sample_frequency / ((bins - 1) * 2), y: 10 * Math.log10(data.readFloatLE(4 * (i + j + k)))});
        }
        var l = 0;
        for(; l < bins * 1./48; l++)
        {
            freq_data.push({x: (i + j * 2 + k * 4 + l * 8) * sample_frequency / ((bins - 1) * 2), y: 10 * Math.log10(data.readFloatLE(4 * (i + j + k + l)))});
        }
        var m = 0;
        for(; m < bins * 1./96; m++)
        {
            console.log("bin " + i + j * 2 + k * 4 + l * 8 + m * 16 + " byte" + 4  * (i + j + k + l + m))
            //freq_data.push({x: (i + j * 2 + k * 4 + l * 8 + m * 16) * sample_frequency / ((bins - 1) * 2), y: 10 * Math.log10(data.readFloatLE(4 * (i + j + k + l + m)))});
        }
        var n = 0;
        for(; n < bins * 1./192; n++)
        {
            console.log("bin " + i + j * 2 + k * 4 + l * 8 + m * 16 + n * 32 + " byte" + 4  * (i + j + k + l + m + n))
            //freq_data.push({x: (i + j * 2 + k * 4 + l * 8 + m * 16 + n * 32) * sample_frequency / ((bins - 1) * 2), y: 10 * Math.log10(data.readFloatLE(4 * (i + j + k + l + m + n)))});
        }


        console.log(freq_data);

        if(this.msgHandlers["LEFT"] && channel === 0)
        {
            this.msgHandlers["LEFT"](freq_data);
        }
        if(this.msgHandlers["RIGHT"] && channel === 1)
        {
            this.msgHandlers["RIGHT"](freq_data);
        }
        console.log(freq_data)
    }
    registerHandler(type, handler)
    {
        console.log("Statuspage Registering handler for type " + type  )
        this.msgHandlers[type] = handler;
    }

    static getPageName()
    {
        return "Status";
    }
    render()
    {
        return(
            <div>
                <FrequencyPlot title="LEFT"  parentObj = {this}/>
                <FrequencyPlot title="RIGHT" parentObj = {this}/>
            </div>
        );
    }
  }
  