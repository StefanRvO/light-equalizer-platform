const COLORORDER = {
    0 : 'RGB',
    1 : 'RBG',
    2 : 'GBR',
    3 : 'GRB',
    4 : 'BGR',
    5 : 'BRG',
}

export default COLORORDER;