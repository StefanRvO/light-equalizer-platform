const LIGHT_TYPES = {
    0 : 'STRING',
    1 : 'PANEL',
    2 : 'RING'
}

export default LIGHT_TYPES;