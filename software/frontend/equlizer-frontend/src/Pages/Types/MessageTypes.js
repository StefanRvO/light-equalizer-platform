const MESSAGE_TYPES = {
    'LIGHT_MODE'             : 0,
    'STROBE'                 : 1,
    'GLITTER'                : 2,
    'NODE_INFO'              : 3,
    'FREQUENCY_INFO'         : 4,
    'SAMPLES'                : 5,
    'TETRIS_MOVE'            : 6,
    'WIFI_SETTINGS'          : 7,
    'SCAN_WIFI'              : 8,
    'OTA_DATA'               : 9,
    'PING'                   : 10,
    'PULSE_SETTINGS'         : 11,
    'ONBOARD_LED_STRENGTH'   : 12,
    'GAME_OF_LIFE_SETTINGS'  : 13,
    'RUNNING_SECTIONS'       : 14,
    'RAINBOW'                : 15,
    'FREQUENCY_SETTINGS'     : 16,
    'SHOOTING'               : 17,
}

export default MESSAGE_TYPES;