const MODES = {
    0  : 'OFF',
    1  : 'ON',
    2  : 'STROBE',
    3  : 'GLITTER',
    4  : 'FREQUENCY_PANEL',
    5  : 'TETRIS',
    6  : 'GAME_OF_LIFE',
    7  : 'RUNNING_RING',
    8  : 'PULSE',
    9  : 'RUNNING_SECTIONS',
    10 : 'RAINBOW',
    14 : 'FREQUENCY_SINGLE_COLOR_BEAT',
    17 : 'MATRIX',
    18 : 'FREQUENCY_PANEL_FALLING',
    20 : 'FREQUENCY_GLITTER',
    21 : 'FIRE',
    22 : 'FIRE_FREQUENCY',
    24 : 'SHOOTING',
    25 : 'LIGHT_MODE_MANDELBROT',
}

export default MODES;