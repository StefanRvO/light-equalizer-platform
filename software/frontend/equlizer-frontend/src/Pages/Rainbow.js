import React, { Component } from "react";
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import MESSAGE_TYPES from './Types/MessageTypes'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    margin: 'auto'
  },
  formControl: {
    margin: 'auto',
    width: '90%',
  },
  title: {
    flexGrow: 1,
  },    
});

  
class Rainbow extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleData = this.handleData.bind(this);
      this.handleOnChangeSlider = this.handleOnChangeSlider.bind(this);

      this.msgType = MESSAGE_TYPES["RAINBOW"];
      
      props.socket.registerHandler(this.msgType, this.handleData);

      this.state = {
        update_rate_ms : 0
      }
    }

    static getPageName()
    {
        return "Rainbow";
    }

    handleData(data)
    {
        this.setState({
            update_rate_ms  : data.readUInt32LE(0),
        });
    }

    handleOnChangeSlider = name => (e, value) => {
        this.setState({
            [name]: value
        });
        this.publishState();
    };

    publishState()
    {
        var buf = new Buffer(5)
        buf.writeUInt8(this.msgType);
        buf.writeUInt32LE(this.state.update_rate_ms, 1);
        this.props.socket.publish(buf);
    }
      render()
    {
        const { classes } = this.props;
        return(
            <div>
            <Typography align="center" variant="h5">Rainbow Settings</Typography>
            <form className={classes.root} autoComplete="off">
            <FormControl className={classes.formControl} margin='normal'>
                <Typography id="update-rate-label" gutterBottom>
                    update rate (ms)
                </Typography>
                <Slider
                    value={this.state.update_rate_ms}
                    onChange={this.handleOnChangeSlider("update_rate_ms")}
                    onChangeCommitted={this.handleOnChangeSlider("update_rate_ms")}
                    aria-labelledby="update-rate-label"
                    step={1}
                    min={0}
                    max={300}
                    marks
                    name='update_rate_ms'
                    valueLabelDisplay="auto"    
                />           
                </FormControl>
            </form>
            </div>
        );
    }
  }

  export default withStyles(styles)(Rainbow);