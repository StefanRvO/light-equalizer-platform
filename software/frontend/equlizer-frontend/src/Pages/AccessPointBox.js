import React, { Component } from "react";
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'; 
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    root: {
        width: '100%',
      },
      heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
      },
      table: {
        minWidth: 250,
      },
      textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
      },
  });

  class AccessPointBox extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleOnChange  = this.handleOnChange.bind(this);
      this.createTextField = this.createTextField.bind(this);
      this.createButton    = this.createButton.bind(this);
      this.connect         = this.connect.bind(this);

      this.state = {
        ssid  : props.data.ssid,
        bssid : props.data.bssid,
        pass : "",
        rssi : props.data.rssi,

      };
      this.msgType = props.msgType;
    }


    connect()
    {
        var buf = new Buffer(99);
        buf.writeUInt8(this.msgType, 0);
        buf.writeUInt8(this.state.ssid.length, 1);
        buf.writeUInt8(this.state.pass.length, 2);
        buf.write(this.state.ssid, 3, 35, "ascii");
        buf.write(this.state.pass, 35, 99, "ascii");
        this.props.socket.publish(buf);
    }
    
    createData(name, value) {
        return { name, value };
    }

    createTextField(name, id)
    {
        const { classes } = this.props;
        return(
            <TableRow key = {id}>
                <TableCell component="th" scope="row">
                    {name}
                </TableCell>
                <TableCell align="right">
                    <TextField
                        id={id}
                        className={classes.textField}
                        onChange={this.handleOnChange}
                        margin="normal"
                    />
                </TableCell>
            </TableRow>
        )
    }

    createButton(name, id)
    {
        return(
            <TableRow key = "SubmitButton">
                <TableCell component="th" scope="row">
                </TableCell>
                <TableCell align="right">
                    <Button variant="contained" color="primary" onClick={this.connect}>
                        Connect
                    </Button>
                </TableCell>
            </TableRow>
        )
    }

    handleOnChange(e, value)  {
        this.setState({
            [e.target.id]: e.target.value,
        }, this.publishState);
    };

      render()
    {
      const { classes } = this.props;
      const rows = [
        this.createData('SSID', this.props.data.ssid),
        this.createData('BSSID', this.props.data.bssid),
        this.createData('RSSI', this.props.data.rssi),
        ];
        const table_elements = rows.map(row => (
                                        <TableRow key={row.name}>
                                        <TableCell component="th" scope="row">
                                            {row.name}
                                        </TableCell>
                                        <TableCell align="right">{row.value}</TableCell>
                                        </TableRow>
                                        ))
      return(
          <div>
            <ExpansionPanel key={this.props.key}>
                <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls={this.props.key + "-content"}
                id={this.props.key + "-header"}
                >
                <Typography className={classes.heading}>{this.state.ssid || "Custom"}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableBody>
                        {this.props.data.bssid  && table_elements}
                        {!this.props.data.bssid && this.createTextField("SSID", "ssid")}    

                        {this.createTextField("Password", "pass")}
                        {this.createButton()}
                    </TableBody>
                </Table>
                </Paper>
                </ExpansionPanelDetails>
            </ExpansionPanel>
          </div>
      );
    }
  }

  export default withStyles(styles)(AccessPointBox);