import React, { Component } from "react";
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import MESSAGE_TYPES from './Types/MessageTypes'

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        margin: 'auto'
      },
      formControl: {
        margin: 'auto',
        width: '90%',
      },
      title: {
        flexGrow: 1,
      },    
  });
  
class ShootingPage extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleData = this.handleData.bind(this);
      this.handleOnChangeSlider = this.handleOnChangeSlider.bind(this);

      this.msgType = MESSAGE_TYPES["SHOOTING"];
      
      props.socket.registerHandler(this.msgType, this.handleData);

      this.state = {
        spawn_rate    : 0,
        fade_rate_min : 0,
        fade_rate_max : 0,
        speed_min     : 0,
        speed_max     : 0,
      }
    }

    static getPageName()
    {
        return "Shooting";
    }

    handleData(data)
    {
        this.setState({
            spawn_rate    : data.readFloatLE(0),
            fade_rate_min : data.readFloatLE(4),
            fade_rate_max : data.readFloatLE(8),
            speed_min     : data.readFloatLE(12),
            speed_max     : data.readFloatLE(16),
        });
    }

    handleOnChangeSlider = name => (e, value) => {
        this.setState({
            [name]: value
        });
        this.publishState();
    };

    publishState()
    {
        var buf = new Buffer(21)
        buf.writeUInt8(this.msgType);
        buf.writeFloatLE(this.state.spawn_rate,    1);
        buf.writeFloatLE(this.state.fade_rate_min, 5);
        buf.writeFloatLE(this.state.fade_rate_max, 9);
        buf.writeFloatLE(this.state.speed_min,     13);
        buf.writeFloatLE(this.state.speed_max,     17);
        this.props.socket.publish(buf);
    }
      render()
    {
        const { classes } = this.props;
        return(
            <div>
            <Typography align="center" variant="h5">Shooting Mode Settings</Typography>
            <form className={classes.root} autoComplete="off">
            <FormControl className={classes.formControl} margin='normal'>
                <Typography id="spawn-rate-label" gutterBottom>
                    Spawn Rate
                </Typography>
                <Slider
                    value={this.state.spawn_rate}
                    onChange={this.handleOnChangeSlider("spawn_rate")}
                    onChangeCommitted={this.handleOnChangeSlider("spawn_rate")}
                    aria-labelledby="spawn-rate-label"
                    step={0.05}
                    min={0.1}
                    max={10}
                    marks
                    name='spawn_rate'
                    valueLabelDisplay="auto"    
                />           
                <Typography id="fade_rate_min-label" gutterBottom>
                Minimum Fade Rate
                </Typography>
                <Slider
                    value={this.state.fade_rate_min}
                    onChange={this.handleOnChangeSlider("fade_rate_min")}
                    onChangeCommitted={this.handleOnChangeSlider("fade_rate_min")}
                    aria-labelledby="fade_rate_min-label"
                    step={0.01}
                    min={0.01}
                    max={1}
                    marks
                    name='fade_rate_min'
                    valueLabelDisplay="auto"    
                />
                <Typography id="fade_rate_max-label" gutterBottom>
                Maximum Fade Rate
                </Typography>
                <Slider
                    value={this.state.fade_rate_max}
                    onChange={this.handleOnChangeSlider("fade_rate_max")}
                    onChangeCommitted={this.handleOnChangeSlider("fade_rate_max")}
                    aria-labelledby="fade_rate_max-label"
                    step={0.01}
                    min={0.01}
                    max={1}
                    marks
                    name='fade_rate_max'
                    valueLabelDisplay="auto"    
                />
                <Typography id="speed_min-label" gutterBottom>
                Minimum Speed
                </Typography>
                <Slider
                    value={this.state.speed_min}
                    onChange={this.handleOnChangeSlider("speed_min")}
                    onChangeCommitted={this.handleOnChangeSlider("speed_min")}
                    aria-labelledby="speed_min-label"
                    step={1}
                    min={1}
                    max={50}
                    marks
                    name='speed_min'
                    valueLabelDisplay="auto"    
                />
                <Typography id="speed_max-label" gutterBottom>
                Maximum Speed
                </Typography>
                <Slider
                    value={this.state.speed_max}
                    onChange={this.handleOnChangeSlider("speed_max")}
                    onChangeCommitted={this.handleOnChangeSlider("speed_max")}
                    aria-labelledby="speed_max-label"
                    step={1}
                    min={10}
                    max={250}
                    marks
                    name='speed_max'
                    valueLabelDisplay="auto"    
                />
                </FormControl>
            </form>
            </div>
        );
    }
  }

  export default withStyles(styles)(ShootingPage);